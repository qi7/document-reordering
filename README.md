### README ###

This repo retrieves new ordering for faster document intersection.

### Setup ###

This repo is an Indri C++ Application, it relies on Indri API and Indri Index.


To set it up, refer to:


https://lemur.sourceforge.io/indri/


https://sourceforge.net/p/lemur/wiki/Home/


https://sourceforge.net/p/lemur/wiki/Using%20the%20API%20to%20Write%20Your%20Own%20Application/


### Build Indri Index ###


After configure Indri, the next step is to build Indri indexes, refer to:


https://sourceforge.net/p/lemur/wiki/Quick%20Start/


This repo builds indexes for Gov2 and ClueWeb09B, Indri requires parameter files to build indexes.


Sample Parameter file needed by Indri for Gov2:


```
<parameters>
<memory>5120M</memory>
<storeDocs>true</storeDocs>
<index>INDEX_PATH</index>
 <corpus>
    <path>GOV2_DATA_PATH</path>
    <class>trecweb</class>
 </corpus>
<stemmer><name>porter</name></stemmer>
<metadata>
 <forward>url</forward>
 <backward>url</backward>
</metadata>
</parameters>
```


Sample Parameter file needed by Indri for Clueweb09B:

```
<parameters>
<memory>5120M</memory>
<storeDocs>true</storeDocs>
<index>INDEX_PATH</index>
 <corpus>
    <path>ClueWeb09B_DATA_PATH</path>
    <class>warc</class>
 </corpus>
<stemmer><name>porter</name></stemmer>
<metadata>
 <forward>url</forward>
 <backward>url</backward>
</metadata>
</parameters>
```

Then run the following under Indri tool dir:

```
./buildindex parameter_file
```


### Compile and Run the code ###


After the Indri index is build, under root dir of this repo, run:

```
make -f Makefile.app
```

Build forward index used for reordering:


```
./builder vbyte_index_doc_range [start docid] [end docid], [input_indri_index_path], [output_vbyte_lex], [output_vbyte_index], [output_offset]
```


If the machine has enough memory, the start and end could cover the whole range of the document in the collection.


Or else, the above command can be run in batch based on docid range, and the final forward index can be concatenated together using the output_offset info.


Get new ordering: Modify the file path in the code accordingly, and then run

```
./builder or_bp
```