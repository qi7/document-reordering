#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <iostream>
#include <bitset>
#include <unordered_set>
#include <set>
#include "indri/QueryEnvironment.hpp"
#include "indri/SnippetBuilder.hpp"
#include "indri/LocalQueryServer.hpp"
#include "indri/ScopedLock.hpp"
#include <cmath>
using namespace indri::api;

#define MAXD 25205180

int compressionVbytes(std::vector<int>& input, std::vector<unsigned char>& compressedList);
int decompressionVbytes(char* input, int* output, int size);

class termInfo{
public:
	string term;
	int listlen;
	termInfo(){}
	termInfo(string t, int l){
		term = t;
		listlen = l;
	}
};

class docInfoR{
public:
	int part;
	int did;
	double gain;
	string url;
	docInfoR(){}
	docInfoR(int d, int p, double g){
		part = p;
		did = d;
		gain = g;
	}
	docInfoR(int d, int p, double g, string u){
		part = p;
		did = d;
		gain = g;
		url = u;
	}
};

class lexInfoR{
public:
	int id; 
	int count; 
	double gain;
	bool indicator;
	lexInfoR(){}
	lexInfoR(int i, int c, double g){
		id = i;
		count = c;
		gain = g;
	}
	lexInfoR(int i, int c, double g, bool b){
		id = i;
		count = c;
		gain = g;
		indicator = b;
	}
};

class QueryRanker{
public:
	int qid;
	string term1;
	string term2;
	int diff;
	int length_url;
	int length_bp;
	double ratio;
	QueryRanker(){}
	QueryRanker(string t1, string t2, int d){
		term1 = t1;
		term2 = t2;
		diff = d;
	}
	QueryRanker(string t1, string t2, int l_url, int l_bp){
		term1 = t1;
		term2 = t2;
		length_url = l_url;
		length_bp = l_bp;
		diff = l_url - l_bp;
	}
	QueryRanker(int id, string t1, string t2, int l_url, int l_bp){
		qid = id;
		term1 = t1;
		term2 = t2;
		length_url = l_url;
		length_bp = l_bp;
		diff = l_url - l_bp;
	}
	QueryRanker(string t1, string t2, double r, int l_url, int l_bp){
		term1 = t1;
		term2 = t2;
		ratio = r;
		length_url = l_url;
		length_bp = l_bp;
		diff = l_url - l_bp;
	}
	QueryRanker(int id, string t1, string t2, double r, int l_url, int l_bp){
		qid = id;
		term1 = t1;
		term2 = t2;
		ratio = r;
		length_url = l_url;
		length_bp = l_bp;
		diff = l_url - l_bp;
	}
};

class OrderR{
public:
	/*test on 10k set*/
	void init_test();
	/*reordering with ideal language model(test query) on gov2*/
	void init_gov2();
	/*start with url sorting each time*/
	void init_bp();
	/*the algorithm from the fb paper*/
	void init_fb();
	/*init stats*/
	void init_stats();
	/*debugging with subset of documents*/
	void init_subsets();
	/*analysis gain*/
	void analysis_gain(string dir);

private:
	int threshold;
	int round;
	int current_ID;
	int term_num;
	int did_num;
	vector<int> final_order;
	map<int, vector<int>> forward_index_map;
	/*for ideal lm*/
	map<int, int> lm_map;
	// vector<int> lm_vec;

	/*for gov2*/
	vector<int> lengths;
    vector<int> sizes;
    vector<long long> offsets;
    char* data;

    /*for real lm*/
    vector<pair<int, int>> lm_content_vec;
    map<int, pair<int, int>> lm_term_map;

	void increase_count(vector<lexInfoR>& lex, int tid);
	void increase_count_with_indicator(vector<lexInfoR>& lex, int tid);

	int get_count(vector<lexInfoR>& lex, int tid);

	double exp_bp(int p, int q);
	double exp_bp_no_minus_one(int p, int q);
	double exp_fb(int fs, int ts, int fc, int tc);

	/*objective functions*/
	void get_bp_gain(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc);
	void get_bp_gain_gov2(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc);
	void get_bp_gain_using_lm(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc);
	void get_bp_gain_debugging(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc);
	void get_fb_gain_debugging(vector<lexInfoR>& from_lex, int from_size, vector<lexInfoR>& to_lex, int to_size, docInfoR& doc, FILE* file);

	void get_bp_gain_fb(vector<lexInfoR>& from_lex, int from_size, vector<lexInfoR>& to_lex, int to_size, docInfoR& doc);

	/*partition functions*/
	void partition(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);
	void partition_gov2(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);

	/*for bp algo*/
	void partition_lm_no_presorting(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);
	void partition_lm_sort_url(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);
	void partition_lm_sort_percentile(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);
	void partition_lm_sort_percentile_random_pairs(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);

	/*for fb algo*/
	void partition_fb(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);

	/*get the gains for each iteration*/
	void partition_gain_analysis(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);
	void partition_fb_gain_analysis(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex);

	/*get how many pairs getting swapped at each level*/
	void partition_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);
	void partition_lm_sort_percentile_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);
	void partition_lm_sort_percentile_raondom_pairs_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);
	void partition_fb_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);

	/*get the documents of certain levels*/
	void partition_identify_dids(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);
	void partition_fb_identify_dids(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);

	/*track the gain of one document*/
	void partition_doc_tracker(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);
    void partition_fb_doc_tracker(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file);
};

class wholeDocInfo{
public:
	int did;
	std::string url;
	std::string dname;
	wholeDocInfo(){}
	wholeDocInfo(int d, std::string dn, std::string u){
		did = d;
		dname = dn;
		url = u;
	}
};

class lexInfo{
public:
	int listlen;
	long long offset;
	lexInfo(){}
	lexInfo(int l, long long o){
		listlen = l;
		offset = o;
	}
};

class wholeLexInfo{
public:
	string term;
	int listlen;
	long long offset;
	wholeLexInfo(){}
	wholeLexInfo(string t, int l, long long o){
		term = t;
		listlen = l;
		offset = o;
	}
};

class docInfo{
public:
	int did;
	std::string dname;
	docInfo(){}
	docInfo(int d, std::string n){
		did = d;
		dname = n;
	}
};

class posting{
public:
	int did;
	int tid;
	int freq;
	posting(){}
	posting(int d, int t, int f){
		did = d;
		tid = t;
		freq = f;
	}

};

class LMPair{
public:
	string term1;
	string term2;
	float prob;
	LMPair(){}
	LMPair(string t1, string t2, float p){
		term1 = t1;
		term2 = t2;
		prob = p;
	}
};

class LMUnigram{
public:
	string term;
	float prob;
	float backoff_prob;
	LMUnigram(){}
	LMUnigram(string t, float p, float pb){
		term = t;
		prob = p;
		backoff_prob = pb;
	}
};

class LMPairScore{
public:
	string term1;
	string term2;
	int score;
	LMPairScore(){}
	LMPairScore(string t1, string t2, int s){
		term1 = t1;
		term2 = t2;
		score = s;
	}
};

class qpList{
public:
	vector<int> dids;
	int cur_did;
	int index;
	int listlen;

	qpList(){}
	qpList(vector<int> ds, int max_d){
		dids = ds;
		index = 0;
		cur_did = max_d;
		listlen = ds.size();
		if(listlen!=0) {
			cur_did = ds[0];
		}
	}

	void get_next_GEQ(int did, int max_d){
		if(did >= max_d) cur_did = max_d;
		while(cur_did < did && cur_did < max_d){
			index++;
			cur_did = dids[index];
		}
	}
};

class QueryLexInfo{
public:
	int qid;
	string term1;
	string term2;
	string query;
	QueryLexInfo(){}
	QueryLexInfo(int id, string t1, string t2, string q){
		qid = id;
		term1 = t1;
		term2 = t2;
		query = q;
	}
};

class FIndexBuilder{
public:
	void shuffle_all_lex(); //remove duplicate url and sort by url
	void forward_lex_using_doc_range(int start_id, int end_id, string indri_index, string output_lex); //build forward index with certain doc range
	void forward_lex_using_doc_range_free_memory(int start_id, int end_id, string indri_index, string output_lex); //build forward index with certain doc range
	void forward_lex_patch_with_dups(int patch, string indri_index, string output_lex); //build forward lex by patch 100k
	void doc_content_patch(string indri_index, string vbyte_lex, string vbyte_index, int patch, long long off); // generate compressed forward index by patch
	void doc_content_using_doc_range(int start_id, int end_id, string indri_index, string vbyte_lex, string vbyte_index, long long off);
	// void query_processing_gov2(); // query processing with system ordering
	void query_processing_ordering(std::string order, std::string indri_index, std::string order_path, std::string query, int max_d); // query processing with url, random, bp ordering
	void query_processing_single_query(std::string term1, std::string term2, std::string order, std::string indri_index, std::string order_path, int max_d); //single query processing
	void verify_vbyte_forward_index(string indri_index, string vbyte_lex, string vbyte_index, int max_d, long long data_size, int d);

	void get_random_order(string url_lex, string random_order); //random ordering

	void doc_length_using_doc_range(int start_id, int end_id, string indri_index, string doc_length_lex);

	/*million09 query trace*/
	void million_query_lex_generation(string indri_index, string term_lex, string query, string parsed_query);
	void parse_query_million(string indri_index, string term_lex, string query, string parsed_query);
	void random_query_generation_m9(); //select 3k random query

	/*terabyte 2006 query trace*/
	void tera_query_lex_generation(string indri_index, string term_lex, string query, string parsed_query);
	void parse_query_tera2006(string indri_index, string term_lex, string query, string parsed_query);
	void select_random_10k_from_other_set(string potential_other, int potential_size, int random_size, string random_10k_other_set);
	void random_query_generation_tera2006();
    void stem_terms(string indri_index, string input_terms, string output_terms);

	/*query generation from query lex*/
	void query_set_construction(string query_lex, string training_set, string testing_set, string other_set);
	void remove_qid_and_raw_query(string test_query_lex, string test_pair_input_for_lm);

	/*parse language model*/
	void parse_lm(string mit_lm, string unigrams, string bigrams);
	void compute_pair_probs_from_unigrams_and_bigrams(string unigrams, string bigrams, string pairs);
	void compute_pair_score(string lm_pairs, double threshold, double multi, string pairs_with_score);
	void organize_pairs(string indri_index, string pair_scores, string lm_lex, string lm_index);
	void lm_coverage();
	void show_pair_prob();
	void show_unigram_prob();
	void organized_pair_with_probs();

	/*build lm from query trace with same weight*/
	void compute_pair_score_with_same_weight(string bigrams, string pairs_with_score);

	/*build lm for fb*/
	void compute_term_score(string indri_index, string unigrams, double multi, string term_with_score);
	void compute_term_score_unweighted(string indri_index, string unigrams, string term_with_score);

	/*10k test collection*/
	void random_10k_lex();
	void get_random_order_r10k();
	void inverted_index_r10k();
	// void query_processing(std::string order);
	void build_small_lex();
	void add_term_to_lex();
	void forward_index_r10k_url_ordering();
	void random_query_generation();
	void verify_index();
	void inverted_index_test();
	void forward_index_test();

	/*utility*/
	void build();
	void print_vocabulary(string indri_index, string term_lex);
	std::string getDocumentName(ParsedDocument* Pd);
	std::string getDocumentUrl(ParsedDocument* Pd);
	void getTermList(vector<int>& termIds, int did);
	void lookup_term(int tid);
	void lookup_tid(string term);
	void show_documents(int did);
	void show_unique_tid_documents(int did);
	void get_inverted_list(std::string termString);
	void test_vbyte();
	void reverse_query_pair();

	/*compress the index*/
	void fetch_longer_terms();
	void postings_percentage();
	void compress_list(vector<int>& dids, double& bits);
	// void fetch_query_terms(std::string queries, std::string terms);
	void compute_compression_stat(std::string order);
	void compute_compression_stat_query_average(std::string order);

	/*write out non-indri inverted lists*/
    void fetch_query_terms(std::string queries, std::string terms);
    void write_non_indri_inverted_list(string indri_index, string terms, string content, string lex);
    void write_non_indri_term_freqs(string indri_index, string terms, string content, string lex);
	void verify_non_indri_inverted_list(string indri_index, string non_indri_lex, string non_indri_content, long long data_size, int tid_to_verify);
    void verify_non_indri_term_freqs(string indri_index, string non_indri_lex, string non_indri_content, long long data_size, int tid_to_verify);

    /*write out non-indri inverted lists in ascii with url ordering*/
	void write_non_indri_inverted_lists_ascii_url(string indri_index, string terms, string ordering, string content);
	//void write_non_indri_inverted_lists_freqs_ascii_url(string indri_idnex, string terms, string ordering, string content);

    /*query processing using non indri data*/
    void qp_non_indri(std::string order, std::string non_indri_lex, std::string non_indri_content, long long data_size, int max_d, std::string order_path, std::string query); // query processing with url, random, bp ordering

	/*vldb revised*/
    void stem_queries_generate_term_lex(string indri_index, string input_query, string stemmed_queried, string stemmed_term_lex); 

private:
	std::map<std::string, docInfo> url_map;
	/*query process interfaces*/
	void and_bp(qpList& l1, qpList& l2, int& bp, int& hit, int max_d);
};
