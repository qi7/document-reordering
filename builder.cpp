#include <iostream>
#include "FIndexBuilder.h"

int main(int argc, char *argv[]){
   std::string command(argv[1]);
   if(command == "0407"){
      std::cout << "random test\n";
      double fc = 1;
      double tc = 10;
      double fs = 20;
      double ts = 20;

      double result = fc * log(fs / (fc)) + tc * log(ts / (tc+2.0));

      double result2 = result + 1.0;
      cout << result << " " << result2 << "\n";
   }
   
   if (command == "stem_terms") {
      std::cout << "stem_terms\n";
      string indri_index(argv[2]);
      string input_terms(argv[3]);
      string output_terms(argv[4]);
      FIndexBuilder b;
      b.stem_terms(indri_index, input_terms, output_terms);
   }

   if (command == "stem_full_query") {
      std::cout << "stem_full_query\n";
      string indri_index(argv[2]);
      string input_query(argv[3]);
      string output_query(argv[4]);
      FIndexBuilder b;
   }

   if(command == "posting_perc"){
      std::cout << "get posting percentage\n";
      FIndexBuilder b;
      b.postings_percentage();
   }

   if(command == "long_terms"){
      std::cout << "select long terms for fb algorithm\n";
      FIndexBuilder b;
      b.fetch_longer_terms();
   }

   if(command == "fetch_query_terms"){
      std::cout << "fetch query terms for non-indri inverted list\n";
      std::string queries(argv[2]);
      std::string terms(argv[3]);
      FIndexBuilder b;
      b.fetch_query_terms(queries, terms);
   }

   if(command == "qp_non_indri"){
     std::string order(argv[2]);
     std::string non_indri_lex(argv[3]);
     std::string non_indri_content(argv[4]);
     std::string data_size_s(argv[5]);
     std::string max_d_s(argv[6]);
     std::string order_path(argv[7]);
     std::string query(argv[8]);
     
     std::string::size_type sz;
     long long data_size = std::stoll(data_size_s, &sz);
     int max_d = std::stoi(max_d_s, &sz);
     
     FIndexBuilder b;
     b.qp_non_indri(order, non_indri_lex, non_indri_content, data_size, max_d, order_path, query); 
   }

   if(command == "verify_non_indri_term_freqs"){
      cout << "verify_non_indri_term_freqs\n";
      string indri_index(argv[2]);
      string non_indri_lex(argv[3]);
      string non_indri_content(argv[4]);
      string data_size_s(argv[5]);
      string tid_s(argv[6]);
      std::string::size_type sz;     // alias of size_t
      long long data_size = std::stoll(data_size_s, &sz);
      int tid_to_verify = std::stoi(tid_s, &sz);
      FIndexBuilder b;
      b.verify_non_indri_term_freqs(indri_index, non_indri_lex, non_indri_content, data_size, tid_to_verify);
   }

   if(command == "verify_non_indri_inverted_list"){
      cout << "verify_non_indri_inverted_list\n";
      string indri_index(argv[2]);
      string non_indri_lex(argv[3]);
      string non_indri_content(argv[4]);
      string data_size_s(argv[5]);
      string tid_s(argv[6]);
      std::string::size_type sz;     // alias of size_t
      long long data_size = std::stoll(data_size_s, &sz);
      int tid_to_verify = std::stoi(tid_s, &sz);
      FIndexBuilder b;
      b.verify_non_indri_inverted_list(indri_index, non_indri_lex, non_indri_content, data_size, tid_to_verify);
   }


   if(command == "write_non_indri_inverted_list"){
      cout << "write_non_indri_inverted_list\n";
      string indri_index(argv[2]);
      string terms(argv[3]);
      string content(argv[4]);
      string lex(argv[5]);
      /*string ss(argv[6]);
      string se(argv[7]);
      std::string::size_type sz;     // alias of size_t
      int start = std::stoi(ss, &sz);
      int end = std::stoi(se, &sz);*/
      FIndexBuilder b;
      b.write_non_indri_inverted_list(indri_index, terms, content, lex);
   }

   if(command == "write_non_indri_term_freqs"){
     cout << "write_non_indri_term_freqs\n";
     string indri_index(argv[2]);
     string terms(argv[3]);
     string content(argv[4]);
     string lex(argv[5]);
     FIndexBuilder b;
     b.write_non_indri_term_freqs(indri_index, terms, content, lex);
   }

   if(command == "write_inverted_list_ascii_url"){
      cout << "write_inverted_list_ascii_url\n";
      string indri_index(argv[2]);
      string terms(argv[3]);
      string ordering(argv[4]);
      string content(argv[5]);
      FIndexBuilder b;
      b.write_non_indri_inverted_lists_ascii_url(indri_index, terms, ordering, content);
   }

   if(command == "c_comp_query"){
      std::string ordering(argv[2]);
      std::cout << "compute gamma coding compression size with different ordering: " << ordering << "\n";
      FIndexBuilder b;
      b.compute_compression_stat_query_average(ordering);
   }

   if(command == "c_comp"){
      std::string ordering(argv[2]);
      std::cout << "compute gamma coding compression size with different ordering: " << ordering << "\n";
      FIndexBuilder b;
      b.compute_compression_stat(ordering);
   }

   if(command == "ana_gains"){
      std::cout << "analysis gains\n";
      std::string dir(argv[2]);
      OrderR r;
      r.analysis_gain(dir);
   }

   if(command == "o_pairs_with_probs") {
      std::cout << "read organized pairs\n";
      FIndexBuilder b;
      b.organized_pair_with_probs();
   }

   if(command == "compute_pair_score_with_same_weight"){
      std::cout << "compute_pair_score_with_same_weight\n";
      string pair_weighted_scores(argv[2]);
      string pair_unweighted_scores(argv[3]);
      FIndexBuilder b;
      b.compute_pair_score_with_same_weight(pair_weighted_scores, pair_unweighted_scores);
   }

   if(command == "organize_pairs") {
      std::cout << "organize pairs\n";
      string indri_index(argv[2]); 
      string pair_scores(argv[3]); 
      string lm_lex(argv[4]); 
      string lm_index(argv[5]);
      FIndexBuilder b;
      b.organize_pairs(indri_index, pair_scores, lm_lex, lm_index);
   }

   if(command == "compute_term_score"){
      std::cout << "compute term score\n";
      string indri_index(argv[2]);
      string unigrams(argv[3]);
      string multi_s(argv[4]);
      string term_with_score(argv[5]);
      std::string::size_type sz;
      double multi = std::stod(multi_s, &sz);
      FIndexBuilder b;
      b.compute_term_score(indri_index, unigrams, multi, term_with_score);
   }

   if(command == "compute_term_score_unweighted"){
      std::cout << "compute term score\n";
      string indri_index(argv[2]);
      string unigrams(argv[3]);
      string term_with_score(argv[4]);
      FIndexBuilder b;
      b.compute_term_score_unweighted(indri_index, unigrams, term_with_score);
   }

   if(command == "compute_pair_score") {
      std::cout << "compute pair score\n";
      std::string lm_pairs(argv[2]);
      string th(argv[3]);
      std::string mul(argv[4]);
      std::string pairs_with_score(argv[5]);
      std::string::size_type sz;     // alias of size_t
      double multi = std::stod(mul, &sz);
      double threshold = std::stod(th, &sz);
      FIndexBuilder b;
      b.compute_pair_score(lm_pairs, threshold, multi, pairs_with_score);
   }

   if(command == "s_unigram_prob"){
      std::cout << "show unigram probs\n";
      FIndexBuilder b;
      b.show_unigram_prob();
   }

   if(command == "s_pair_prob"){
      std::cout << "show pair probs\n";
      FIndexBuilder b;
      b.show_pair_prob();
   }

   if(command == "compute_pair_probs_from_unigrams_and_bigrams"){
      std::cout << "compute_pair_probs_from_unigrams_and_bigrams\n";
      string unigrams(argv[2]);
      string bigrams(argv[3]);
      string pairs(argv[4]);
      FIndexBuilder b;
      b.compute_pair_probs_from_unigrams_and_bigrams(unigrams, bigrams, pairs);
   }

   /*test coverage of the lm*/
   if(command == "lm_coverage"){
      std::cout << "lm coverage test\n";
      FIndexBuilder b;
      b.lm_coverage();
   }
   /*parse lm*/
   if(command == "parse_lm"){
      std::cout << "parse lm\n";
      string mit_lm(argv[2]);
      string unigrams(argv[3]);
      string bigrams(argv[4]);
      FIndexBuilder b;
      b.parse_lm(mit_lm, unigrams, bigrams);
   }

   /*test vbyte*/
   if(command == "test_vbyte"){
      std::cout << "test vbyte\n";
      FIndexBuilder b;
      b.test_vbyte();
   }

   /*inverted index stats all*/
   if(command == "term_lex"){
      std::cout << "get inverted index stats for all\n";
      std::string indri_index(argv[2]);
      std::string term_lex(argv[3]);
      FIndexBuilder b;
      b.print_vocabulary(indri_index, term_lex);
   }

   /*get inverted list content for a particular term*/
   if(command == "i_term"){
      std::string term(argv[2]);
      std::cout << "get inverted list content for term: " << term << endl;
      FIndexBuilder b;
      b.get_inverted_list(term);
   }

   /*order retriever on test 10k*/
   if(command == "or_test"){
      std::cout << "retrieving new order\n";
      OrderR r;
      r.init_test();
   }

   /*order retriever with fb algorithm*/
   if(command == "or_fb"){
      std::cout << "retrieving new order with fb\n";
      OrderR r;
      r.init_fb();
   }

   /*order retriever on whole gov2*/
   if(command == "or_gov2"){
      std::cout << "retrieving new order for gov2\n";
      OrderR r;
      r.init_gov2();
   }

   /*order retriever with real lm*/
   if(command == "or_bp"){
      std::cout << "retrieving new order with bp optimization algorithm\n";
      OrderR r;
      r.init_bp();
   }

   /*order retriever for stats*/
   if(command == "or_stats"){
      std::cout << "retrieving new order for stats\n";
      OrderR r;
      r.init_stats();
   }

   /*order retriever on a subset for test*/
   if(command == "or_subset"){
      std::cout << "debugging using subsets\n";
      OrderR r;
      r.init_subsets();
   }

   /*get random ordering for all gov2*/
   if(command == "get_randome_ordering"){
      std::cout << "get a random ordering for whole gov2\n";
      string url_lex(argv[2]);
      string random_order(argv[3]);
      FIndexBuilder b;
      b.get_random_order(url_lex, random_order);
   }

   /*get random ordering for 10k*/
   if(command == "ro_r10k"){
      std::cout << "get a random ordering for random 10k\n";
      FIndexBuilder b;
      b.get_random_order_r10k();
   }

   /*query processing m9*/
   // if(command == "qp_gov2") {
   //    std::cout << "run m9 test query processing with system ordering\n";
   //    FIndexBuilder b;
   //    b.query_processing_gov2();
   // }

   /*query processing m9 with ordering*/
   if(command == "query_processing_with_order") {
      std::string indri_index(argv[2]);
      std::string maxd_s(argv[3]);
      std::string order_type(argv[4]);
      std::string order_path(argv[5]);
      std::string query(argv[6]);
      std::cout << "run test query processing with ordering: " << order_type << "\n";
      std::string::size_type sz;     // alias of size_t
      int max_d = std::stoi(maxd_s, &sz);
      FIndexBuilder b;
      b.query_processing_ordering(order_type, indri_index, order_path, query, max_d);
   }

   /*query processing m9 for a single query with ordering*/
   if(command == "query_processing_single_query") {
      std::string indri_index(argv[2]);
      std::string maxd_s(argv[3]);
      std::string ordering(argv[4]);
      std::string order_path(argv[5]);
      std::string t1(argv[6]);
      std::string t2(argv[7]);
      std::cout << "run qp for pair " << t1 << " " << t2 << " using ordering: " << ordering << "\n";
      std::string::size_type sz;     // alias of size_t
      int max_d = std::stoi(maxd_s, &sz);
      FIndexBuilder b;
      b.query_processing_single_query(t1, t2, ordering, indri_index, order_path, max_d);
   }

   /*query processing*/
   // if(command == "q_p") {
   //    std::string ordering(argv[2]);
   //    std::cout << "run query processing with ordering: " << ordering << "\n";
   //    FIndexBuilder b;
   //    b.query_processing(ordering);
   // }

   /*random query generation on million09*/
   if(command == "rqg_m9") { // do p_m9 first
      std::cout << "get random 3k queries from parsed m9 query trace\n";
      FIndexBuilder b;
      b.random_query_generation_m9();
   }

   /*random query generation on tera 2006*/
   if(command == "10k_random_query_from_other") { // do p_m9 first
      std::cout << "get random 10k query from potential other query set\n";
      string potential_other(argv[2]);
      string potential(argv[3]);
      std::string::size_type sz;   // alias of size_t
      int potential_size = std::stoi(potential, &sz);
      string random(argv[4]);
      int random_size = std::stoi(random, &sz);
      string random_10k_other_set(argv[5]);
      FIndexBuilder b;
      b.select_random_10k_from_other_set(potential_other, potential_size, random_size, random_10k_other_set);
   }

   /*training testing query set construction*/
   if(command == "remove_qid_and_raw_query"){
      std::cout << "remove_qid_and_raw_query\n";
      std::string query_lex(argv[2]);
      std::string pair_input_for_lm(argv[3]);
      FIndexBuilder b;
      b.remove_qid_and_raw_query(query_lex, pair_input_for_lm);
   }

   /*training testing query set construction*/
   if(command == "query_set_construction"){
      std::cout << "query set construction using query lex\n";
      std::string query_lex(argv[2]);
      std::string training_set(argv[3]);
      std::string testing_set(argv[4]);
      std::string other_set(argv[5]);
      FIndexBuilder b;
      b.query_set_construction(query_lex, training_set, testing_set, other_set);
   }

   /*tera2006 query parsing*/
   if(command == "parse_tera2006") { // do i_all first
      std::cout << "query parsing for tera2006\n";
      std::string indri_index(argv[2]);
      std::string term_lex(argv[3]);
      std::string query(argv[4]);
      std::string parsed_query(argv[5]);
      FIndexBuilder b;
      b.parse_query_tera2006(indri_index, term_lex, query, parsed_query);
   }


   /*tera06_query_lex_generation*/
   if(command == "tera06_query_lex_generation") { // do i_all first
      std::cout << "query lex generation for million query trace\n";
      std::string indri_index(argv[2]);
      std::string term_lex(argv[3]);
      std::string query(argv[4]);
      std::string parsed_query(argv[5]);
      FIndexBuilder b;
      b.tera_query_lex_generation(indri_index, term_lex, query, parsed_query);
   }

   /*million_query_lex_generation*/
   if(command == "million_query_lex_generation") { // do i_all first
      std::cout << "query lex generation for million query trace\n";
      std::string indri_index(argv[2]);
      std::string term_lex(argv[3]);
      std::string query(argv[4]);
      std::string parsed_query(argv[5]);
      FIndexBuilder b;
      b.million_query_lex_generation(indri_index, term_lex, query, parsed_query);
   }

   /*million09 parsing*/
   if(command == "parse_million_query") { // do i_all first
      std::cout << "query parsing for million query trace\n";
      std::string indri_index(argv[2]);
      std::string term_lex(argv[3]);
      std::string query(argv[4]);
      std::string parsed_query(argv[5]);
      FIndexBuilder b;
      b.parse_query_million(indri_index, term_lex, query, parsed_query);
   }

   /*random query generation*/
   if(command == "rq_g") { 
      std::cout << "generate random queries\n";
      FIndexBuilder b;
      b.random_query_generation();
   }

   /*lookup a term*/
   if(command == "lookup_term") {
      std::string ps(argv[2]);
      std::string::size_type sz;   // alias of size_t
      int tid = std::stoi(ps, &sz);
      std::cout << "lookup term with tid: " << tid << "\n";
      FIndexBuilder b;
      b.lookup_term(tid);
   }

   /*lookup a tid*/
   if(command == "lookup_tid") {
      std::string term(argv[2]);
      std::cout << "lookup tid with term: " << term << "\n";
      FIndexBuilder b;
      b.lookup_tid(term);
   }

   /*add term for inverted lex*/
   if(command == "a_t") {
      std::cout << "add term for inverted index\n";
      FIndexBuilder b;
      b.add_term_to_lex();
   }

   /*inverted index for r10k*/
   if(command == "i_r10k"){
      std::cout << "get inverted index for r10k\n";
      FIndexBuilder b;
      b.inverted_index_r10k();
   }

   /*forward index for r10k*/
   if(command == "f_r10k"){
      std::cout << "get forward index for r10k\n";
      FIndexBuilder b;
      b.forward_index_r10k_url_ordering();
   }

   /*random 10k*/
   if(command == "r_10k_url"){
      std::cout << "random 10k small lex\n";
      FIndexBuilder b;
      b.random_10k_lex();
   }

   /*shuffle lex*/
   if(command == "s_l"){
      std::cout << "shuffle the whole lex\n";
      FIndexBuilder b;
      b.shuffle_all_lex();
   }

  /*get lex of certain doc ranges free memory*/
   if(command == "url_lex_doc_range_free_memory"){
      std::string indri_index(argv[2]);
      std::string output_lex(argv[3]);
      std::string sd(argv[4]);
      std::string ed(argv[5]);
      std::string::size_type sz;   // alias of size_t
      int s = std::stoi(sd, &sz);
      int e = std::stoi(ed, &sz);
      std::cout << "generate url lex for doc range: " << s << " to: " << e << "\n";
      std::cout << "read from indri index: " << indri_index << "\n";
      std::cout << "write to forward url lex: " << output_lex << "\n";
      FIndexBuilder b;
      b.forward_lex_using_doc_range_free_memory(s, e, indri_index, output_lex);
   }

   /*get lex of certain doc ranges*/
   if(command == "url_lex_doc_range"){
      std::string indri_index(argv[2]);
      std::string output_lex(argv[3]);
      std::string sd(argv[4]);
      std::string ed(argv[5]);
      std::string::size_type sz;   // alias of size_t
      int s = std::stoi(sd, &sz);
      int e = std::stoi(ed, &sz);
      std::cout << "generate url lex for doc range: " << s << " to: " << e << "\n";
      std::cout << "read from indri index: " << indri_index << "\n";
      std::cout << "write to forward url lex: " << output_lex << "\n";
      FIndexBuilder b;
      b.forward_lex_using_doc_range(s, e, indri_index, output_lex);
   }

   /*get lex in patch with dups*/
   if(command == "url_lex_by_patch"){
      std::string indri_index(argv[2]);
      std::string output_lex(argv[3]);
      std::string ps(argv[4]);
      std::string::size_type sz;   // alias of size_t
      int p = std::stoi(ps, &sz);
      std::cout << "forward index url lex patch: " << p << "\n";
      std::cout << "read from indri index: " << indri_index << "\n";
      std::cout << "write to forward url lex: " << output_lex << "\n";
      FIndexBuilder b;
      b.forward_lex_patch_with_dups(p, indri_index, output_lex);
   }

   /*verify vbyte forward index*/
   if(command == "verify_vbyte_forward_index"){
      std::cout << "verify vbyte forward index\n";
      std::string indri_index(argv[2]);
      std::string vbyte_lex(argv[3]);
      std::string vbyte_index(argv[4]);
      std::string maxds(argv[5]);
      std::string datas(argv[6]);
      std::string dids(argv[7]);
      std::string::size_type sz;   // alias of size_t
      int max_d = std::stoi(maxds, &sz);
      long long data_size = std::stoll(datas, &sz, 0);
      int did = std::stoi(dids, &sz);
      FIndexBuilder b;
      b.verify_vbyte_forward_index(indri_index, vbyte_lex, vbyte_index, max_d, data_size, did);
   }

   /*get forward index content in patch*/
   if(command == "vbyte_index_doc_range"){
      std::string indri_index(argv[2]);
      std::string vbyte_lex(argv[3]);
      std::string vbyte_index(argv[4]);
      std::string sd(argv[5]);
      std::string ed(argv[6]);
      std::string os(argv[7]);
      std::string::size_type sz;   // alias of size_t
      long long offset = std::stoll(os, &sz, 0);
      int s = std::stoi(sd, &sz);
      int e = std::stoi(ed, &sz);
      std::cout << "get forward index content for certain doc range\n";
      FIndexBuilder b;
      b.doc_content_using_doc_range(s, e, indri_index, vbyte_lex, vbyte_index, offset);
   }

   /*get forward index content in patch*/
   if(command == "vbyte_index_by_patch"){
      std::string indri_index(argv[2]);
      std::string vbyte_lex(argv[3]);
      std::string vbyte_index(argv[4]);
      std::string ps(argv[5]);
      std::string::size_type sz;   // alias of size_t
      int p = std::stoi(ps, &sz);
      std::string os(argv[6]);
      long long offset = std::stoll(os, &sz, 0);
      std::cout << "forward index content patch " << p << "\n";
      FIndexBuilder b;
      b.doc_content_patch(indri_index, vbyte_lex, vbyte_index, p, offset);
   }

   /*get doc length lex in patch*/
   if(command == "doc_length_lex_doc_range"){
      std::string indri_index(argv[2]);
      std::string doc_length_lex(argv[3]);
      std::string sd(argv[4]);
      std::string ed(argv[5]);
      std::string::size_type sz;   // alias of size_t
      int s = std::stoi(sd, &sz);
      int e = std::stoi(ed, &sz);
      std::cout << "get forward index content for certain doc range\n";
      FIndexBuilder b;
      b.doc_length_using_doc_range(s, e, indri_index, doc_length_lex);
   }

   /*test*/
   if(command == "test") {
      std::cout << "test\n";
      FIndexBuilder b;
      b.build();
   }

   /*show document content*/
   if(command == "show_doc") {
      std::string ps(argv[2]);
      std::string::size_type sz;   // alias of size_t
      int did = std::stoi(ps, &sz);
      std::cout << "show document content of did: " << did << "\n";
		FIndexBuilder b;
		b.show_documents(did);
   }

   if(command == "show_unique_tid_doc") {
      std::string ps(argv[2]);
      std::string::size_type sz;   // alias of size_t
      int did = std::stoi(ps, &sz);
      std::cout << "show document content of did: " << did << "\n";
      FIndexBuilder b;
      b.show_unique_tid_documents(did);
   }

   /*build forward index*/
   if(command == "f_t") {
      std::cout << "small forward index\n";
      FIndexBuilder b;
      b.forward_index_test();
   }

   /*generate postings*/
   if(command == "i_test") {
      std::cout << "small inverted index\n";
      FIndexBuilder b;
      b.inverted_index_test();
   }

   /*reverse query pair*/
   if(command == "r_pair") {
      std::cout << "reverse query pair\n";
      FIndexBuilder b;
      b.reverse_query_pair();
   }

   /*vldb revised*/
   /*stem_queries_generate_term_lex*/
   if (command == "stem_queries_generate_term_lex") {
      std::cout << "stem_queries_generate_term_lex\n";
      string indri_index(argv[2]); 
      string input_query(argv[3]);
      string stemmed_query(argv[4]);
      string stemmed_term_lex(argv[5]);
      FIndexBuilder b;
      b.stem_queries_generate_term_lex(indri_index, input_query, stemmed_query, stemmed_term_lex);
   }

   return 0;
}
