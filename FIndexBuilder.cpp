#include "FIndexBuilder.h"
#include <unordered_map>
using namespace indri::api;

template <typename Iterator>
void dumpToFile(const std::string& path, Iterator start, Iterator end) { ///home/qw376/SIGIR2017/data
  // FILE *fdo = fopen64(path.c_str(),"w");
  FILE *fdo = fopen(path.c_str(),"a+");
  if(! fdo) {
    std::cout << "Failed writing to " << path << " (Hint: create folder for path?)" << std::endl;
    return;
  }
  assert(fdo);
  for (; start !=end; ++start)
    if (fwrite(&(*start), sizeof(typename Iterator::value_type), 1, fdo) != 1)
      std::cout << "Failed writing to " << path << " (Hint: create folder for path?)" << std::endl;
  fclose(fdo);
}

bool sort_docs (docInfoR a, docInfoR b) { 
    if(a.part < b.part){
      return true;
    }else if(a.part > b.part){
      return false;
    }else{
      if(a.gain > b.gain){
        return true;
      }else{
        return false;
      }
    }
}

bool sort_docs_url (docInfoR a, docInfoR b) { 
    if(a.url < b.url){
      return true;
    }else{
      return false;
    }
}

bool sort_LMPair_by_prob (LMPair a, LMPair b) { 
    if(a.prob > b.prob){
      return true;
    }else{
      return false;
    }
}

bool sort_LMUnigram_by_prob (LMUnigram a, LMUnigram b) { 
    if(a.prob + a.backoff_prob > b.prob + b.backoff_prob){
      return true;
    }else{
      return false;
    }
}

bool sort_LMPair_by_terms (LMPairScore a, LMPairScore b) { 
    if(a.term1.compare(b.term1) < 0){
      return true;
    }else if(a.term1.compare(b.term1) > 0){
      return false;
    }else{
      if(a.term2.compare(b.term2) < 0){
        return true;
      }else{
        return false;
      }
    }
}

bool sort_LMPair_by_term_score (LMPairScore a, LMPairScore b) { 
    if(a.term1.compare(b.term1) < 0){
      return true;
    }else if(a.term1.compare(b.term1) > 0){
      return false;
    }else{
      if(a.score > b.score){
        return true;
      }else{
        return false;
      }
    }
}

bool sort_query_ranker_term (QueryRanker a, QueryRanker b) {
  if(a.term1.compare(b.term1) < 0){
    return true;
  }else{
    return false;
  }
}

bool sort_query_ranker_diff (QueryRanker a, QueryRanker b) {
  if(a.diff > b.diff) {
    return true;
  }else{
    return false;
  }
}

bool sort_query_ranker_ratio (QueryRanker a, QueryRanker b) {
  if(a.ratio > b.ratio) {
    return true;
  }else{
    return false;
  }
}

bool sort_query_ranker_length (QueryRanker a, QueryRanker b) {
  if(a.length_url < b.length_url) {
    return true;
  }else{
    return false;
  }
}

bool sort_query_ranker_qid (QueryRanker a, QueryRanker b) {
  if(a.qid < b.qid) {
    return true;
  }else{
    return false;
  }
}

void OrderR::analysis_gain(string dir){
  FILE* f = fopen(dir.c_str(), "r");
  int rank;
  float d1;
  float d2;
  float dt;

  int n_10m = 0;
  int n_1m = 0;
  int n_100k = 0;
  int n_10k = 0;
  int n_1k = 0;
  int n_100 = 0;
  int n_10 = 0;
  int n_1 = 0;
  int n_0 = 0;

  while(fscanf(f, "%d %f %f %f", &rank, &d1, &d2, &dt) == 4){
    // cout << rank << " " << dt << "\n";
    if(dt >= 10000000.00){
      n_10m++;
      continue;
    }
    if(dt >= 1000000.00){
      n_1m++;
      continue;
    }
    if(dt >= 100000.00){
      n_100k++;
      continue;
    }
    if(dt >= 10000.00){
      n_10k++;
      continue;
    }
    if(dt >= 1000.00){
      n_1k++;
      continue;
    }
    if(dt >= 100.00){
      n_100++;
      continue;
    }
    if(dt >= 10.00){
      n_10++;
      continue;
    }
    if(dt >= 1.00){
      n_1++;
      continue;
    }
    if(dt >= 0.00){
      n_0++;
      continue;
    }
  }
  cout << "more than 10 million: " << n_10m << "\n";
  cout << "more than 1 million: " << n_1m << "\n";
  cout << "more than 100k: " << n_100k << "\n";
  cout << "more than 10k: " << n_10k << "\n";
  cout << "more than 1k: " << n_1k << "\n";
  cout << "more than 100: " << n_100 << "\n";
  cout << "more than 10: " << n_10 << "\n";
  cout << "more than 1: " << n_1 << "\n";
  cout << "more than 0: " << n_0 << "\n";
  fclose(f);
}

void OrderR::increase_count(vector<lexInfoR>& lex, int tid) {  
  if(lex[tid-1].id == current_ID) {
    lex[tid-1].count++;
  }else{
    lex[tid-1].id = current_ID;
    lex[tid-1].count = 1;
    lex[tid-1].gain = -10000.00;
  }
}

void OrderR::increase_count_with_indicator(vector<lexInfoR>& lex, int tid) {  
  if(lex[tid-1].id == current_ID) {
    lex[tid-1].count++;
  }else{
    lex[tid-1].id = current_ID;
    lex[tid-1].count = 1;
    lex[tid-1].gain = 0.0;
    lex[tid-1].indicator = false;
  }
}

int OrderR::get_count(vector<lexInfoR>& lex, int tid) {
  if(lex[tid-1].id == current_ID){
    return lex[tid-1].count;
  }else{
    return 0;
  }
}

/* compute the expected number of breakpoints when randomly 
   interleaving p postings of one term and q of the other */
double OrderR::exp_bp(int p, int q){
  if ((p <= 0) || (q <= 0)) return 0.0;
  return((double)(2*p*q)/(double)(p+q-1));  
}

double OrderR::exp_bp_no_minus_one(int p, int q){
  if ((p <= 0) || (q <= 0)) return 0.0;
  return((double)(2*p*q)/(double)(p+q));  
}

double OrderR::exp_fb(int fs, int ts, int fc, int tc){
  if( (fs == 0) || (ts == 0)){
    std::cout << "fs or ts should not be zero in any case\n";
    exit(0);
  }
  if( (fc < 0) || (tc < 0) ){
    std::cout << "fc or tc should not be less than zero in any case\n";
    exit(0);
  }
  return((double)fc * log((double)fs / ((double)fc+1.0)) + (double)tc * log((double)ts / ((double)tc+1.0)));
}

void OrderR::get_bp_gain(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc){
  double gain = 0;
  int tfrom, tto, t2from, t2to;
  /*  fetch terms */
  std::vector<int> termIds = forward_index_map[doc.did];
  /*  loop over all terms */
  for(int i = 0; i < termIds.size(); i++){
    int tid = termIds[i];
    // cout << "tid1: " << tid << " ";
    if(from_lex[tid-1].gain <= -10000.00) {

      /*get count for from_lex of tid1*/
      tfrom = get_count(from_lex, tid);

      /*get count for to_lex of tid1*/
      tto = get_count(to_lex, tid);

      /* now loop over terms t2 until pr[t2] becomes less than threshold */
      auto it = lm_map.find(tid);
      if(it!=lm_map.end()){
        int tid2 = lm_map[tid];
        // cout << "in lm, tid2: " << tid2 << " ";
        /*get count for from lex of tid2*/
        t2from = get_count(from_lex, tid2);

        /*get count for to lex of tid2*/
        t2to = get_count(to_lex, tid2);
        // cout << tfrom << " " << tto << " " << t2from << " " << t2to << " ";

        /* add current expected number of breakpoints */ 
        gain += exp_bp(tfrom, t2from) + exp_bp(tto, t2to);

        /* deduct expected number of breakpoints after moving document */ 
        gain -= exp_bp(tfrom-1, t2from) + exp_bp(tto+1, t2to);

        // cout << "gain: " << gain << " "; 
      }
    }
    // cout << endl;
  }
  doc.gain = gain;
}

void OrderR::get_bp_gain_fb(vector<lexInfoR>& from_lex, int from_size, vector<lexInfoR>& to_lex, int to_size, docInfoR& doc){
  double doc_gain = 0.0;
  double term_gain = 0.0;
  int tfrom, tto, t2from, t2to;
  /*  fetch terms */
  int d = doc.did;
  int chunk_size = sizes[d];
  int length = lengths[d];
  long long offset = offsets[d];
  int fc, tc;
  char* d_data = new char[chunk_size];
  int* uncompressed_data = new int[length];
  for(int j = 0; j < chunk_size; j++){
    d_data[j] = data[offset+j];
  }

  decompressionVbytes(d_data, uncompressed_data, length);

  for(int j = 1; j < length; j++){
    uncompressed_data[j]+= uncompressed_data[j-1]; 
  }

  /*  loop over all terms */
  for(int i = 0; i < length; i++){
    int tid = uncompressed_data[i];
    // cout << "tid1: " << tid << " ";
    if(from_lex[tid-1].indicator == false) {
        term_gain = 0;
        /*get count for from_lex of tid1*/
        fc = get_count(from_lex, tid);

        /*get count for to_lex of tid1*/
        tc = get_count(to_lex, tid);

        /* first add current estimated compressed size of list */
        // term_gain += fc * log(fs / (fc+1.0)) + tc * log(ts / (tc+1.0));
        term_gain += exp_fb(from_size, to_size, fc, tc);

        /* then deduct compressed size after moving document */
        // term_gain -= fc * log(fs / (fc)) + tc * log(ts / (tc+2.0));
        term_gain -= exp_fb(from_size, to_size, fc-1, tc+1);

        from_lex[tid-1].gain = term_gain;
        from_lex[tid-1].indicator = true;
    }
    doc_gain += from_lex[tid-1].gain;
  }
  doc.gain = doc_gain * (double)1000000.00;
  delete[] d_data;
  delete[] uncompressed_data;
}

void OrderR::get_fb_gain_debugging(vector<lexInfoR>& from_lex, int from_size, vector<lexInfoR>& to_lex, int to_size, docInfoR& doc, FILE* file){
  double doc_gain = 0.0;
  double term_gain = 0.0;
  int tfrom, tto, t2from, t2to;
  /*  fetch terms */
  int d = doc.did;
  if(d == 7171143) {
    fprintf(file, "did: %d\n", d);
    int chunk_size = sizes[d];
    int length = lengths[d];
    long long offset = offsets[d];
    int fc, tc;
    char* d_data = new char[chunk_size];
    int* uncompressed_data = new int[length];
    for(int j = 0; j < chunk_size; j++){
      d_data[j] = data[offset+j];
    }

    decompressionVbytes(d_data, uncompressed_data, length);

    for(int j = 1; j < length; j++){
      uncompressed_data[j]+= uncompressed_data[j-1]; 
    }

    /*  loop over all terms */
    for(int i = 0; i < length; i++){
      int tid = uncompressed_data[i];
      // cout << "tid1: " << tid << " ";
      if(from_lex[tid-1].indicator == false) {
          term_gain = 0;
          /*get count for from_lex of tid1*/
          fc = get_count(from_lex, tid);

          /*get count for to_lex of tid1*/
          tc = get_count(to_lex, tid);

          /* first add current estimated compressed size of list */
          // term_gain += fc * log(fs / (fc+1.0)) + tc * log(ts / (tc+1.0));
          double tg = exp_fb(from_size, to_size, fc, tc);
          term_gain += tg;

          /* then deduct compressed size after moving document */
          // term_gain -= fc * log(fs / (fc)) + tc * log(ts / (tc+2.0));
          double fg = exp_fb(from_size, to_size, fc-1, tc+1);
          term_gain -= fg;

          fprintf(file, "tid %d from_count %d to_count %d from_size %d to_size %d original_gain %f change_gain %f total_gain %f\n", tid, fc, tc, from_size, to_size, tg, -fg, term_gain);

          from_lex[tid-1].gain = term_gain;
          from_lex[tid-1].indicator = true;
      }
      doc_gain += from_lex[tid-1].gain;
    }
    fprintf(file, "doc_gain: %f\n", doc_gain);
    doc.gain = doc_gain * (double)1000000.00;
    delete[] d_data;
    delete[] uncompressed_data;
  }
}

void OrderR::get_bp_gain_gov2(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc){
  double gain = 0;
  int tfrom, tto, t2from, t2to;
  /*  fetch terms */
  int d = doc.did;
  int chunk_size = sizes[d];
  int length = lengths[d];
  long long offset = offsets[d];
  char* d_data = new char[chunk_size];
  int* uncompressed_data = new int[length];
  for(int j = 0; j < chunk_size; j++){
    d_data[j] = data[offset+j];
  }

  decompressionVbytes(d_data, uncompressed_data, length);

  for(int j = 1; j < length; j++){
    uncompressed_data[j]+= uncompressed_data[j-1]; 
  }

  /*  loop over all terms */
  for(int i = 0; i < length; i++){
    int tid = uncompressed_data[i];
    // cout << "tid1: " << tid << " ";
    if(from_lex[tid-1].gain <= -10000.00) {

      /*get count for from_lex of tid1*/
      tfrom = get_count(from_lex, tid);

      /*get count for to_lex of tid1*/
      tto = get_count(to_lex, tid);

      /*now loop over terms t2 until pr[t2] becomes less than threshold*/
      auto it = lm_map.find(tid);
      if(it!=lm_map.end()){
        int tid2 = lm_map[tid];
        // cout << "in lm, tid2: " << tid2 << " ";
        /*get count for from lex of tid2*/
        t2from = get_count(from_lex, tid2);

        /*get count for to lex of tid2*/
        t2to = get_count(to_lex, tid2);
        // cout << tfrom << " " << tto << " " << t2from << " " << t2to << " ";

        /* add current expected number of breakpoints */ 
        gain += exp_bp(tfrom, t2from) + exp_bp(tto, t2to);

        /* deduct expected number of breakpoints after moving document */ 
        gain -= exp_bp(tfrom-1, t2from) + exp_bp(tto+1, t2to);

        // cout << "gain: " << gain << " "; 
      }
    }
    // cout << endl;
  }
  doc.gain = gain;
  delete[] d_data;
  delete[] uncompressed_data;
}

void OrderR::get_bp_gain_debugging(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc){
  double term_gain = 0;
  double doc_gain = 0;
  int tfrom, tto, t2from, t2to;
  /*  fetch terms */
  int d = doc.did;
  if((d == 682017)||(d == 10638945)) {
    cout << "document: " << d << "\n";
  }
  int chunk_size = sizes[d];
  int length = lengths[d];
  long long offset = offsets[d];
  char* d_data = new char[chunk_size];
  int* uncompressed_data = new int[length];
  for(int j = 0; j < chunk_size; j++){
    d_data[j] = data[offset+j];
  }

  decompressionVbytes(d_data, uncompressed_data, length);

  for(int j = 1; j < length; j++){
    uncompressed_data[j]+= uncompressed_data[j-1]; 
  }

  /*  loop over all terms */
  for(int i = 0; i < length; i++){
    int tid = uncompressed_data[i];
    // cout << "tid1: " << tid << " ";
    if(from_lex[tid-1].indicator == false) {
      term_gain = 0;
      /*get count for from_lex of tid1*/
      tfrom = get_count(from_lex, tid);

      /*get count for to_lex of tid1*/
      tto = get_count(to_lex, tid);

      /*now loop over terms t2 until pr[t2] becomes less than threshold*/
      auto it = lm_term_map.find(tid);
      if(it!=lm_term_map.end()){
        int length_lm = it->second.first;
        int offset_lm = it->second.second;

        for(int k = 0; k < length_lm; k++){
          int tid2 = lm_content_vec[offset_lm+k].first;
          int score = lm_content_vec[offset_lm+k].second;
          // cout << "tid2: " << tid2 << " ";
          /*get count for from lex of tid2*/
          t2from = get_count(from_lex, tid2);

          /*get count for to lex of tid2*/
          t2to = get_count(to_lex, tid2);
          // if(tid == 754){
          //   cout << "score: " << score << " tid1: " << tid << " t1from: " << tfrom << " t1to: " << tto << " tid2: " << tid2 << " t2from: " << t2from << " t2to: " << t2to << "\n";
          // }

          /* add current expected number of breakpoints */ 
          term_gain += (double)score * (exp_bp(tfrom, t2from) + exp_bp(tto, t2to));

          /* deduct expected number of breakpoints after moving document */ 
          term_gain -= (double)score * (exp_bp(tfrom-1, t2from) + exp_bp(tto+1, t2to));

          // cout << "gain: " << gain << " "; 
        }
      }
      from_lex[tid-1].gain = term_gain;
      from_lex[tid-1].indicator = true;
    }
    // cout << endl;
    if( (d == 682017) && (from_lex[tid-1].indicator == true) && (from_lex[tid-1].gain != 0)) {
      cout << "(" << tid << " " << from_lex[tid-1].gain << ")\n";
      // cout << from_lex[tid-1].gain << " ";
    }

    if( (d == 10638945) && (from_lex[tid-1].indicator == true) && (from_lex[tid-1].gain != 0)) {
      cout << "(" << " " << tid << " " << from_lex[tid-1].gain << ")\n";
      // cout << from_lex[tid-1].gain << " ";
    }
    doc_gain += from_lex[tid-1].gain;
  }
  // cout << "\n";
  doc.gain = doc_gain;
  if((d == 682017)||(d == 10638945)) {
    cout << "total gain for did: " << d << " gain: " << doc_gain << "\n";
  }
  delete[] d_data;
  delete[] uncompressed_data;
}

void OrderR::get_bp_gain_using_lm(vector<lexInfoR>& from_lex, vector<lexInfoR>& to_lex, docInfoR& doc){
  double term_gain = 0;
  double doc_gain = 0;
  int tfrom, tto, t2from, t2to;
  /*  fetch terms */
  int d = doc.did;
  int chunk_size = sizes[d];
  int length = lengths[d];
  long long offset = offsets[d];
  char* d_data = new char[chunk_size];
  int* uncompressed_data = new int[length];
  for(int j = 0; j < chunk_size; j++){
    d_data[j] = data[offset+j];
  }

  decompressionVbytes(d_data, uncompressed_data, length);

  for(int j = 1; j < length; j++){
    uncompressed_data[j]+= uncompressed_data[j-1]; 
  }

  /*  loop over all terms */
  for(int i = 0; i < length; i++){
    int tid = uncompressed_data[i];
    // cout << "tid1: " << tid << " ";
    if(from_lex[tid-1].indicator == false) {
      term_gain = 0;
      /*get count for from_lex of tid1*/
      tfrom = get_count(from_lex, tid);

      /*get count for to_lex of tid1*/
      tto = get_count(to_lex, tid);

      /*now loop over terms t2 until pr[t2] becomes less than threshold*/
      auto it = lm_term_map.find(tid);
      if(it!=lm_term_map.end()){
        int length_lm = it->second.first;
        int offset_lm = it->second.second;

        for(int k = 0; k < length_lm; k++){
          int tid2 = lm_content_vec[offset_lm+k].first;
          int score = lm_content_vec[offset_lm+k].second;
          // cout << "tid2: " << tid2 << " ";
          /*get count for from lex of tid2*/
          t2from = get_count(from_lex, tid2);

          /*get count for to lex of tid2*/
          t2to = get_count(to_lex, tid2);
          // cout << tfrom << " " << tto << " " << t2from << " " << t2to << " ";

          /* add current expected number of breakpoints */ 
          term_gain += (double)score * (exp_bp(tfrom, t2from) + exp_bp(tto, t2to));

          /* deduct expected number of breakpoints after moving document */ 
          term_gain -= (double)score * (exp_bp(tfrom-1, t2from) + exp_bp(tto+1, t2to));

          // cout << "gain: " << gain << " "; 
        }
      }
      from_lex[tid-1].gain = term_gain;
      from_lex[tid-1].indicator = true;
    }
    // cout << endl;
    doc_gain += from_lex[tid-1].gain;
  }
  doc.gain = doc_gain;
  // cout << "total gain for did: " << d << " gain: " << gain << "\n";
  delete[] d_data;
  delete[] uncompressed_data;
}

void OrderR::partition_identify_dids(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= 500) {
    for(int i=0; i<docs.size(); i++){
      fprintf(file, "%d ", docs[i].did);
    }
    fprintf(file, "\n");
  }

  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;

      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      // fprintf(file, "%d ", m);
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_identify_dids(docs1, left_lex, right_lex, file);
  partition_identify_dids(docs2, left_lex, right_lex, file);

}

void OrderR::partition_doc_tracker(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_debugging(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_debugging(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      // for(int k = 0; k < docs.size(); k++) {
      //   cout << docs[k].part << " " << docs[k].did << " " << docs[k].gain << "\n";
      // }

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        // cout << "( " << docs[m].did << " " << docs[size/2 + m].did << " )\n";
        if( (docs[m].did == 682017) || (docs[size/2 + m].did == 682017) ){
          cout << "did: " << 682017 << " rank: " << m << "\n";
        }

        if( (docs[m].did == 10638945) || (docs[size/2 + m].did == 10638945) ){
          cout << "did: " << 10638945 << " rank: " << m << "\n";
        }

        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      fprintf(file, "%d ", m);
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  fprintf(file, "\n");
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

}

void OrderR::partition_fb_doc_tracker(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  // sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  // for(int i = 0; i < round; i++){
  for(int i = 0; i < 1; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_fb_gain_debugging(left_lex, size/2, right_lex, size-size/2, docs[k], file);
      }
      for(int k = size/2; k < size; k++) {
         get_fb_gain_debugging(right_lex, size-size/2, left_lex, size/2, docs[k], file);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      // for(int k = 0; k < docs.size(); k++) {
      //   cout << docs[k].part << " " << docs[k].did << " " << docs[k].gain << "\n";
      // }

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        // cout << "( " << docs[m].did << " " << docs[size/2 + m].did << " )\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
  }
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

}

void OrderR::partition_fb_identify_dids(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= 1000) {
    fprintf(file, "%lu: ", docs.size());
    for(int i=0; i<docs.size(); i++){
      fprintf(file, "%d ", docs[i].did);
    }
    fprintf(file, "\n");
  }

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_fb(left_lex, size/2, right_lex, size-size/2, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_fb(right_lex, size-size/2, left_lex, size/2, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      // fprintf(file, "%d ", m);
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  // fprintf(file, "\n");
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_fb_identify_dids(docs1, left_lex, right_lex, file);
  partition_fb_identify_dids(docs2, left_lex, right_lex, file);

}

void OrderR::partition_fb_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  int size = docs.size();
  fprintf(file, "%d: ", size);
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_fb(left_lex, size/2, right_lex, size-size/2, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_fb(right_lex, size-size/2, left_lex, size/2, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      fprintf(file, "%d ", m);
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  fprintf(file, "\n");
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_fb_lvl_stats(docs1, left_lex, right_lex, file);
  partition_fb_lvl_stats(docs2, left_lex, right_lex, file);

}

void OrderR::partition_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  int size = docs.size();
  fprintf(file, "size: %d ", size);
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      fprintf(file, "%d ", m);
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  fprintf(file, "\n");
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_lvl_stats(docs1, left_lex, right_lex, file);
  partition_lvl_stats(docs2, left_lex, right_lex, file);

}

void OrderR::partition_fb_gain_analysis(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;
  vector<double> gains;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* randomly assign half of docs to left half (part[i] = 0) or right half (part[i] = 1) */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  string base = "/home/qi/reorder_data/log/gains_fb_0410_";
  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
      string dir = base + to_string(i);
      cout << dir << "\n";
      FILE* file = fopen(dir.c_str(), "w");
  // for(int i = 0; i < 1; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_fb(left_lex, size/2, right_lex, size-size/2, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_fb(right_lex, size-size/2, left_lex, size/2, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m+1 << " document(left and right) has gain: " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        fprintf(file, "%d %d %lf %d %lf %lf\n", m+1, docs[m].did, docs[m].gain, docs[size/2 + m].did, docs[size/2 + m].gain, docs[m].gain + docs[size/2 + m].gain);
        // cout << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
      fclose(file);
  }

  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }
}

void OrderR::partition_gain_analysis(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;
  vector<double> gains;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* randomly assign half of docs to left half (part[i] = 0) or right half (part[i] = 1) */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  string base = "/home/qi/reorder_data/log/gains_bp_0415_";
  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
      string dir = base + to_string(i);
      cout << dir << "\n";
      FILE* file = fopen(dir.c_str(), "w");
  // for(int i = 0; i < 1; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m+1 << " document(left and right) has gain: " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        if(m == 0){
          fprintf(file, "%d %d %lf %d %lf %lf\n", m+1, docs[m].did, docs[m].gain, docs[size/2 + m].did, docs[size/2 + m].gain, docs[m].gain + docs[size/2 + m].gain);
        }
        // cout << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
      fclose(file);
  }

  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

}

void OrderR::partition_lm_no_presorting(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  if(docs.size() <= threshold){
    sort(docs.begin(), docs.end(), sort_docs_url);
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* randomly assign half of docs to left half (part[i] = 0) or right half (part[i] = 1) */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size; i++) { 
    docs[i].part = 1; 
    docs[i].gain = 0.0; 
  }

  srand (time(NULL));
  for (int c = 0; c < size/2;){
    int j = rand() % size;
    // cout << j << "\n";
    if (docs[j].part == 1) { docs[j].part = 0; c++; }
  }

  /* next, move documents to their partitions by sorting */
  sort(docs.begin(), docs.end(), sort_docs);

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }

  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_lm_no_presorting(docs1, left_lex, right_lex);
  partition_lm_no_presorting(docs2, left_lex, right_lex);

}

void OrderR::partition_lm_sort_percentile_random_pairs(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* sort by url */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        // tmp = docs[m];
        // docs[m] = docs[size/2+m];
        // docs[size/2 + m] = tmp;

        // docs[m].part = 0;
        // docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      int half_m = m;
      /*50%*/
      // if (m > 1){
      //    half_m = m/2;
      // }

      /*60%*/
      if (m > 2){
         half_m = (m*3)/5;
      }

      srand (time(NULL));
      std::vector<int> selected1(m, 0);
      std::vector<int> selected2(m, 0);
      for(int k = 0; k < half_m;) {
        int l = rand() % m;
        if(selected1[l] == 0){
          int j = rand() % m;    
          if(selected2[j] == 0){
            tmp = docs[l];
            docs[l] = docs[size/2+j];
            docs[size/2 + j] = tmp;

            docs[l].part = 0;
            docs[size/2 + j].part = 1;

            k++;
            selected1[l] = 1;
            selected2[j] = 1;
          }
        }
      }


      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_lm_sort_percentile_random_pairs(docs1, left_lex, right_lex);
  partition_lm_sort_percentile_random_pairs(docs2, left_lex, right_lex);

}

void OrderR::partition_lm_sort_percentile(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* sort by url */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        // tmp = docs[m];
        // docs[m] = docs[size/2+m];
        // docs[size/2 + m] = tmp;

        // docs[m].part = 0;
        // docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /*60%*/
      // if (m > 2){
      //    m = (m*3)/5;
      // }

      /*50%*/
      // if (m > 1){
      //    m = m/2;
      // }

      for(int k = 0; k < m; k++) {
        tmp = docs[k];
        docs[k] = docs[size/2+k];
        docs[size/2 + k] = tmp;

        docs[k].part = 0;
        docs[size/2 + k].part = 1;
      }


      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_lm_sort_percentile(docs1, left_lex, right_lex);
  partition_lm_sort_percentile(docs2, left_lex, right_lex);

}


void OrderR::partition_lm_sort_percentile_raondom_pairs_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  int size = docs.size();
  fprintf(file, "size %d: ", size);
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        // tmp = docs[m];
        // docs[m] = docs[size/2+m];
        // docs[size/2 + m] = tmp;

        // docs[m].part = 0;
        // docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      int half_m = m;
      if (m > 1){
         half_m = m/2;
      }

      srand (time(NULL));
      std::vector<int> selected1(m, 0);
      std::vector<int> selected2(m, 0);
      for(int k = 0; k < half_m;) {
        int l = rand() % m;
        if(selected1[l] == 0){
          int j = rand() % m;    
          if(selected2[j] == 0){
            tmp = docs[l];
            docs[l] = docs[size/2+j];
            docs[size/2 + j] = tmp;

            docs[l].part = 0;
            docs[size/2 + j].part = 1;

            k++;
            selected1[l] = 1;
            selected2[j] = 1;
          }
        }
      }


      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      fprintf(file, "%d ", m);
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  fprintf(file, "\n");
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_lm_sort_percentile_raondom_pairs_lvl_stats(docs1, left_lex, right_lex, file);
  partition_lm_sort_percentile_raondom_pairs_lvl_stats(docs2, left_lex, right_lex, file);

}

void OrderR::partition_lm_sort_percentile_lvl_stats(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex, FILE* file){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    exit(0);
    return;
  }

  /* sort by url */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        // tmp = docs[m];
        // docs[m] = docs[size/2+m];
        // docs[size/2 + m] = tmp;

        // docs[m].part = 0;
        // docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      if (m > 1){
         m = m/2;
      }

      for(int k = 0; k < m; k++) {
        tmp = docs[k];
        docs[k] = docs[size/2+k];
        docs[size/2 + k] = tmp;

        docs[k].part = 0;
        docs[size/2 + k].part = 1;
      }


      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      fprintf(file, "%d ", m);
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }
  fprintf(file, "\n");
  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_lm_sort_percentile_lvl_stats(docs1, left_lex, right_lex, file);
  partition_lm_sort_percentile_lvl_stats(docs2, left_lex, right_lex, file);

}

void OrderR::partition_lm_sort_url(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;

  //sort the dids here by url
  sort(docs.begin(), docs.end(), sort_docs_url);

  if(docs.size() <= threshold){
    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* sort by url */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size/2; i++) { 
    docs[i].part = 0; 
    docs[i].gain = 0.0; 
  }

  /*divide the documents by sorting*/
  for (int i = size/2; i < size; i++){
    docs[i].part = 1;
    docs[i].gain = 0.0; 
  }

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
  // for(int i = 0; i < 2000; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_using_lm(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_using_lm(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 10); m++)
      { 
        // cout << "# " << m << " document(left and right) has gain: " << docs[m].did << " " << docs[m].gain << " " << docs[size/2 + m].gain
        // << " total gain " << docs[m].gain + docs[size/2 + m].gain << "\n";
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }

  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_lm_sort_url(docs1, left_lex, right_lex);
  partition_lm_sort_url(docs2, left_lex, right_lex);

}

void OrderR::partition_fb(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  docInfoR tmp;
  if(docs.size() <= threshold){
    //sort the dids here by url
    sort(docs.begin(), docs.end(), sort_docs_url);

    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* randomly assign half of docs to left half (part[i] = 0) or right half (part[i] = 1) */
  int size = docs.size();
  cout << "number of documents(left+right) at this level: " << size << "\n";
  for (int i = 0; i < size; i++) { 
    docs[i].part = 1; 
    docs[i].gain = 0.0; 
  }

  srand (time(NULL));
  for (int c = 0; c < size/2;){
    int j = rand() % size;
    // cout << j << "\n";
    if (docs[j].part == 1) { docs[j].part = 0; c++; }
  }

  /* next, move documents to their partitions by sorting */
  sort(docs.begin(), docs.end(), sort_docs);

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count_with_indicator(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_fb(left_lex, size/2, right_lex, size-size/2, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_fb(right_lex, size-size/2, left_lex, size/2, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      {
        tmp = docs[m];
        docs[m] = docs[size/2+m];
        docs[size/2 + m] = tmp;

        docs[m].part = 0;
        docs[size/2 + m].part = 1;
      }
   
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      // cout << "current_ID: " << current_ID << "\n";
      cout << "current_ID: " << current_ID << " number of document pairs swapped: " << m <<"\n";
  }

  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_fb(docs1, left_lex, right_lex);
  partition_fb(docs2, left_lex, right_lex);

}

void OrderR::partition_gov2(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  int d, length, chunk_size;
  long long offset;
  if(docs.size() <= threshold){
    //sort the dids here by url
    sort(docs.begin(), docs.end(), sort_docs_url);

    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* randomly assign half of docs to left half (part[i] = 0) or right half (part[i] = 1) */
  int size = docs.size();
  for (int i = 0; i < size; i++) { 
    docs[i].part = 1; 
    docs[i].gain = 0.0; 
  }

  srand (time(NULL));
  for (int c = 0; c < size/2;){
    int j = rand() % size;
    // cout << j << "\n";
    if (docs[j].part == 1) { docs[j].part = 0; c++; }
  }

  /* next, move documents to their partitions by sorting */
  sort(docs.begin(), docs.end(), sort_docs);

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count(left_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        d = docs[k].did;
        chunk_size = sizes[d];
        length = lengths[d];
        offset = offsets[d];
        char* d_data = new char[chunk_size];
        int* uncompressed_data = new int[length];
        for(int j = 0; j < chunk_size; j++){
          d_data[j] = data[offset+j];
        }

        decompressionVbytes(d_data, uncompressed_data, length);

        for(int j = 1; j < length; j++){
          uncompressed_data[j]+= uncompressed_data[j-1]; 
        }

        for(int j = 0; j < length; j++) {
          // cout << uncompressed_data[j] << " ";
          increase_count(right_lex, uncompressed_data[j]);
        }
        // cout << "\n";
        delete[] d_data;
        delete[] uncompressed_data;
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain_gov2(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain_gov2(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      int tmp = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      {
        tmp = docs[m].did;
        docs[m].did = docs[size/2+m].did;
        docs[size/2 + m].did = tmp;
      }
   
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << "\n";
  }

  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition_gov2(docs1, left_lex, right_lex);
  partition_gov2(docs2, left_lex, right_lex);

}

void OrderR::partition(vector<docInfoR>& docs, vector<lexInfoR>& left_lex, vector<lexInfoR>& right_lex){
  if(docs.size() <= threshold){
    //sort the dids here by url
    sort(docs.begin(), docs.end(), sort_docs_url);

    //out put them to some global structure
    for(int i=0; i<docs.size(); i++){
      final_order.push_back(docs[i].did);
    }
    return;
  }

  /* randomly assign half of docs to left half (part[i] = 0) or right half (part[i] = 1) */
  int size = docs.size();
  for (int i = 0; i < size; i++) { 
    docs[i].part = 1; 
    docs[i].gain = 0.0; 
  }

  srand (time(NULL));
  for (int c = 0; c < size/2;){
    int j = rand() % size;
    // cout << j << "\n";
    if (docs[j].part == 1) { docs[j].part = 0; c++; }
  }

  /* next, move documents to their partitions by sorting */
  sort(docs.begin(), docs.end(), sort_docs);

  // for(int i = 0; i < docs.size(); i++) {
  //   std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* iterate 20 times as suggested in paper */
  for(int i = 0; i < round; i++){
      /* now count number of term occurrences in left partition */
      for(int k = 0; k < size/2; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        std::vector<int> termIds = forward_index_map[docs[k].did];

        for(int j = 0; j < termIds.size(); j++) {
          // cout << termIds[j] << " ";
          increase_count(left_lex, termIds[j]);
        }
        // cout << "\n";
      }
      /* now count number of term occurrences in right partition */
      for(int k = size/2; k < size; k++) {
        /*fetch the terms*/
        // std::cout << k << " " << docs[k].did << "\n";
        std::vector<int> termIds = forward_index_map[docs[k].did];

        for(int j = 0; j < termIds.size(); j++) {
          // cout << termIds[j] << " ";
          increase_count(right_lex, termIds[j]);
        }
        // cout << "\n";
      }

      /* now for each document compute gain of going to other partition */
      for(int k = 0; k < size/2; k++) {
         get_bp_gain(left_lex, right_lex, docs[k]);
      }
      for(int k = size/2; k < size; k++) {
         get_bp_gain(right_lex, left_lex, docs[k]);
      }

      /* next, need to sort docs in each partition by gain */
      sort(docs.begin(), docs.end(), sort_docs);

      /* now, swap documents until gain less than zero */
      int m = 0;
      int tmp = 0;
      for (m = 0; (m < size/2) && (docs[m].gain + docs[size/2 + m].gain > 0); m++)
      {
        tmp = docs[m].did;
        docs[m].did = docs[size/2+m].did;
        docs[size/2 + m].did = tmp;
      }
   
      /* if nothing was swapped in this iteration, no need for another one */
      if (m == 0) break;

      /* reset leftLex and rightLex lexicon structures to zero */
      current_ID++;
      cout << "current_ID: " << current_ID << "\n";
  }

  // for(int i = 0; i < docs.size(); i++) {
  //     std::cout << docs[i].did << " " << docs[i].part << " " << docs[i].gain << "\n";
  // }

  /* we have fixed the partition on this level. Now recurse */
  vector<docInfoR> docs1;
  vector<docInfoR> docs2;
  for(int k = 0; k < size/2; k++) {
    docs1.push_back(docs[k]);
  }
  for(int k = size/2; k < size; k++) {
    docs2.push_back(docs[k]);
  }

  partition(docs1, left_lex, right_lex);
  partition(docs2, left_lex, right_lex);

}

void OrderR::init_subsets(){
    /*load forward index*/
    /*load lex*/
    FILE* lex = fopen("/home/qi/reorder_data/forward_index_with_dups/all_nlex", "r");
    int did, size, length;
    long long offset;

    sizes.reserve(MAXD);
    lengths.reserve(MAXD);
    offsets.reserve(MAXD);
    for(int i = 0; i < MAXD; i++){
      sizes.push_back(0);
      lengths.push_back(0);
      offsets.push_back(0);
    }

    while(fscanf(lex, "%d %d %d %lld", &did, &length, &size, &offset) == 4) {
      // cout << did << " " << size << " " << offset << "\n";
      sizes[did] = size;
      lengths[did] = length;
      offsets[did] = offset;
    }
    fclose(lex);

    // for(int i = 1; i < MAXD; i++){
    //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
    // }
    cout << "lex loading done\n";

    /*load content*/
    long long data_size = 6679406495;
    FILE* fp = fopen("/home/qi/reorder_data/forward_index_content/content0309","rb");
    data = new char[data_size];
    long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
    if(ret_code == data_size) {
        puts("data read successfully");
    } else { // error handling
       if (feof(fp))
          printf("Error reading test.bin: unexpected end of file\n");
       else if (ferror(fp)) {
           perror("Error reading test.bin");
       }
    }
    fclose(fp);


    /*init doc array*/
    // int total_docs = 25205179;
    int total_docs = 48;
    vector<docInfoR> docs;
    docs.reserve(total_docs);
    // std::string lex_file = "/home/qi/reorder_data/test_forward_index/48_lex";
    std::string lex_file = "/home/qi/reorder_data/test_forward_index/fb_48_0410";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    // int counter = 0;
    while(fscanf(pfile, "%d", &did) == 1) {
      // std::cout << did << "\n";
      docInfoR d(did, 0, 0.0);
      docs.push_back(d);
      // counter++;
      // if(counter >= 1000) {
      //   break;
      // }
    }
    fclose(pfile);

    threshold = 32;
    round = 20;
    current_ID = 0;

    // /*init doc array*/
    // vector<docInfoR> docs;
    // for(int i = 0; i < 10000; i++) {
    // // for(int i = 1000; i < 1100; i++) {
    //   int did = documentIDs[i];
    //   cout << did << "\n";
    //   docInfoR d(did, 0, 0.0);
    //   docs.push_back(d);
    // }

    /*init lex array*/
    term_num = 38501207;
    vector<lexInfoR> left_lex;
    vector<lexInfoR> right_lex;
    left_lex.reserve(term_num);
    right_lex.reserve(term_num);

    for(int i = 0; i < term_num; i++){
      left_lex.push_back(lexInfoR(-1, 0, 0.0, false));
      right_lex.push_back(lexInfoR(-1, 0, 0.0, false));
    }

    /*load real lm*/
    FILE* flm1 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_content_reverse_0405", "r");
    FILE* flm2 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_lex_reverse_0405", "r");

    // vector<pair<int, int>> lm_content_vec;
    int tid, score;
    while(fscanf(flm1, "%d %d", &tid, &score)==2){
      pair<int, int> p = make_pair(tid, score);
      lm_content_vec.push_back(p);
    }

    // map<string, pair<int, int>> lm_term_map;
    int length_lm;
    int offset_lm;
    while(fscanf(flm2, "%d %d %d", &tid, &length_lm, &offset_lm) == 3){
      lm_term_map[tid] = make_pair(length_lm, offset_lm);
    }
    fclose(flm1);
    fclose(flm2);
    // cout << "lm map size: " <<lm_term_map.size() << " lm content size: " << lm_content_vec.size() << "\n";

    // partition_fb_gain_analysis(docs, left_lex, right_lex);

    FILE* file = fopen("/home/qi/reorder_data/log/doc_term_contribution_0410", "w");
    partition_fb_doc_tracker(docs, left_lex, right_lex, file);
    fclose(file);

    delete[] data;
}

void OrderR::init_stats(){
    /*load forward index*/
    /*load lex*/
    FILE* lex = fopen("/home/qi/reorder_data/forward_index_with_dups/all_nlex", "r");
    int did, size, length;
    long long offset;

    sizes.reserve(MAXD);
    lengths.reserve(MAXD);
    offsets.reserve(MAXD);
    for(int i = 0; i < MAXD; i++){
      sizes.push_back(0);
      lengths.push_back(0);
      offsets.push_back(0);
    }

    while(fscanf(lex, "%d %d %d %lld", &did, &length, &size, &offset) == 4) {
      // cout << did << " " << size << " " << offset << "\n";
      sizes[did] = size;
      lengths[did] = length;
      offsets[did] = offset;
    }
    fclose(lex);

    // for(int i = 1; i < MAXD; i++){
    //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
    // }
    cout << "lex loading done\n";

    /*load content*/
    long long data_size = 6679406495;
    FILE* fp = fopen("/home/qi/reorder_data/forward_index_content/content0309","rb");
    data = new char[data_size];
    long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
    if(ret_code == data_size) {
        puts("data read successfully");
    } else { // error handling
       if (feof(fp))
          printf("Error reading test.bin: unexpected end of file\n");
       else if (ferror(fp)) {
           perror("Error reading test.bin");
       }
    }
    fclose(fp);


    /*init doc array*/
    int total_docs = 25205179;
    // int total_docs = 1000;
    vector<docInfoR> docs;
    docs.reserve(total_docs);
    std::string lex_file = "/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    char dname[1000];
    char url[10000];
    // int counter = 0;
    while(fscanf(pfile, "%s %d %s", url, &did, dname) == 3) {
      // std::cout << did << "\n";
      string s(url);
      docInfoR d(did, 0, 0.0, s);
      docs.push_back(d);
      // counter++;
      // if(counter >= 1000) {
      //   break;
      // }
    }
    fclose(pfile);

    threshold = 48;
    round = 20;
    current_ID = 0;

    /*init lex array*/
    term_num = 38501207;
    vector<lexInfoR> left_lex;
    vector<lexInfoR> right_lex;
    left_lex.reserve(term_num);
    right_lex.reserve(term_num);

    for(int i = 0; i < term_num; i++){
      left_lex.push_back(lexInfoR(-1, 0, 0.0, false));
      right_lex.push_back(lexInfoR(-1, 0, 0.0, false));
    }

    /*load real lm*/
    FILE* flm1 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_content_reverse_0405", "r");
    FILE* flm2 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_lex_reverse_0405", "r");

    // vector<pair<int, int>> lm_content_vec;
    int tid, score;
    while(fscanf(flm1, "%d %d", &tid, &score)==2){
      pair<int, int> p = make_pair(tid, score);
      lm_content_vec.push_back(p);
    }

    // map<string, pair<int, int>> lm_term_map;
    int length_lm;
    int offset_lm;
    while(fscanf(flm2, "%d %d %d", &tid, &length_lm, &offset_lm) == 3){
      lm_term_map[tid] = make_pair(length_lm, offset_lm);
    }
    fclose(flm1);
    fclose(flm2);
    // cout << "lm map size: " <<lm_term_map.size() << " lm content size: " << lm_content_vec.size() << "\n";

    /*recursion*/
    // partition_gain_analysis(docs, left_lex, right_lex);

    FILE* file = fopen("/home/qi/reorder_data/log/bp_track_doc_0415", "w");
    partition_doc_tracker(docs, left_lex, right_lex, file);
    fclose(file);

    // FILE* file = fopen("/home/qi/reorder_data/log/stat_dids_0403", "w");
    // partition_identify_dids(docs, left_lex, right_lex, file);
    // fclose(file);

    // FILE* file = fopen("/home/qi/reorder_data/log/stat_original_lvls_0405", "w");
    // partition_lvl_stats(docs, left_lex, right_lex, file);
    // fclose(file);

    // FILE* file = fopen("/home/qi/reorder_data/log/stat_50p_lvls_0404", "w");
    // partition_lm_sort_percentile_lvl_stats(docs, left_lex, right_lex, file);
    // fclose(file);

    // FILE* file = fopen("/home/qi/reorder_data/log/stat_rp_lvls_0405", "w");
    // partition_lm_sort_percentile_raondom_pairs_lvl_stats(docs, left_lex, right_lex, file);
    // fclose(file);

    // FILE* file = fopen("/home/qi/reorder_data/log/stat_fb_lvls_0410", "w");
    // partition_fb_lvl_stats(docs, left_lex, right_lex, file);
    // fclose(file);

    // FILE* file = fopen("/home/qi/reorder_data/log/stat_fb_dids_0409", "w");
    // partition_fb_identify_dids(docs, left_lex, right_lex, file);
    // fclose(file);

    delete[] data;

}

void OrderR::init_fb(){
    /*load forward index*/
    /*load lex*/
    FILE* lex = fopen("/home/qi/reorder_data/forward_index_with_dups/all_nlex", "r");
    int did, size, length;
    long long offset;

    sizes.reserve(MAXD);
    lengths.reserve(MAXD);
    offsets.reserve(MAXD);
    for(int i = 0; i < MAXD; i++){
      sizes.push_back(0);
      lengths.push_back(0);
      offsets.push_back(0);
    }

    while(fscanf(lex, "%d %d %d %lld", &did, &length, &size, &offset) == 4) {
      // cout << did << " " << size << " " << offset << "\n";
      sizes[did] = size;
      lengths[did] = length;
      offsets[did] = offset;
    }
    fclose(lex);

    // for(int i = 1; i < MAXD; i++){
    //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
    // }
    cout << "lex loading done\n";

    /*load content*/
    long long data_size = 6679406495;
    FILE* fp = fopen("/home/qi/reorder_data/forward_index_content/content0309","rb");
    data = new char[data_size];
    long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
    if(ret_code == data_size) {
        puts("data read successfully");
    } else { // error handling
       if (feof(fp))
          printf("Error reading forward index: unexpected end of file\n");
       else if (ferror(fp)) {
           perror("Error reading forward index");
       }
    }
    fclose(fp);


    /*init doc array*/
    int total_docs = 25205179;
    // int total_docs = 1000;
    vector<docInfoR> docs;
    docs.reserve(total_docs);
    std::string lex_file = "/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    char dname[1000];
    char url[10000];
    // int counter = 0;
    while(fscanf(pfile, "%s %d %s", url, &did, dname) == 3) {
      // std::cout << did << "\n";
      string s(url);
      docInfoR d(did, 0, 0.0, s);
      docs.push_back(d);
      // counter++;
      // if(counter >= 1000) {
      //   break;
      // }
    }
    fclose(pfile);

    threshold = 50;
    round = 20;
    current_ID = 0;

    /*init lex array*/
    term_num = 38501207;
    vector<lexInfoR> left_lex;
    vector<lexInfoR> right_lex;
    left_lex.reserve(term_num);
    right_lex.reserve(term_num);

    for(int i = 0; i < term_num; i++){
      left_lex.push_back(lexInfoR(-1, 0, 0.0, false));
      right_lex.push_back(lexInfoR(-1, 0, 0.0, false));
    }

    /*recursion*/
    partition_fb(docs, left_lex, right_lex);
    delete[] data;

    // for(int i = 0; i < final_order.size(); i++) {
    //   cout << i << ": " << final_order[i] << "\n";
    // }

    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/order_fb_0409", "w");
    for(int i = 0; i < final_order.size(); i++) {
      std::cout << i+1 << " " << final_order[i] << "\n";
      fprintf(lfile, "%d %d\n", i+1, final_order[i]);
    }
    fclose(lfile);
}

void OrderR::init_bp(){
    /*load forward index*/
    /*load lex*/
    FILE* lex = fopen("/home/qi/reorder_data/forward_index_with_dups/all_nlex", "r");
    int did, size, length;
    long long offset;

    sizes.reserve(MAXD);
    lengths.reserve(MAXD);
    offsets.reserve(MAXD);
    for(int i = 0; i < MAXD; i++){
      sizes.push_back(0);
      lengths.push_back(0);
      offsets.push_back(0);
    }

    while(fscanf(lex, "%d %d %d %lld", &did, &length, &size, &offset) == 4) {
      // cout << did << " " << size << " " << offset << "\n";
      sizes[did] = size;
      lengths[did] = length;
      offsets[did] = offset;
    }
    fclose(lex);

    // for(int i = 1; i < MAXD; i++){
    //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
    // }
    cout << "lex loading done\n";

    /*load content*/
    long long data_size = 6679406495;
    FILE* fp = fopen("/home/qi/reorder_data/forward_index_content/content0309","rb");
    data = new char[data_size];
    long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
    if(ret_code == data_size) {
        puts("data read successfully");
    } else { // error handling
       if (feof(fp))
          printf("Error reading test.bin: unexpected end of file\n");
       else if (ferror(fp)) {
           perror("Error reading test.bin");
       }
    }
    fclose(fp);


    /*init doc array*/
    int total_docs = 25205179;
    // int total_docs = 1000;
    vector<docInfoR> docs;
    docs.reserve(total_docs);
    std::string lex_file = "/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    char dname[1000];
    char url[10000];
    // int counter = 0;
    while(fscanf(pfile, "%s %d %s", url, &did, dname) == 3) {
      // std::cout << did << "\n";
      string s(url);
      docInfoR d(did, 0, 0.0, s);
      docs.push_back(d);
      // counter++;
      // if(counter >= 1000) {
      //   break;
      // }
    }
    fclose(pfile);

    threshold = 50;
    round = 20;
    current_ID = 0;

    // /*init doc array*/
    // vector<docInfoR> docs;
    // for(int i = 0; i < 10000; i++) {
    // // for(int i = 1000; i < 1100; i++) {
    //   int did = documentIDs[i];
    //   cout << did << "\n";
    //   docInfoR d(did, 0, 0.0);
    //   docs.push_back(d);
    // }

    /*init lex array*/
    term_num = 38501207;
    vector<lexInfoR> left_lex;
    vector<lexInfoR> right_lex;
    left_lex.reserve(term_num);
    right_lex.reserve(term_num);

    for(int i = 0; i < term_num; i++){
      left_lex.push_back(lexInfoR(-1, 0, 0.0, false));
      right_lex.push_back(lexInfoR(-1, 0, 0.0, false));
    }

    /*load real lm*/
    FILE* flm1 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_content_reverse_0405", "r");
    FILE* flm2 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_lex_reverse_0405", "r");

    // vector<pair<int, int>> lm_content_vec;
    int tid, score;
    while(fscanf(flm1, "%d %d", &tid, &score)==2){
      pair<int, int> p = make_pair(tid, score);
      lm_content_vec.push_back(p);
    }

    // map<string, pair<int, int>> lm_term_map;
    int length_lm;
    int offset_lm;
    while(fscanf(flm2, "%d %d %d", &tid, &length_lm, &offset_lm) == 3){
      lm_term_map[tid] = make_pair(length_lm, offset_lm);
    }
    fclose(flm1);
    fclose(flm2);
    // cout << "lm map size: " <<lm_term_map.size() << " lm content size: " << lm_content_vec.size() << "\n";

    /*recursion*/
    // partition_lm_sort_url(docs, left_lex, right_lex);
    // partition_lm_no_presorting(docs, left_lex, right_lex);
    partition_lm_sort_percentile(docs, left_lex, right_lex);
    // partition_lm_sort_percentile_random_pairs(docs, left_lex, right_lex);
    delete[] data;

    // for(int i = 0; i < final_order.size(); i++) {
    //   cout << i << ": " << final_order[i] << "\n";
    // }

    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_pres_48_100p_ends_0414", "w");
    for(int i = 0; i < final_order.size(); i++) {
      // std::cout << i+1 << " " << final_order[i] << "\n";
      fprintf(lfile, "%d %d\n", i+1, final_order[i]);
    }
    fclose(lfile);
}

void OrderR::init_gov2(){
    /*load forward index*/
    /*load lex*/
    FILE* lex = fopen("/home/qi/reorder_data/forward_index_with_dups/all_nlex", "r");
    int did, size, length;
    long long offset;

    sizes.reserve(MAXD);
    lengths.reserve(MAXD);
    offsets.reserve(MAXD);
    for(int i = 0; i < MAXD; i++){
      sizes.push_back(0);
      lengths.push_back(0);
      offsets.push_back(0);
    }

    while(fscanf(lex, "%d %d %d %lld", &did, &length, &size, &offset) == 4) {
      // cout << did << " " << size << " " << offset << "\n";
      sizes[did] = size;
      lengths[did] = length;
      offsets[did] = offset;
    }
    fclose(lex);

    // for(int i = 1; i < MAXD; i++){
    //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
    // }
    cout << "lex loading done\n";

    /*load content*/
    long long data_size = 6679406495;
    FILE* fp = fopen("/home/qi/reorder_data/forward_index_content/content0309","rb");
    data = new char[data_size];
    long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
    if(ret_code == data_size) {
        puts("data read successfully");
    } else { // error handling
       if (feof(fp))
          printf("Error reading test.bin: unexpected end of file\n");
       else if (ferror(fp)) {
           perror("Error reading test.bin");
       }
    }
    fclose(fp);


    /*init doc array*/
    int total_docs = 25205179;
    // int total_docs = 1000;
    vector<docInfoR> docs;
    docs.reserve(total_docs);
    std::string lex_file = "/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    char dname[1000];
    char url[10000];
    // int counter = 0;
    while(fscanf(pfile, "%s %d %s", url, &did, dname) == 3) {
      // std::cout << did << "\n";
      string s(url);
      docInfoR d(did, 0, 0.0, s);
      docs.push_back(d);
      // counter++;
      // if(counter >= 1000) {
      //   break;
      // }
    }
    fclose(pfile);

    threshold = 32;
    round = 20;
    current_ID = 0;

    // /*init doc array*/
    // vector<docInfoR> docs;
    // for(int i = 0; i < 10000; i++) {
    // // for(int i = 1000; i < 1100; i++) {
    //   int did = documentIDs[i];
    //   cout << did << "\n";
    //   docInfoR d(did, 0, 0.0);
    //   docs.push_back(d);
    // }

    /*init lex array*/
    term_num = 38501207;
    double gain_low_bound = -10000.00;
    vector<lexInfoR> left_lex;
    vector<lexInfoR> right_lex;
    left_lex.reserve(term_num);
    right_lex.reserve(term_num);

    for(int i = 0; i < term_num; i++){
      left_lex.push_back(lexInfoR(0, 0, gain_low_bound));
      right_lex.push_back(lexInfoR(0, 0, gain_low_bound));
    }

    /*prepare forward index*/
    // for(int i = 0; i < docs.size(); i++){
    //   std::cout << i << " " << docs[i].did << "\n";
    //   const indri::index::TermList *termList = thisIndex->termList(docs[i].did);

    //   std::vector<int> termIds;
    //   for(int j=0; j<termList->terms().size(); ++j){
    //     if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
    //       termIds.push_back(termList->terms()[j]);
    //     }
    //   }
    //   sort(termIds.begin(), termIds.end());

    //   std::vector<int>::iterator it;
    //   it = std::unique (termIds.begin(), termIds.end());       
    //   termIds.resize( std::distance(termIds.begin(),it) );
    //   forward_index_map[docs[i].did] = termIds;
    // }

    /*load lm*/
    std::vector<int> lm_vec(term_num, 0);
    FILE* qfile = fopen("/home/qi/reorder_data/query_info/random_query_pair", "r");
    int qt1, qt2;
    while(fscanf(qfile, "%d %d\n", &qt1, &qt2)==2) {
      // lm_vec[qt1-1] = qt2;
      // lm_vec[qt2-1] = qt1;
      lm_map[qt1] = qt2;
      lm_map[qt2] = qt1;
    }
    fclose(qfile);

    /*recursion*/
    partition_gov2(docs, left_lex, right_lex);
    delete[] data;

    // for(int i = 0; i < final_order.size(); i++) {
    //   cout << i << ": " << final_order[i] << "\n";
    // }

    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_gov2", "w");
    for(int i = 0; i < final_order.size(); i++) {
      std::cout << i+1 << " " << final_order[i] << "\n";
      fprintf(lfile, "%d %d\n", i+1, final_order[i]);
    }
    fclose(lfile);
}

void OrderR::init_test(){
    /*load forward index*/
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    indri::index::Index *thisIndex;
    thisIndex=(*repIndexState)[0];

    long long offset = 0;
    std::vector<lemur::api::DOCID_T> documentIDs;
    int cap = 10000;
    documentIDs.reserve(cap);

    /*init doc array*/
    vector<docInfoR> docs;
    std::string lex_file = "/home/qi/reorder_data/r10k_forward_index/lex_219";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    int did;
    char dname[1000];
    char url[10000];
    while(fscanf(pfile, "%d %s %s", &did, dname, url) == 3) {
      // std::cout << did << "\n";
      string s(url);
      documentIDs.push_back(did);
      docInfoR d(did, 0, 0.0, s);
      docs.push_back(d);
    }
    fclose(pfile);

    std::vector<ParsedDocument*> parsedDocs;
    parsedDocs=indriEnvironment.documents(documentIDs);

    threshold = 32;
    round = 20;
    current_ID = 0;

    // /*init doc array*/
    // vector<docInfoR> docs;
    // for(int i = 0; i < 10000; i++) {
    // // for(int i = 1000; i < 1100; i++) {
    //   int did = documentIDs[i];
    //   cout << did << "\n";
    //   docInfoR d(did, 0, 0.0);
    //   docs.push_back(d);
    // }

    /*init lex array*/
    term_num = 38501207;
    double gain_low_bound = -10000.00;
    vector<lexInfoR> left_lex;
    vector<lexInfoR> right_lex;
    left_lex.reserve(term_num);
    right_lex.reserve(term_num);

    for(int i = 0; i < term_num; i++){
      left_lex.push_back(lexInfoR(0, 0, gain_low_bound));
      right_lex.push_back(lexInfoR(0, 0, gain_low_bound));
    }

    /*prepare forward index*/
    for(int i = 0; i < docs.size(); i++){
      std::cout << i << " " << docs[i].did << "\n";
      const indri::index::TermList *termList = thisIndex->termList(docs[i].did);

      std::vector<int> termIds;
      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }
      sort(termIds.begin(), termIds.end());

      std::vector<int>::iterator it;
      it = std::unique (termIds.begin(), termIds.end());       
      termIds.resize( std::distance(termIds.begin(),it) );
      forward_index_map[docs[i].did] = termIds;
    }

    /*load lm*/
    // std::vector<int> lm_vec(term_num, 0);
    FILE* qfile = fopen("/home/qi/reorder_data/query_info/random_query_pair", "r");
    int qt1, qt2;
    while(fscanf(qfile, "%d %d\n", &qt1, &qt2)==2) {
      // lm_vec[qt1-1] = qt2;
      // lm_vec[qt2-1] = qt1;
      lm_map[qt1] = qt2;
      lm_map[qt2] = qt1;
    }
    fclose(qfile);

    /*recursion*/
    partition(docs, left_lex, right_lex);

    // for(int i = 0; i < final_order.size(); i++) {
    //   cout << i << ": " << final_order[i] << "\n";
    // }

    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order", "w");
    for(int i = 0; i < final_order.size(); i++) {
      std::cout << i+1 << " " << final_order[i] << "\n";
      fprintf(lfile, "%d %d\n", i+1, final_order[i]);
    }
    fclose(lfile);
}

int myrandom (int i) { return std::rand()%i;}

void FIndexBuilder::get_random_order_r10k(){
  vector<int> dids;
  FILE* lfile = fopen("/home/qi/reorder_data/r10k_forward_index/lex_with_offset", "r");
  int ndid, odid;
  while(fscanf(lfile, "%d %d %*[^\n]", &ndid, &odid) == 2){
    std::cout << ndid << " " << odid << "\n";
    dids.push_back(odid);
  }
  fclose(lfile);

  std::random_shuffle ( dids.begin(), dids.end() );
  std::random_shuffle ( dids.begin(), dids.end(), myrandom);

  lfile = fopen("/home/qi/reorder_data/ordering_info/random_order", "w");
  for(int i = 0; i < dids.size(); i++) {
    std::cout << i+1 << " " << dids[i] << "\n";
    fprintf(lfile, "%d %d\n", i+1, dids[i]);
  }
  fclose(lfile);
}

void FIndexBuilder::get_random_order(string url_lex, string random_order) {
  vector<int> dids;
  // FILE* lfile = fopen("/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex", "r");
  FILE* lfile = fopen(url_lex.c_str(), "r");
  int odid;
  char c[50000];
  while(fscanf(lfile, "%s %d %*[^\n]", c, &odid) == 2){
    // std::cout << odid << "\n";
    dids.push_back(odid);
  }
  fclose(lfile);

  std::random_shuffle ( dids.begin(), dids.end() );
  std::random_shuffle ( dids.begin(), dids.end(), myrandom);

  // lfile = fopen("/home/qi/reorder_data/ordering_info/random_order_all", "w");
  lfile = fopen(random_order.c_str(), "w");
  for(int i = 0; i < dids.size(); i++) {
    // std::cout << i+1 << " " << dids[i] << "\n";
    fprintf(lfile, "%d %d\n", i+1, dids[i]);
  }
  fclose(lfile); 
}

void FIndexBuilder::and_bp(qpList& l1, qpList& l2, int& bp, int& hit, int max_d){
  // std::cout << l1.cur_did << "\n";
  // std::cout << l2.cur_did << "\n";
  vector<qpList> lists;
  /*assgin cur_did as the smaller list's cur did*/
  int cur_did = 0;
  if(l1.listlen < l2.listlen){
    lists.push_back(l1);
    lists.push_back(l2);
  }else{
    lists.push_back(l2);
    lists.push_back(l1);
  }

  cur_did = lists[0].cur_did;

  while(cur_did < max_d){
    int i = 0;
    for(; i < 2; i++){
      if(lists[i].cur_did < cur_did){
        lists[i].get_next_GEQ(cur_did, max_d);
        bp++;
      }

      /*if no match for the cur_did, update the cur_did to the next element of the mismatch list*/
      if(lists[i].cur_did!=cur_did){
        cur_did = lists[i].cur_did;
        break;
      }
    }

    /*a match happened between two lists, update cur_did to the next element in the smaller list*/
    if(i == 2){
      lists[0].get_next_GEQ(cur_did+1, max_d);
      // cout << cur_did << " ";
      cur_did = lists[0].cur_did;
      bp++;
      hit++;
    }
  }
  // cout << "\n";
}

void FIndexBuilder::get_inverted_list(std::string termString){
    indri::collection::Repository repository;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    repository.openRead(indexPath);
    
    std::string stem = repository.processTerm( termString );
    indri::server::LocalQueryServer local(repository);

    UINT64 totalCount = local.termCount();
    UINT64 termCount = local.termCount( termString );

    std::cout << termString << " "
            << stem << " "
            << termCount << " " 
            << totalCount << " " << std::endl;

    indri::collection::Repository::index_state state = repository.indexes();

    for( size_t i=0; i<state->size(); i++ ) {
      cout << "i: " << i << endl;
      indri::index::Index* index = (*state)[i];
      indri::thread::ScopedLock( index->iteratorLock() );

      indri::index::DocListIterator* iter = index->docListIterator( stem );
      if (iter == NULL) continue;

      iter->startIteration();

      int doc = 0;
      indri::index::DocListIterator::DocumentData* entry;

      for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();

        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
      }

      delete iter;
    }
}

void FIndexBuilder::query_processing_single_query(std::string term1, std::string term2, std::string order, std::string indri_index, std::string order_path, int max_d){

  if( order!="url" && order!="random" && order!="bp" && order != "system"){
    std::cout << "pls specify ordering\n";
    return;
  }

  /*load new ordering*/
  std::map<int, int> order_map;
  if(order == "random"){
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }else if(order == "url"){
    FILE* lfile = fopen(order_path.c_str(), "r");
    int did;
    int count = 1;
    char c[50000];
    while(fscanf(lfile, "%s %d %*[^\n]\n", c, &did) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[did] = count;
      count++;
    }
    fclose(lfile);
  } else if(order == "bp"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order", "r");
    // int ndid, odid;
    // while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
    //   // std::cout << ndid << " " << odid << "\n";
    //   order_map[odid] = ndid;
    // }
    // fclose(lfile);
  }

  indri::collection::Repository repository;
  const std::string indexPath = indri_index;
  repository.openRead(indexPath);
  indri::server::LocalQueryServer local(repository);
  indri::collection::Repository::index_state state = repository.indexes();
  indri::index::Index* index = (*state)[0];
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;

  std::vector<int> dids1;
  std::vector<int> dids2;
  std::vector<int> ndids1;
  std::vector<int> ndids2;

  iter = index->docListIterator(term1);
  if (iter == NULL){
    std::cout << term1 << ": not in the index, attention!\n";
    return;
  }
  iter->startIteration();
  for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
      entry = iter->currentEntry();
      dids1.push_back(entry->document);
      // std::cout << entry->document << " "
      //           << entry->positions.size() << " "
      //           << index->documentLength( entry->document ) << std::endl;
  }

  iter = index->docListIterator(term2);

  if (iter == NULL){
    std::cout << term2 << ": not in the index, attention!\n";
    return;
  }
  iter->startIteration();
  for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
      entry = iter->currentEntry();
      dids2.push_back(entry->document);
      // std::cout << entry->document << " "
      //           << entry->positions.size() << " "
      //           << index->documentLength( entry->document ) << std::endl;
  }

  if(order == "system") {
    ndids1 = dids1;
    ndids2 = dids2;
  }else{
    for(int i = 0; i < dids1.size(); i++) {
      // cout << dids1[i] << " " << order_map[dids1[i]] << " "; 
      if(order_map.find(dids1[i])!=order_map.end()){
        ndids1.push_back(order_map[dids1[i]]);
      }else{
        cout << "attention: " << dids1[i] << " has no match in ordering\n";
      }
    }
    // cout << "\n";
    for(int i = 0; i < dids2.size(); i++) {
       // cout << dids2[i] << " " << order_map[dids2[i]] << " "; 
      if(order_map.find(dids2[i])!=order_map.end()){
        ndids2.push_back(order_map[dids2[i]]);
      }else{
        cout << "attention: " << dids2[i] << " has no match in ordering\n";
      }
    }
    // cout << "\n";
    sort(ndids1.begin(), ndids1.end());
    sort(ndids2.begin(), ndids2.end());
    // for(int i = 0; i < ndids1.size(); i++){
    //   cout << ndids1[i] << " ";
    // }
    // cout << "\n";
  }

  ndids1.push_back(max_d);
  ndids2.push_back(max_d);

  int bp = 0;
  int hit = 0;
  qpList l1(ndids1, max_d);
  qpList l2(ndids2, max_d);
  and_bp(l1, l2, bp, hit, max_d);
  std::cout << hit << " " << bp << "\n";
}

void FIndexBuilder::postings_percentage(){
  FILE* f = fopen("/home/qi/reorder_data/inverted_index_all/term_infos", "r");
  char term[10000];
  int listlen;
  unsigned long long total;
  unsigned long long total_postings = 23451774775;
  unsigned long long sum = 0;
  int count = 0;
  int threshold = 100000;

  FILE* f1 = fopen("/home/qi/reorder_data/query_info/terms_2", "w");
  // while(fscanf(f, "%s %d %llu", term, &listlen, &total) == 3){
  //   cout << term << " " << listlen << " " << total << "\n";
  //   if(listlen >= 4096){
  //     count ++;
  //   }
  // }
  while(fscanf(f, "%s %d %llu", term, &listlen, &total) == 3){
    // cout << term << " " << listlen << " " << total << "\n";
    if(listlen >= threshold){
      sum += total;
      count ++;
    }
  }
  fclose(f);
  fclose(f1);
  cout << "count: " << count << " threshold: " << threshold << "\n";
  cout << "sum: " << sum << " total_postings: " << total_postings << "\n";
  double p = (double)sum/(double)total_postings;
  cout << "percentage: " << p << "\n";
}

void FIndexBuilder::fetch_longer_terms(){
  FILE* f = fopen("/home/qi/reorder_data/inverted_index_all/term_infos", "r");
  char term[10000];
  int listlen;
  unsigned long long total;
  int count = 0;

  FILE* f1 = fopen("/home/qi/reorder_data/query_info/terms_2", "w");
  // while(fscanf(f, "%s %d %llu", term, &listlen, &total) == 3){
  //   cout << term << " " << listlen << " " << total << "\n";
  //   if(listlen >= 4096){
  //     count ++;
  //   }
  // }
  while(fscanf(f, "%s %d %llu", term, &listlen, &total) == 3){
    // cout << term << " " << listlen << " " << total << "\n";
    if(listlen >= 2){
      fprintf(f1, "%s\n", term);
      count ++;
    }
  }
  fclose(f);
  fclose(f1);
  cout << count << "\n";

}

void FIndexBuilder::compress_list(vector<int>& dids, double& bits){
  /*take delta of the list*/
  for(int k = dids.size()-1; k > 0; k--){
    dids[k] = dids[k] - dids[k-1];
  }

  /*take log of each number*/
  double logs;
  for(int i = 0; i < dids.size(); i++){
    logs = log(dids[i]);
    bits += logs;
  }
}

void FIndexBuilder::compute_compression_stat_query_average(std::string order){
  std::cout << order << "\n";

  if( order!="url" && order!="random" && order!="bp" && order!="bplm" && order!="fb"){
    std::cout << "pls specify ordering\n";
    return;
  }

  indri::collection::Repository repository;
  const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 
  repository.openRead(indexPath);
  indri::server::LocalQueryServer local(repository);
  indri::collection::Repository::index_state state = repository.indexes();
  indri::index::Index* index = (*state)[0];

  /*load new ordering*/
  std::map<int, int> order_map;
  if(order == "random"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/random_order_all_0306", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }else if(order == "url"){
    FILE* lfile = fopen("/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex", "r");
    int did;
    int count = 1;
    char c[50000];
    while(fscanf(lfile, "%s %d %*[^\n]\n", c, &did) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[did] = count;
      count++;
    }
    fclose(lfile);
  } else if(order == "bp"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_gov2_0311", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "bplm"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_pres_48_60p_ends_0410", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "fb"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/order_fb_0408", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }

  /*do terms first*/
  map<string, double> term_bits;
  FILE* qfile = fopen("/home/qi/reorder_data/query_info/tera06_stable", "r");
  char t1[1000];
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;
  std::vector<int> dids1;
  double total_bits = 0;
  int num_terms = 0;
  while(fscanf(qfile, "%s", t1) == 1) {
    cout << t1 << " ";
    iter = index->docListIterator(t1);
    if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      continue;
    }
    iter->startIteration();
    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        dids1.push_back(entry->document);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
    }

    std::vector<int> ndids1;
    for(int i = 0; i < dids1.size(); i++) {
      if(order_map.find(dids1[i])!=order_map.end()){
        ndids1.push_back(order_map[dids1[i]]);
      }else{
        cout << "attention: " << dids1[i] << " has no match in ordering\n";
        return;
      }
    }

    dids1.clear();
    sort(ndids1.begin(), ndids1.end());

    double bits = 0;
    compress_list(ndids1, bits);

    std::cout << bits << "\n";
    term_bits[t1] = bits;
  }
  delete iter;
  fclose(qfile);

  double query_bits = 0.0;
  int num_queries = 0;
  char t2[1000];
  qfile = fopen("/home/qi/reorder_data/query_info/test_queries_stable", "r");
  while(fscanf(qfile, "%s %s\n", t1, t2) == 2) {
    // cout << t1 << " " << t2 << " ";
    query_bits += term_bits[t1];
    query_bits += term_bits[t2];
    num_queries ++;
  }
  fclose(qfile);
  std::cout << "num of query: " << num_queries << "\n";
  std::cout << "ave bits: " << query_bits/(double)num_queries << "\n";

}

void FIndexBuilder::compute_compression_stat(std::string order){
  std::cout << order << "\n";

  if( order!="url" && order!="random" && order!="bp" && order!="bplm" && order!="fb"){
    std::cout << "pls specify ordering\n";
    return;
  }

  indri::collection::Repository repository;
  const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 
  repository.openRead(indexPath);
  indri::server::LocalQueryServer local(repository);
  indri::collection::Repository::index_state state = repository.indexes();
  indri::index::Index* index = (*state)[0];

  /*load new ordering*/
  std::map<int, int> order_map;
  if(order == "random"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/random_order_all_0306", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }else if(order == "url"){
    FILE* lfile = fopen("/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex", "r");
    int did;
    int count = 1;
    char c[50000];
    while(fscanf(lfile, "%s %d %*[^\n]\n", c, &did) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[did] = count;
      count++;
    }
    fclose(lfile);
  } else if(order == "bp"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_gov2_0311", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "bplm"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_pres_48_60p_ends_0410", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "fb"){
    FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/order_fb_0408", "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }

  /*do terms first*/
  FILE* qfile = fopen("/home/qi/reorder_data/query_info/terms_100k", "r");
  char t1[1000];
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;
  std::vector<int> dids1;
  double total_bits = 0;
  int num_terms = 0;
  while(fscanf(qfile, "%s", t1) == 1) {
    cout << t1 << " ";
    iter = index->docListIterator(t1);
    if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      continue;
    }
    iter->startIteration();
    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        dids1.push_back(entry->document);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
    }

    std::vector<int> ndids1;
    for(int i = 0; i < dids1.size(); i++) {
      if(order_map.find(dids1[i])!=order_map.end()){
        ndids1.push_back(order_map[dids1[i]]);
      }else{
        cout << "attention: " << dids1[i] << " has no match in ordering\n";
        return;
      }
    }

    dids1.clear();
    sort(ndids1.begin(), ndids1.end());

    double bits = 0;
    compress_list(ndids1, bits);

    std::cout << bits << "\n";
    total_bits += bits;
    num_terms++;
  }
  std::cout << "num of terms: " << num_terms << "\n";
  std::cout << "ave bits: " << total_bits/(double)num_terms << "\n";
  delete iter;
  fclose(qfile);
}

void FIndexBuilder::query_processing_ordering(std::string order, std::string indri_index, std::string order_path, std::string query, int max_d){
  std::cout << order << "\n";

  if( order!="url" && order!="random" && order!="bp" && order!="bplm" && order!="fb"){
    std::cout << "pls specify ordering\n";
    return;
  }

  indri::collection::Repository repository;
  // const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 
  const std::string indexPath = indri_index; 
  repository.openRead(indexPath);
  indri::server::LocalQueryServer local(repository);
  indri::collection::Repository::index_state state = repository.indexes();
  indri::index::Index* index = (*state)[0];

  /*load new ordering*/
  std::map<int, int> order_map;
  if(order == "random"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/random_order_all_0306", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }else if(order == "url"){
    // FILE* lfile = fopen("/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int did;
    int count = 1;
    char c[50000];
    while(fscanf(lfile, "%s %d %*[^\n]\n", c, &did) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[did] = count;
      count++;
    }
    fclose(lfile);
  } else if(order == "bp"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_gov2_0311", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "bplm"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_pres_48_100p_ends_0414", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "fb"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/order_fb_0408", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }

  /*load query*/
  FILE* qfile = fopen(query.c_str(), "r");
  char t1[1000];
  char t2[1000];
  int qid;
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;
  long long total_bp = 0;
  long long total_hit = 0;
  int num_query = 0;
  while(fscanf(qfile, "%s %s %d %*[^\n]\n", t1, t2, &qid) == 3) { //for new format
  // while(fscanf(qfile, "%s %s \n", t1, t2) == 2) { //for old format
    std::vector<int> dids1;
    std::vector<int> dids2;
    cout << t1 << " " << t2 << " " << qid << " ";
    iter = index->docListIterator(t1);
    if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      continue;
    }
    iter->startIteration();
    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        dids1.push_back(entry->document);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
    }

    iter = index->docListIterator(t2);

    if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      continue;
    }
    iter->startIteration();
    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        dids2.push_back(entry->document);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
    }

    std::vector<int> ndids1;
    std::vector<int> ndids2;
    for(int i = 0; i < dids1.size(); i++) {
      if(order_map.find(dids1[i])!=order_map.end()){
        ndids1.push_back(order_map[dids1[i]]);
      }else{
        cout << "attention: " << dids1[i] << " has no match in ordering\n";
        return;
      }
    }
    for(int i = 0; i < dids2.size(); i++) {
      if(order_map.find(dids2[i])!=order_map.end()){
        ndids2.push_back(order_map[dids2[i]]);
      }else{
        cout << "attention: " << dids2[i] << " has no match in ordering\n";
        return;
      }
    }

    sort(ndids1.begin(), ndids1.end());
    sort(ndids2.begin(), ndids2.end());

    ndids1.push_back(max_d);
    ndids2.push_back(max_d);

    int bp = 0;
    int hit = 0;
    qpList l1(ndids1, max_d);
    qpList l2(ndids2, max_d);
    and_bp(l1, l2, bp, hit, max_d);
    std::cout << hit << " " << bp << "\n";
    total_bp += bp;
    total_hit += hit;
    num_query++;

  }
  std::cout << "num of queries: " << num_query << "\n";
  std::cout << "ave nextGEQ: " << total_bp/num_query << "\n";
  std::cout << "ave hit: " << total_hit/num_query << "\n";
  delete iter; // indri code does delete this pointer, so do this same here
  fclose(qfile);
}


void FIndexBuilder::select_random_10k_from_other_set(string potential_other, int potential_size, int random_size, string random_10k_other_set){

  int count = potential_size;

  /*get random index*/
  std::set<int> nums;
  srand (time(NULL));
  int cn = 0;
  while(cn < random_size){
    int num = rand() % count + 1;
    // std::cout << num << "\n";
    if(nums.find(num) == nums.end()) {
       nums.insert(num);
       cn ++;
    }
  }

  cout << "size of random numbers:" << nums.size() <<"\n"; 

  /*get 3k testing queries*/
  char c[10000];
  int line = 1;
  FILE* file = fopen(potential_other.c_str(), "r");
  FILE* f1 = fopen(random_10k_other_set.c_str(), "w");
  while(fscanf(file, "%[^\n]\n", c) == 1) {
    if(nums.find(line)!=nums.end()) {
      // std::cout << "testing " << line << " " << c << endl;
      fprintf(f1, "%s\n", c);
    }
    line++;
  }
  fclose(file);
  fclose(f1);
}

void FIndexBuilder::random_query_generation_tera2006(){

  int count = 97613;

  /*get random index*/
  std::set<int> nums;
  srand (time(NULL));
  int cn = 0;
  while(cn < 3000){
    int num = rand() % count + 1;
    // std::cout << num << "\n";
    if(nums.find(num) == nums.end()) {
       nums.insert(num);
       cn ++;
    }
  }

  cout << "size of random numbers:" << nums.size() <<"\n"; 

  /*get 3k testing queries*/
  char c[10000];
  int line = 1;
  FILE* file = fopen("/home/qi/reorder_data/query_info/tera06_processed_query_pair", "r");
  FILE* f1 = fopen("/home/qi/reorder_data/query_info/tera06_test_queries", "w");
  FILE* f2 = fopen("/home/qi/reorder_data/query_info/tera06_train_queries", "w");
  while(fscanf(file, "%[^\n]\n", c) == 1) {
    if(nums.find(line)!=nums.end()) {
      // std::cout << "testing " << line << " " << c << endl;
      fprintf(f1, "%s\n", c);
    }else{
      // std::cout << "training " << c << endl;
      fprintf(f2, "%s\n", c);
    }
    line++;
  }
  fclose(file);
  fclose(f1);
  fclose(f2);
}

void FIndexBuilder::random_query_generation_m9(){

  // int count = 32547;
  int count = 32355;

  /*get random index*/
  std::set<int> nums;
  srand (time(NULL));
  int cn = 0;
  while(cn < 3000){
    int num = rand() % count + 1;
    // std::cout << num << "\n";
    if(nums.find(num) == nums.end()) {
       nums.insert(num);
       cn ++;
    }
  }

  cout << "size of random numbers:" << nums.size() <<"\n"; 

  /*get 3k testing queries*/
  char c[10000];
  int line = 1;
  FILE* file = fopen("/home/qi/reorder_data/query_info/m9_processed_query_pair", "r");
  FILE* f1 = fopen("/home/qi/reorder_data/query_info/test_queries", "w");
  FILE* f2 = fopen("/home/qi/reorder_data/query_info/train_queries", "w");
  while(fscanf(file, "%[^\n]\n", c) == 1) {
    if(nums.find(line)!=nums.end()) {
      // std::cout << "testing " << line << " " << c << endl;
      fprintf(f1, "%s\n", c);
    }else{
      // std::cout << "training " << c << endl;
      fprintf(f2, "%s\n", c);
    }
    line++;
  }
  fclose(file);
  fclose(f1);
  fclose(f2);
}

void FIndexBuilder::random_query_generation(){
  /*load lex*/
  std::map<int, lexInfo> lex_map;
  int tid, listlen;
  long long offset;
  FILE* file = fopen("/home/qi/reorder_data/r10k_inverted_index/lex", "r");
  int counter = 0;
  int threshold = 10;
  while(fscanf(file, "%d %d %lld\n", &tid, &listlen, &offset) == 3) {
    // std::cout << tid << " " << listlen << " " << offset << "\n";
    if(listlen > threshold) {
      counter ++;
      lexInfo l(listlen, offset);
      lex_map[tid] = l;
    }
  }
  fclose(file);

  std::cout << "# of listlen larger than " << threshold << " : " << counter << "\n";

  std::set<int> nums;
  srand (time(NULL));
  int cn = 0;
  while(cn < 2000){
    int num = rand() % counter + 1;
    // std::cout << num << "\n";
    if(nums.find(num) == nums.end()) {
       nums.insert(num);
       cn ++;
    }
  }

  /*write out lex*/
  std::map<int, lexInfo> small_lex_map;
  file = fopen("/home/qi/reorder_data/query_info/query_lex", "w");
  counter = 1;
  for(auto it = lex_map.begin(); it!=lex_map.end(); ++it) {
      if(nums.find(counter)!=nums.end()) {
        std::cout << counter << " " << it->first << " " << it->second.listlen << " " << it->second.offset << "\n";
        lexInfo l(it->second.listlen, it->second.offset);
        small_lex_map[it->first] = l;
        fprintf(file, "%d %d %lld\n", it->first, it->second.listlen, it->second.offset);
      }
      counter++;
  }
  fclose(file);

  /*generate query pairs*/
  nums.clear();
  cn = 0;
  while(cn < 1000){
    int num = rand() % 2000 + 1;
    if(nums.find(num) == nums.end()) {
       nums.insert(num);
       cn ++;
    }
  }

  counter = 1;
  std::vector<int> first_half;
  std::vector<int> second_half;
  for(auto it = small_lex_map.begin(); it!=small_lex_map.end(); ++it) {
    if(nums.find(counter)!=nums.end()) {
      first_half.push_back(it->first);
    }else {
      second_half.push_back(it->first);
    }
    counter++;
  }

  /*output the query*/
  /*random shuffle the second part*/
  std::random_shuffle ( second_half.begin(), second_half.end() );
  std::random_shuffle ( second_half.begin(), second_half.end(), myrandom);
  file = fopen("/home/qi/reorder_data/query_info/random_query_pair", "w");
  for(int i = 0; i < 1000; i++) {
    if(first_half[i] < second_half[i]){
      // std::cout << first_half[i] << " " << second_half[i] << "\n";
      fprintf(file, "%d %d\n", first_half[i], second_half[i]);
    }else{
      // std::cout << second_half[i] << " " << first_half[i] << "\n";
      fprintf(file, "%d %d\n", second_half[i], first_half[i]);
    }
  }
  fclose(file);
}

void FIndexBuilder::reverse_query_pair(){

  map<int, string> term_map;
  int tid, listlen;
  long long offset;
  FILE* f = fopen("/home/qi/reorder_data/query_info/query_lex_term", "r");
  char term[1000];
  while(fscanf(f, "%s %d %d %lld\n", term, &tid, &listlen, &offset) == 4) {
    string t(term);
    term_map[tid] = t;
    // std::cout << tid << " " << listlen << " " << offset << "\n";
  }
  fclose(f);

  FILE* file = fopen("/home/qi/reorder_data/query_info/random_query_pair", "r");
  FILE* file1 = fopen("/home/qi/reorder_data/query_info/random_query_pair_terms_0312", "w");
  int t1, t2;
  while(fscanf(file, "%d %d\n", &t1, &t2) == 2) {
    string term1 = term_map[t1];
    string term2 = term_map[t2];
    // cout << term1 << " " << term2 << "\n";
    fprintf(file1, "%s %s\n", term1.c_str(), term2.c_str());
  }
  fclose(file);
  fclose(file1);
}

bool sort_postings(posting i, posting j){
  if(i.tid < j.tid){
    return true;
  }else if(i.tid > j.tid){
    return false;
  }else{
    if(i.did < j.did){
      return true;
    }else{
      return false;
    }
  }
}

bool sort_terms (termInfo a, termInfo b) { 
    if(a.listlen < b.listlen){
      return true;
    }else{
      return false;
    }
}

bool sort_terms_by_term (termInfo a, termInfo b) { 
    if(a.term.compare(b.term) < 0){
      return true;
    }else{
      return false;
    }
}

void FIndexBuilder::print_vocabulary(string indri_index, string term_lex) {
  indri::collection::Repository r;
  // const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 
  const std::string indexPath = indri_index;
  r.openRead(indexPath);
  indri::collection::Repository::index_state state = r.indexes();

  indri::index::Index* index = (*state)[0];
  indri::index::VocabularyIterator* iter = index->vocabularyIterator();

  iter->startIteration();
  std::cout << "TOTAL" << " " << index->termCount() << " " << index->documentCount() << std::endl;

  /*to use uncomment following code*/
  // FILE* f = fopen("/home/qi/reorder_data/inverted_index_all/term_infos", "w");
  FILE* f = fopen(term_lex.c_str(), "w");
  while( !iter->finished() ) {
    indri::index::DiskTermData* entry = iter->currentEntry();
    indri::index::TermData* termData = entry->termData;

    // std::cout << termData->term << " "
    //           << termData->corpus.documentCount << " " 
    //           << termData->corpus.totalCount <<  std::endl;
    fprintf(f, "%s %d %lu\n", termData->term, termData->corpus.documentCount, termData->corpus.totalCount);
    iter->nextEntry();
  }
  fclose(f);

  delete iter;
}


void FIndexBuilder::tera_query_lex_generation(string indri_index, string term_lex, string query, string parsed_query){
    /*load term info*/
    map<string, int> term_length_map;
    FILE* f = fopen(term_lex.c_str(), "r");
    char term[10000];
    int length;
    unsigned long total_count;
    while(fscanf(f, "%s %d %lu\n", term, &length, &total_count) == 3){
      string t(term);
      // cout << t << " " << length << "\n";
      term_length_map[t] = length;
    }
    fclose(f);

    indri::collection::Repository repository;
    const std::string indexPath = indri_index; 
    repository.openRead(indexPath);
    indri::server::LocalQueryServer local(repository);
    indri::collection::Repository::index_state state = repository.indexes();

    FILE* file = fopen(query.c_str(), "r");
    FILE* fl = fopen(parsed_query.c_str(), "w");
    char c[10000];
    while(fscanf(file, "%[^\n]\n", c) == 1) {
      string s(c);
      string d = ":";
      string token = s.substr(s.find_last_of(d)+1, s.size());
      string id = s.substr(0, s.find_first_of(d));
      string original_query = token;

      //replace '-' to ' '
      std::replace( token.begin(), token.end(), '-', ' ');

      //remove &
      token.erase(
      remove( token.begin(), token.end(), '&' ),
      token.end()
      );

      //remove "
      token.erase(
      remove( token.begin(), token.end(), '\"' ),
      token.end()
      );

      //remove '
      token.erase(
      remove( token.begin(), token.end(), '\'' ),
      token.end()
      );

      //remove .
      token.erase(
      remove( token.begin(), token.end(), '.' ),
      token.end()
      );

      //remove ?
      token.erase(
      remove( token.begin(), token.end(), '?' ),
      token.end()
      );

      //remove +
      token.erase(
      remove( token.begin(), token.end(), '+' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), '(' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), ')' ),
      token.end()
      );

      //remove ;
      token.erase(
      remove( token.begin(), token.end(), ';' ),
      token.end()
      );

      d = " ";
      size_t pos = 0;
      string term;
      vector<string> terms;
      if(token.find(d)!=string::npos){//remove all queries with only one term
        while((pos = token.find(d)) != std::string::npos){
           term = token.substr(0, pos);
           terms.push_back(term);
           token.erase(0, pos + d.length()); 
        }
        terms.push_back(token);

        vector<termInfo> termInfos;
        for(int i = 0; i < terms.size(); i++){
          std::string stem = repository.processTerm( terms[i] );
          if(term_length_map.find(stem)!=term_length_map.end()){ //remove terms that are not in the index
            termInfo t(stem, term_length_map[stem]);
            termInfos.push_back(t);
          }
        }
        // cout << "\n";
        if(termInfos.size() >= 2){
          sort(termInfos.begin(), termInfos.end(), sort_terms);
          // termInfos.resize(2);
          // sort(termInfos.begin(), termInfos.end(), sort_terms_by_term);
          if(termInfos[0].term.compare(termInfos[1].term)!=0){
            fprintf(fl, "%s %s %s %s\n", termInfos[0].term.c_str(), termInfos[1].term.c_str(), id.c_str(), original_query.c_str());
          }
        }
      }
    }
    fclose(file);
    fclose(fl);
}

void FIndexBuilder::stem_terms(string indri_index, string input_terms, string output_terms) {
    indri::collection::Repository repository;
    const std::string indexPath = indri_index; 
    repository.openRead(indexPath);
    //indri::server::LocalQueryServer local(repository);
    indri::collection::Repository::index_state state = repository.indexes();
    FILE* old_terms = fopen(input_terms.c_str(), "r");
    FILE* new_terms = fopen(output_terms.c_str(), "w");
    char term[2000];
    while (fscanf(old_terms, "%s\n", term) == 1) {
       string t(term);
       string stem = repository.processTerm(t);
       fprintf(new_terms, "%s\n", stem.c_str());
       cout << stem << endl;
    }
    fclose(old_terms);
    fclose(new_terms);
}

void FIndexBuilder::parse_query_tera2006(string indri_index, string term_lex, string query, string parsed_query){
    /*parse by :*/
    // std::string s = "1:2:test";
    // std::string d = ":";
    // std::string token = s.substr(s.find(d, 3)+1, s.size());
    // std::cout << token << "\n";

    /*remove "*/
    // s = "\"test1\"test2\"";
    // s.erase(
    // remove( s.begin(), s.end(), '\"' ),
    // s.end()
    // );
    // cout << s << endl;

    /*load term info*/
    map<string, int> term_length_map;
    FILE* f = fopen(term_lex.c_str(), "r");
    char term[10000];
    int length;
    unsigned long total_count;
    while(fscanf(f, "%s %d %lu\n", term, &length, &total_count) == 3){
      string t(term);
      // cout << t << " " << length << "\n";
      term_length_map[t] = length;
    }
    fclose(f);

    indri::collection::Repository repository;
    const std::string indexPath = indri_index; 
    repository.openRead(indexPath);
    indri::server::LocalQueryServer local(repository);
    indri::collection::Repository::index_state state = repository.indexes();

    FILE* file = fopen(query.c_str(), "r");
    FILE* fl = fopen(parsed_query.c_str(), "w");
    char c[10000];
    while(fscanf(file, "%[^\n]\n", c) == 1) {
      string s(c);
      string d = ":";
      string token = s.substr(s.find_first_of(d)+1, s.size());
      // cout << token << " || ";

      //replace '-' to ' '
      std::replace( token.begin(), token.end(), '-', ' ');

      //remove &
      token.erase(
      remove( token.begin(), token.end(), '&' ),
      token.end()
      );

      //remove "
      token.erase(
      remove( token.begin(), token.end(), '\"' ),
      token.end()
      );

      //remove '
      token.erase(
      remove( token.begin(), token.end(), '\'' ),
      token.end()
      );

      //remove .
      token.erase(
      remove( token.begin(), token.end(), '.' ),
      token.end()
      );

      //remove ?
      token.erase(
      remove( token.begin(), token.end(), '?' ),
      token.end()
      );

      //remove +
      token.erase(
      remove( token.begin(), token.end(), '+' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), '(' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), ')' ),
      token.end()
      );

      //remove ;
      token.erase(
      remove( token.begin(), token.end(), ';' ),
      token.end()
      );

      cout << token << "\n";

      d = " ";
      size_t pos = 0;
      string term;
      vector<string> terms;
      if(token.find(d)!=string::npos){//remove all queries with only one term
        while((pos = token.find(d)) != std::string::npos){
           term = token.substr(0, pos);
           terms.push_back(term);
           token.erase(0, pos + d.length()); 
        }
        terms.push_back(token);

        vector<termInfo> termInfos;
        for(int i = 0; i < terms.size(); i++){
          std::string stem = repository.processTerm( terms[i] );
          if(term_length_map.find(stem)!=term_length_map.end()){ //remove terms that are not in the index
            termInfo t(stem, term_length_map[stem]);
            termInfos.push_back(t);
          }
        }
        // cout << "\n";
        if(termInfos.size() >= 2){
          sort(termInfos.begin(), termInfos.end(), sort_terms);
          fprintf(fl, "%s %s\n", termInfos[0].term.c_str(), termInfos[1].term.c_str());
        }
      }
    }
    fclose(file);
    fclose(fl);
}

void FIndexBuilder::remove_qid_and_raw_query(string query_lex, string pair_input_for_lm){
  FILE* f = fopen(query_lex.c_str(), "r");
  FILE* f1 = fopen(pair_input_for_lm.c_str(), "w");
  char t1[10000];
  char t2[10000];
  char line[10000];
  while(fscanf(f, "%s %s %[^\n]\n", t1, t2, line) == 3){
    // cout << t1 << " " << t2 << "\n";
    fprintf(f1, "%s %s\n", t1, t2);
  }
  fclose(f);
  fclose(f1);
}

void FIndexBuilder::query_set_construction(string query_lex, string training_set, string testing_set, string other_set){

    FILE* f = fopen(query_lex.c_str(), "r");
    char t1[10000];
    char t2[10000];
    int qid;
    char line[10000];

    map< pair<string, string>, vector< pair<int,string> > > query_map;
    multiset<string> term_set;

    while(fscanf(f, "%s %s %d %[^\n]\n", t1, t2, &qid, line) == 4){
      // cout << t1 << " " << t2 << " " << qid << " " << line << "\n";
      string term1(t1);
      string term2(t2);
      term_set.insert(term1);
      term_set.insert(term2);
      pair<string, string> p(term1, term2);
      auto it = query_map.find(p);
      if(it == query_map.end()){
        vector<pair<int, string>> content;
        string query(line);
        pair<int, string> query_pair(qid, query);
        content.push_back(query_pair);
        query_map[p] = content; 
      }else{
        string query(line);
        pair<int, string> query_pair(qid, query);
        it->second.push_back(query_pair);
      }
    }
    fclose(f);

    cout << "number of unique queries: "<< query_map.size() << "\n";

    vector<QueryLexInfo> potential_test_vec;
    vector<QueryLexInfo> training_vec;
    vector<QueryLexInfo> unused_vec;

    int count = 0;
    FILE* f1 = fopen(testing_set.c_str(), "w");
    for(auto it = query_map.begin(); it!=query_map.end(); it++){
      // cout << it->first.first << " " << it->first.second << " " << it->second.size() << "\n";
      if(it->second.size() == 1) {
        QueryLexInfo q(it->second[0].first, it->first.first, it->first.second, it->second[0].second);
        // cout << it->second[0].second << "\n";
        unused_vec.push_back(q);
      }else{
        QueryLexInfo q(it->second[0].first, it->first.first, it->first.second, it->second[0].second);
        potential_test_vec.push_back(q);
        // cout << it->second[0].second << "\n";
        fprintf(f1, "%s %s %d %s\n", it->first.first.c_str(), it->first.second.c_str(), 
        it->second[0].first, it->second[0].second.c_str());
        for(int i = 1; i < it->second.size(); i++){
          // cout << it->second[i].second << "\n";
          QueryLexInfo q1(it->second[i].first, it->first.first, it->first.second, it->second[i].second);
          training_vec.push_back(q1);
        }
      }
    }
    fclose(f1);

    cout << unused_vec.size() << "\n";
    cout << training_vec.size() << "\n";
    cout << potential_test_vec.size() << "\n";

    /*randomly get remaining training queries*/
    int training_size = 19000;
    int remaining_for_training = training_size - training_vec.size();
    cout << remaining_for_training << "\n";

    vector<int> selected(unused_vec.size(), 0);
    srand (time(NULL));
    int counter = 0;
    while(counter < remaining_for_training){
      int num = rand() % unused_vec.size();
      // std::cout << num << "\n";
      if(selected[num] == 0){
        counter++;
        selected[num] = 1;
      }
    }


    /*fill up training query and write out unused query*/
    f1 = fopen(other_set.c_str(), "w");
    for(int i = 0; i < selected.size(); i++){
      if(selected[i] == 0){
        fprintf(f1, "%s %s %d %s\n", unused_vec[i].term1.c_str(), unused_vec[i].term2.c_str(), 
        unused_vec[i].qid, unused_vec[i].query.c_str());
      }else{
        training_vec.push_back(unused_vec[i]);
      }
    }
    fclose(f1);

    /*write out training*/
    f1 = fopen(training_set.c_str(), "w");
    for(int i = 0; i < training_vec.size(); i++){
      fprintf(f1, "%s %s %d %s\n", training_vec[i].term1.c_str(), training_vec[i].term2.c_str(), 
        training_vec[i].qid, training_vec[i].query.c_str());
    }
    fclose(f1);

    // map< pair<string, string>, pair< pair<int, string>, int> > other_map;

    // int count = 0;
    // FILE* f1 = fopen(other_set.c_str(), "w");
    // for(auto it = query_map.begin(); it!=query_map.end(); it++){
    //   // cout << it->first.first << " " << it->first.second << " " << it->second.size() << "\n";
    //   if(it->second.size() == 1) {
    //     int t1_count = term_set.count(it->first.first);
    //     int t2_count = term_set.count(it->first.second);
    //     other_map[it->first] = make_pair(it->second[0], t1_count+t2_count);
    //     fprintf(f1, "%s %s %d %s\n", it->first.first.c_str(), it->first.second.c_str(), 
    //     it->second[0].first, it->second[0].second.c_str());
    //   }else{

    //   }
    // }
    // fclose(f1);

    // // cout << "size of other set: " << other_map.size() << "\n";
    // for(auto it = other_map.begin(); it!=other_map.end(); it++){
    //   if( it->second.second >= 20){
    //     cout << it->first.first << " " << it->first.second << " " << it->second.first.first << " " << it->second.first.second  << " " << it->second.second << "\n";
    //   }
    // }
}

void FIndexBuilder::million_query_lex_generation(string indri_index, string term_lex, string query, string parsed_query){
    /*load term info*/
    map<string, int> term_length_map;
    FILE* f = fopen(term_lex.c_str(), "r");
    char term[10000];
    int length;
    unsigned long total_count;
    while(fscanf(f, "%s %d %lu\n", term, &length, &total_count) == 3){
      string t(term);
      // cout << t << " " << length << "\n";
      term_length_map[t] = length;
    }
    fclose(f);

    indri::collection::Repository repository;
    const std::string indexPath = indri_index; 
    repository.openRead(indexPath);
    indri::server::LocalQueryServer local(repository);
    indri::collection::Repository::index_state state = repository.indexes();

    FILE* file = fopen(query.c_str(), "r");
    FILE* fl = fopen(parsed_query.c_str(), "w");
    char c[10000];
    while(fscanf(file, "%[^\n]\n", c) == 1) {
      string s(c);
      string d = ":";
      string token = s.substr(s.find_last_of(d)+1, s.size());
      string id = s.substr(0, s.find_first_of(d));
      string original_query = token;

      //remove "
      token.erase(
      remove( token.begin(), token.end(), '\"' ),
      token.end()
      );

      //remove '
      token.erase(
      remove( token.begin(), token.end(), '\'' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), '?' ),
      token.end()
      );

      //remove +
      token.erase(
      remove( token.begin(), token.end(), '+' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), '(' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), ')' ),
      token.end()
      );

      d = " ";
      size_t pos = 0;
      string term;
      vector<string> terms;
      if(token.find(d)!=string::npos){//remove all queries with only one term
        while((pos = token.find(d)) != std::string::npos){
           term = token.substr(0, pos);
           terms.push_back(term);
           token.erase(0, pos + d.length()); 
        }
        terms.push_back(token);

        vector<termInfo> termInfos;
        for(int i = 0; i < terms.size(); i++){
          std::string stem = repository.processTerm( terms[i] );
          if(term_length_map.find(stem)!=term_length_map.end()){ //remove terms that are not in the index
            termInfo t(stem, term_length_map[stem]);
            termInfos.push_back(t);
          }
        }
        // cout << "\n";
        if(termInfos.size() >= 2){
          sort(termInfos.begin(), termInfos.end(), sort_terms);
          // termInfos.resize(2);
          // sort(termInfos.begin(), termInfos.end(), sort_terms_by_term);
          if(termInfos[0].term.compare(termInfos[1].term)!=0){
            fprintf(fl, "%s %s %s %s\n", termInfos[0].term.c_str(), termInfos[1].term.c_str(), id.c_str(), original_query.c_str());
          }
        }
      }
    }
    fclose(file);
    fclose(fl);
}

void FIndexBuilder::parse_query_million(string indri_index, string term_lex, string query, string parsed_query){

    /*load term info*/
    map<string, int> term_length_map;
    FILE* f = fopen(term_lex.c_str(), "r");
    char term[10000];
    int length;
    unsigned long total_count;
    while(fscanf(f, "%s %d %lu\n", term, &length, &total_count) == 3){
      string t(term);
      // cout << t << " " << length << "\n";
      term_length_map[t] = length;
    }
    fclose(f);

    indri::collection::Repository repository;
    const std::string indexPath = indri_index; 
    repository.openRead(indexPath);
    indri::server::LocalQueryServer local(repository);
    indri::collection::Repository::index_state state = repository.indexes();

    FILE* file = fopen(query.c_str(), "r");
    FILE* fl = fopen(parsed_query.c_str(), "w");
    char c[10000];
    while(fscanf(file, "%[^\n]\n", c) == 1) {
      string s(c);
      string d = ":";
      string token = s.substr(s.find_last_of(d)+1, s.size());

      //remove "
      token.erase(
      remove( token.begin(), token.end(), '\"' ),
      token.end()
      );

      //remove '
      token.erase(
      remove( token.begin(), token.end(), '\'' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), '?' ),
      token.end()
      );

      //remove +
      token.erase(
      remove( token.begin(), token.end(), '+' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), '(' ),
      token.end()
      );

      //remove (
      token.erase(
      remove( token.begin(), token.end(), ')' ),
      token.end()
      );

      d = " ";
      size_t pos = 0;
      string term;
      vector<string> terms;
      if(token.find(d)!=string::npos){//remove all queries with only one term
        while((pos = token.find(d)) != std::string::npos){
           term = token.substr(0, pos);
           terms.push_back(term);
           token.erase(0, pos + d.length()); 
        }
        terms.push_back(token);

        vector<termInfo> termInfos;
        for(int i = 0; i < terms.size(); i++){
          std::string stem = repository.processTerm( terms[i] );
          if(term_length_map.find(stem)!=term_length_map.end()){ //remove terms that are not in the index
            termInfo t(stem, term_length_map[stem]);
            termInfos.push_back(t);
          }
        }
        // cout << "\n";
        if(termInfos.size() >= 2){
          sort(termInfos.begin(), termInfos.end(), sort_terms);
          fprintf(fl, "%s %s\n", termInfos[0].term.c_str(), termInfos[1].term.c_str());
        }
      }
    }
    fclose(file);
    fclose(fl);
}

void FIndexBuilder::inverted_index_r10k(){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    long long offset = 0;
    std::vector<lemur::api::DOCID_T> documentIDs;

    std::string lex_file = "/home/qi/reorder_data/r10k_forward_index/lex_219";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    int did;
    while(fscanf(pfile, "%d %*[^\n]", &did) == 1) {
      std::cout << did << "\n";
      documentIDs.push_back(did);
    }
    fclose(pfile);

    parsedDocs=indriEnvironment.documents(documentIDs);
    std::vector<posting> postings; 
    vector <int> termIds;

    for (int i=0; i < documentIDs.size(); i++) {
      /*get list content*/
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);
      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      std::sort(termIds.begin(), termIds.end());
      // std::vector<int>::iterator ip;
      // ip = std::unique(termIds.begin(), termIds.end());
      // termIds.resize(std::distance(termIds.begin(), ip));
      /*go through list and get postings*/
      int tid;
      int freq = 1;
      if(termIds.size() > 1) {
        int pre_tid = termIds[0];
        // std::cout << pre_tid << "\n";
        for(int j = 1; j<termIds.size(); j++){
          if(termIds[j] == pre_tid){
            freq ++;
          }else{
            // std::cout << documentIDs[i] << " " << pre_tid << " " << freq << "\n";
            posting p(documentIDs[i], pre_tid, freq);
            postings.push_back(p);
            freq = 1;
            pre_tid = termIds[j];
          }
        }
        /*flush the buffer*/
        posting p(documentIDs[i], pre_tid, freq);
        postings.push_back(p);
      }else{
        posting p(documentIDs[i], termIds[0], freq);
        postings.push_back(p);
      }
      termIds.clear();
    }
    std::sort(postings.begin(), postings.end(), sort_postings);
    std::cout << postings.size() << std::endl;

    /*go through posting lists, write invert lists*/
    std::ofstream outfile;
    outfile.open("/home/qi/reorder_data/r10k_inverted_index/index", std::ios::binary | std::ios::out);
    int tid;
    int listlen = 1;
    int pre_tid = postings[0].tid;
    outfile.write(reinterpret_cast<const char *>(&postings[0].did), sizeof(int));
    outfile.write(reinterpret_cast<const char *>(&postings[0].freq), sizeof(int));
    std::vector<int> term_ids;
    std::vector<int> listlengths;
    std::vector<long long> offsets;

    std::cout << "go through postings list\n";
    for(int i=1; i<postings.size(); i++){
      if(postings[i].tid == pre_tid) {
        listlen ++;
      }else{
        term_ids.push_back(pre_tid);
        listlengths.push_back(listlen);
        offsets.push_back(offset);
        offset += listlen*8;//freq and did each uses 4 bytes
        listlen = 1;
        pre_tid = postings[i].tid;
      }
      /*write out dids and freqs*/
      outfile.write(reinterpret_cast<const char *>(&postings[i].did), sizeof(int));
      outfile.write(reinterpret_cast<const char *>(&postings[i].freq), sizeof(int));
    }
    term_ids.push_back(pre_tid);
    listlengths.push_back(listlen);
    offsets.push_back(offset);
    offset += listlen*8;

    // for(int i=0; i<postings.size(); i++){
    //   if(postings[i].tid == 718) {
    //     std::cout << postings[i].tid << " " << postings[i].did << " " << postings[i].freq << "\n";
    //   }
    //   if(postings[i].tid > 718) {
    //     break;
    //   }
    // }
    outfile.close();

    lex_file = "/home/qi/reorder_data/r10k_inverted_index/lex";
    pfile = fopen(lex_file.c_str(), "w");
    for(int i = 0; i < term_ids.size(); i++) {
      // std::cout << term_ids[i] << " " << listlengths[i] << " " << offsets[i] << "\n";
      fprintf(pfile, "%d %d %lld\n", term_ids[i], listlengths[i], offsets[i]);
    }
    fclose(pfile);
}

void FIndexBuilder::forward_index_r10k_url_ordering(){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    long long offset = 0;
    std::vector<lemur::api::DOCID_T> documentIDs;
    std::vector<int> doc_sizes;
    std::vector<long long> offsets;

    int cap = 10000;

    documentIDs.reserve(cap);
    doc_sizes.reserve(cap);
    offsets.reserve(cap);

    std::string lex_file = "/home/qi/reorder_data/r10k_forward_index/lex_219";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    int did;
    while(fscanf(pfile, "%d %*[^\n]", &did) == 1) {
      std::cout << did << "\n";
      documentIDs.push_back(did);
    }
    fclose(pfile);

    parsedDocs=indriEnvironment.documents(documentIDs);

    for (int i=0; i < documentIDs.size(); i++) {
      std::string documentName = getDocumentName(parsedDocs[i]);
      std::string documentUrl = getDocumentUrl(parsedDocs[i]);

      docInfo d(documentIDs[i], documentName);
      url_map[documentUrl] = d;

      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);
      doc_sizes.push_back(termList->terms().size());
      offsets.push_back(offset);
      offset += termList->terms().size();

    }

    lex_file = "/home/qi/reorder_data/r10k_forward_index/lex_with_offset";
    pfile = fopen(lex_file.c_str(), "w");
    std::ofstream outfile;
    outfile.open("/home/qi/reorder_data/r10k_forward_index/index", std::ios::binary | std::ios::out);
    int new_did = 0;
    for (std::map<std::string,docInfo>::iterator it=url_map.begin(); it!=url_map.end(); ++it) {
      new_did ++;
      std::cout << new_did << " " << it->first << " " << it->second.did << " " << it->second.dname << '\n';
      fprintf(pfile, "%d %d %d %lld %s %s\n", new_did, it->second.did, doc_sizes[new_did-1], offsets[new_did-1], it->second.dname.c_str(), it->first.c_str());
      /*get list content*/
      vector <int> termIds;
      const indri::index::TermList *termList = thisIndex->termList(it->second.did);

      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      /*sort the list and do delta*/
      //todo

      /*write out postings*/
      for(int j=0; j<termIds.size(); ++j){
        // std::cout << termIds[j] << " ";
        // std::cout << thisIndex->term(termIds[j]) << " ";
        outfile.write(reinterpret_cast<const char *>(&termIds[j]), sizeof(int));
      }
    }
    fclose(pfile);
    outfile.close();
}

void FIndexBuilder::random_10k_lex(){
  std::string lex_file = "/home/qi/reorder_data/forward_index_all/url_sorted_lex_all";
  FILE* pfile = fopen(lex_file.c_str(), "r");
  int new_did, did;
  char dname[1000];
  char url[10000];
  std::map<int, wholeDocInfo> the_map;
  while(fscanf(pfile, "%d %d %s %s", &new_did, &did, dname, url) == 4) {
    std::string s1(dname);
    std::string s2(url);
    wholeDocInfo d(did, s1, s2);
    the_map[new_did] =  d;
  }
  fclose(pfile);

  std::vector<int> ids;
  std::unordered_set<int> nums;
  srand (time(NULL));
  int counter = 0;
  while(counter < 10){
    int num = rand() % 25171 + 1;
    std::cout << num << "\n";
    if(nums.find(num) == nums.end()) {
       nums.insert(num);
       for(int i = (num-1)*1000 + 1; i <= num*1000; i++){
          ids.push_back(i);
       }
       counter ++;
    }
  }

  std::cout << ids.size() << std::endl;

  lex_file = "/home/qi/reorder_data/r10k_forward_index/lex";
  pfile = fopen(lex_file.c_str(), "w");
  for(int i = 0; i < ids.size(); i++) {
    // std::cout << ids[i] << " " << the_map[ids[i]].did << " " << the_map[ids[i]].dname << " " << the_map[ids[i]].url << "\n";
    fprintf(pfile, "%d %s %s\n", the_map[ids[i]].did, the_map[ids[i]].dname.c_str(), the_map[ids[i]].url.c_str());
  }
  fclose(pfile);
}
void FIndexBuilder::shuffle_all_lex(){
    std::string lex_file = "/home/qi/reorder_data/forward_index_all/lex_all";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    int did;
    char dname[1000];
    char url[10000];
    while(fscanf(pfile, "%d %s %s\n", &did, dname, url)==3) {
      string s1(dname);
      string s2(url);
      docInfo d(did, s1);
      url_map[s2] = d;
    }
    fclose(pfile);

    lex_file = "/home/qi/reorder_data/forward_index_all/url_sorted_lex_all";
    pfile = fopen(lex_file.c_str(), "w");
    int new_did = 0;
    for (std::map<std::string,docInfo>::iterator it=url_map.begin(); it!=url_map.end(); ++it) {
      new_did++;
      // std::cout << new_did << " " << it->first << " " << it->second.did << " " << it->second.dname << '\n';
      fprintf(pfile, "%d %d %s %s\n", new_did, it->second.did, it->second.dname.c_str(), it->first.c_str());
    }
    fclose(pfile);
}

void FIndexBuilder::lookup_tid(string term){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    int tid = thisIndex->term(term);

    std::cout << tid << " " << term << "\n";
}

void FIndexBuilder::lookup_term(int tid){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    string term = thisIndex->term(tid);

    std::cout << tid << " " << term << "\n";

}

void FIndexBuilder::add_term_to_lex(){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    std::vector<string> terms;
    std::vector<int> term_ids;
    std::vector<int> listlengths;
    std::vector<int> offsets;
    const std::string lex_file = "/home/qi/reorder_data/query_info/query_lex";
    FILE* pfile = fopen(lex_file.c_str(), "r");
    const std::string lex_file_term = "/home/qi/reorder_data/query_info/query_lex_term";
    FILE* pfilea = fopen(lex_file_term.c_str(), "w");   
    int tid, listlen;
    long long offset;
    while(fscanf(pfile, "%d %d %lld\n", &tid, &listlen, &offset)==3) {
      string term = thisIndex->term(tid);
      std::cout << term << " " << tid << " " << listlen << " " << offset << "\n";
      fprintf(pfilea, "%s %d %d %lld\n", term.c_str(), tid, listlen, offset);
    }
    fclose(pfile);
    fclose(pfilea);
}

void FIndexBuilder::verify_vbyte_forward_index(string indri_index, string vbyte_lex, string vbyte_index, int max_d, long long data_size, int d){

  /*load lex*/
  // FILE* lex = fopen("/home/qi/reorder_data/forward_index_with_dups/all_nlex", "r");
  FILE* lex = fopen(vbyte_lex.c_str(), "r");
  int did, size, length;
  long long offset;
  vector<int> lengths;
  vector<int> sizes;
  vector<long long> offsets; 

  sizes.reserve(max_d);
  lengths.reserve(max_d);
  offsets.reserve(max_d);
  for(int i = 0; i < max_d; i++){
    sizes.push_back(0);
    lengths.push_back(0);
    offsets.push_back(0);
  }

  while(fscanf(lex, "%d %d %d %lld", &did, &length, &size, &offset) == 4) {
    // if(did == 100){
    //   cout << did << " " << length << " " << size << " " << offset << "\n";
    // }
    // cout << did << " " << length << " " << size << " " << offset << "\n";
    sizes[did] = size;
    lengths[did] = length;
    offsets[did] = offset;
  }
  fclose(lex);

  // for(int i = 1; i < MAXD; i++){
  //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
  // }
  cout << "lex loading done\n";

  /*load content*/
  FILE* fp = fopen(vbyte_index.c_str(),"rb");
  char* data = new char[data_size];
  long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
  if(ret_code == data_size) {
      puts("data read successfully");
  } else { // error handling
     if (feof(fp))
        printf("Error reading test.bin: unexpected end of file\n");
     else if (ferror(fp)) {
         perror("Error reading test.bin");
     }
  }
  fclose(fp);

  /*take out did content and decompress*/
  size = sizes[d];
  length = lengths[d];
  offset = offsets[d];
  // cout << d << " " << size << " " << length << " " << offset << endl;
  // exit(0);
  char* d_data = new char[size];
  int* uncompressed_data = new int[length];
  // char d_data[size];
  // int uncompressed_data[length];
  for(int i = 0; i < size; i++){
    d_data[i] = data[offset+i];
  }

  decompressionVbytes(d_data, uncompressed_data, length);

  // for(int i = 0; i < length; i++){
  //   cout << uncompressed_data[i] << " "; 
  // }
  // cout << "\n";

  for(int i = 1; i < length; i++){
    uncompressed_data[i]+= uncompressed_data[i-1]; 
  }

  for(int i = 0; i < length; i++){
    cout << uncompressed_data[i] << " "; 
  }
  cout << "\n";  

  delete[] data;
  delete[] d_data;

  /*take out origin document content*/
  QueryEnvironment indriEnvironment;
  indri::collection::Repository repository;
  std::vector<ParsedDocument*> parsedDocs;
  indri::index::Index *thisIndex;
  // const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 
  const std::string indexPath = indri_index;

  indriEnvironment.addIndex(indexPath);
  repository.openRead(indexPath);
  indri::collection::Repository::index_state repIndexState = repository.indexes();
  thisIndex=(*repIndexState)[0];

  std::vector<lemur::api::DOCID_T> documentIDs;
  documentIDs.push_back(d);

  parsedDocs=indriEnvironment.documents(documentIDs);
  vector <int> termIds;

  for (int i=0; i < documentIDs.size(); i++) {
    /*get list content*/
    const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);

    for(int j=0; j<termList->terms().size(); ++j){
      if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
        termIds.push_back(termList->terms()[j]);
      }
    }

    sort(termIds.begin(), termIds.end());
    /*unique the tids*/
    std::vector<int>::iterator it;
    it = std::unique (termIds.begin(), termIds.end());        
    termIds.resize( std::distance(termIds.begin(),it) ); 

    for(int j=0; j<termIds.size(); ++j){
      std::cout << termIds[j] << " ";
    }
    std::cout << std::endl;
  }

  bool equal = true;

  for(int i = 0; i < length; i++){
    if(uncompressed_data[i] != termIds[i]){
      equal = false;
      // cout << uncompressed_data[i] << " " << termIds[i] << " not equal\n";
    }
  }

  if(equal){
    cout << "forward index verified!\n";
  }else{
    cout << "forward index not verified\n";
  }

  delete[] uncompressed_data;
}

void FIndexBuilder::doc_length_using_doc_range(int start_id, int end_id, string indri_index, string doc_length_lex){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    //std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = indri_index; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    // int start_did = 25000001;
    // int end_did = 25205179;
    int start_did = start_id;
    int end_did = end_id;
    std::vector<lemur::api::DOCID_T> documentIDs;

    std::cout << "start_did: " << start_did << " end_did: " << end_did << std::endl;

    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    std::string lex_file = doc_length_lex;
    int data_size = 0;
    int size = 0;
    int unit_size = 10000000;
    std::vector<unsigned char> list;
    list.reserve(unit_size);
    FILE* pfile = fopen(lex_file.c_str(), "w");
    for (int i=0; i < documentIDs.size(); i++) {
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);      
      //std::cout << documentIDs[i] << " " << termList->terms().size() << "\n";
      fprintf(pfile, "%d %lu\n", documentIDs[i], termList->terms().size());
      delete termList;
    }
    fclose(pfile);
}

void FIndexBuilder::doc_content_using_doc_range(int start_id, int end_id, string indri_index, string vbyte_lex, string vbyte_index, long long off){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = indri_index; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    // int start_did = 25000001;
    // int end_did = 25205179;
    int start_did = start_id;
    int end_did = end_id;
    int unit = 1000000;
    std::vector<lemur::api::DOCID_T> documentIDs;
    documentIDs.reserve(unit);

    std::cout << "start_did: " << start_did << " end_did: " << end_did << std::endl;

    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    parsedDocs=indriEnvironment.documents(documentIDs);

    std::string lex_file = vbyte_lex;
    std::string content_file = vbyte_index;
    int data_size = 0;
    int size = 0;
    long long offset = off;
    int unit_size = 10000000;
    std::vector<unsigned char> list;
    list.reserve(unit_size);
    FILE* pfile = fopen(lex_file.c_str(), "w");
    for (int i=0; i < documentIDs.size(); i++) {
      std::cout << documentIDs[i] << "\n";
      /*get document content*/
      vector <int> termIds;
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);

      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      /*sort and unique content*/
      /*sort the tids*/
      sort(termIds.begin(), termIds.end());
      /*unique the tids*/
      std::vector<int>::iterator it;
      it = std::unique (termIds.begin(), termIds.end());        
      termIds.resize( std::distance(termIds.begin(),it) ); 
      size = termIds.size();
      // for(int j=0; j<termIds.size(); ++j){
      //   std::cout << termIds[j] << " ";
      //   // std::cout << thisIndex->term(termIds[j]) << " ";
      // }
      // std::cout << "\n";

      /*take delta of the tids*/
      for(int k = termIds.size()-1; k > 0; k--){
          termIds[k] = termIds[k] - termIds[k-1];
      }

      // std::cout << documentIDs[i] << std::endl;
      // for(int j=0; j<termIds.size(); ++j){
      //   std::cout << termIds[j] << " ";
      //   // std::cout << thisIndex->term(termIds[j]) << " ";
      // }
      // std::cout << "\n";

      /*compress content, write out content*/
      if(list.size() >= unit_size) {
        std::cout << "write out content " << list.size() << "\n";
        dumpToFile(content_file,  list.begin(), list.end());
        list.clear();
      }
      data_size = compressionVbytes(termIds, list);

      /*write out lex*/
      fprintf(pfile, "%d %d %d %lld\n", documentIDs[i], size, data_size, offset);
      offset += data_size;
    }

    /*write out remaining content*/
    std::cout << "write out remaining content " << list.size() << "\n";
    dumpToFile(content_file,  list.begin(), list.end());
    fclose(pfile);
    cout << "last offset: " << offset << "\n";
}

void FIndexBuilder::doc_content_patch(string indri_index, string vbyte_lex, string vbyte_index, int patch, long long off){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    // const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 
    const std::string indexPath = indri_index; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    std::cout << "patch: " << patch << std::endl;
    int unit = 1000000;
    int start_did = (patch-1)*unit + 1;
    int end_did = patch * unit;
    std::vector<lemur::api::DOCID_T> documentIDs;
    documentIDs.reserve(unit);

    std::cout << "start_did: " << start_did << " end_did: " << end_did << std::endl;

    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    parsedDocs=indriEnvironment.documents(documentIDs);

    // std::string lex_file = "/home/qi/reorder_data/forward_index_with_dups/nlex" + std::to_string(patch);
    std::string lex_file = vbyte_lex + std::to_string(patch);
    // std::string content_file = "/home/qi/reorder_data/forward_index_content/content";
    std::string content_file = vbyte_index;
    int data_size = 0;
    int size = 0;
    long long offset = off;
    int unit_size = 10000000;
    std::vector<unsigned char> list;
    list.reserve(unit_size);
    FILE* pfile = fopen(lex_file.c_str(), "w");
    for (int i=0; i < documentIDs.size(); i++) {
      std::cout << documentIDs[i] << "\n";
      /*get document content*/
      vector <int> termIds;
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);

      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }
      /*sort and unique content*/
      /*sort the tids*/
      sort(termIds.begin(), termIds.end());
      /*unique the tids*/
      std::vector<int>::iterator it;
      it = std::unique (termIds.begin(), termIds.end());        
      termIds.resize( std::distance(termIds.begin(),it) ); 
      size = termIds.size();
      // for(int j=0; j<termIds.size(); ++j){
      //   std::cout << termIds[j] << " ";
      //   // std::cout << thisIndex->term(termIds[j]) << " ";
      // }
      // std::cout << "\n";

      /*take delta of the tids*/
      for(int k = termIds.size()-1; k > 0; k--){
          termIds[k] = termIds[k] - termIds[k-1];
      }

      // std::cout << documentIDs[i] << std::endl;
      // for(int j=0; j<termIds.size(); ++j){
      //   std::cout << termIds[j] << " ";
      //   // std::cout << thisIndex->term(termIds[j]) << " ";
      // }
      // std::cout << "\n";

      /*compress content, write out content*/
      if(list.size() >= unit_size) {
        std::cout << "write out content " << list.size() << "\n";
        dumpToFile(content_file,  list.begin(), list.end());
        list.clear();
      }
      data_size = compressionVbytes(termIds, list);

      /*write out lex*/
      fprintf(pfile, "%d %d %d %lld\n", documentIDs[i], size, data_size, offset);
      offset += data_size;
    }

    /*write out remaining content*/
    std::cout << "write out remaining content " << list.size() << "\n";
    dumpToFile(content_file,  list.begin(), list.end());
    fclose(pfile);
    cout << "last offset: " << offset << "\n";
}

bool invalidChar (char c) 
{       
    // if( c>=0 && c<128){
    //   return false;
    // }else{
    //   return true;
    // }
    if( c < 0) {
      return true;
    }else if( c >= 128){
      return true;
    }else if( c>=7 && c<=13){ // remove \a \b \t \v \n \f \r
      return true;
    }else if( c == 27){
      return true;
    }else{
      return false;
    }
} 

void FIndexBuilder::forward_lex_patch_with_dups(int patch, string indri_index, string output_lex){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    // const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 
    const std::string indexPath = indri_index; 


    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    std::cout << "patch: " << patch << std::endl;
    int unit = 1000000;
    int start_did = (patch-1)*1000000 + 1;
    // int end_did = 25205179;
    int end_did = patch * unit;
    std::vector<lemur::api::DOCID_T> documentIDs;
    documentIDs.reserve(unit);

    std::cout << "start_did: " << start_did << " end_did: " << end_did << std::endl;

    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    parsedDocs=indriEnvironment.documents(documentIDs);

    // std::string lex_file = "/home/qi/reorder_data/forward_index_with_dups/lex" + std::to_string(patch);
    std::string lex_file = output_lex + std::to_string(patch);
    FILE* pfile = fopen(lex_file.c_str(), "w");
    for (int i=0; i < documentIDs.size(); i++) {
      std::string documentName = getDocumentName(parsedDocs[i]);
      std::string documentUrl = getDocumentUrl(parsedDocs[i]);
      /*remove non-ascii and some other disruptive char in url*/
      documentUrl.erase(remove_if(documentUrl.begin(), documentUrl.end(), invalidChar), documentUrl.end());
      fprintf(pfile, "%s %d %s\n", documentUrl.c_str(), documentIDs[i], documentName.c_str());

    }
    fclose(pfile);
}

void FIndexBuilder::forward_lex_using_doc_range_free_memory(int start_id, int end_id, string indri_index, string output_lex){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    indri::index::Index *thisIndex;
    const std::string indexPath = indri_index; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    int start_did = start_id;
    int end_did = end_id;
    // int start_did = 25000001;
    // int end_did = 25205179;
    std::string lex_file = output_lex;
    FILE* pfile = fopen(lex_file.c_str(), "w");
     
    std::cout << "start_did: " << start_did << " end_did: " << end_did << std::endl;

    for(int i = start_did; i <= end_did; ++i){
      std::vector<ParsedDocument*> parsedDocs;
      std::vector<lemur::api::DOCID_T> documentIDs;
      documentIDs.push_back(i);
      parsedDocs=indriEnvironment.documents(documentIDs); 
      std::string documentName = getDocumentName(parsedDocs[0]);
      std::string documentUrl = getDocumentUrl(parsedDocs[0]);
      documentUrl.erase(remove_if(documentUrl.begin(), documentUrl.end(), invalidChar), documentUrl.end());
      // cout << documentUrl << "\n";
      fprintf(pfile, "%s %d %s\n", documentUrl.c_str(), i, documentName.c_str());
      delete parsedDocs[0];
    }
    fclose(pfile); 
}



void FIndexBuilder::forward_lex_using_doc_range(int start_id, int end_id, string indri_index, string output_lex){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = indri_index; 
    // const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    int start_did = start_id;
    int end_did = end_id;
    // int start_did = 25000001;
    // int end_did = 25205179;
    std::vector<lemur::api::DOCID_T> documentIDs;
    documentIDs.reserve(end_did);

    std::cout << "start_did: " << start_did << " end_did: " << end_did << std::endl;

    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    parsedDocs=indriEnvironment.documents(documentIDs);

    std::string lex_file = output_lex;
    FILE* pfile = fopen(lex_file.c_str(), "w");
    for (int i=0; i < documentIDs.size(); i++) {
      std::string documentName = getDocumentName(parsedDocs[i]);
      std::string documentUrl = getDocumentUrl(parsedDocs[i]);
      /*remove non-ascii and some other disruptive char in url*/
      documentUrl.erase(remove_if(documentUrl.begin(), documentUrl.end(), invalidChar), documentUrl.end());
      // cout << documentUrl << "\n";

      /*some testing code here*/
      // int n = documentUrl.length(); 
      // // declaring character array
      // char char_array[n+1]; 
      // // copying the contents of the 
      // // string to char array
      // strcpy(char_array, documentUrl.c_str()); 
       
      // for (int i=0; i<n; i++)
      //   cout << +char_array[i] << "\n";  

      fprintf(pfile, "%s %d %s\n", documentUrl.c_str(), documentIDs[i], documentName.c_str());

    }
    fclose(pfile);
  
}

void FIndexBuilder::forward_index_test(){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    int start_did = 1;
    // int end_did = 25205179;
    int end_did = 10000;
    long long offset = 0;
    std::vector<lemur::api::DOCID_T> documentIDs;
    std::vector<int> doc_sizes;
    std::vector<long long> offsets;

    documentIDs.reserve(end_did);
    doc_sizes.reserve(end_did);
    offsets.reserve(end_did);

    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    parsedDocs=indriEnvironment.documents(documentIDs);

    for (int i=0; i < documentIDs.size(); i++) {
      std::string documentName = getDocumentName(parsedDocs[i]);
      std::string documentUrl = getDocumentUrl(parsedDocs[i]);

      docInfo d(documentIDs[i], documentName);
      url_map[documentUrl] = d;

      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);
      doc_sizes.push_back(termList->terms().size());
      offsets.push_back(offset);
      offset += termList->terms().size();

    }

    const std::string lex_file = "/home/qw376/Indri_data/forward_index_10k/lex";
    FILE* pfile = fopen(lex_file.c_str(), "w");
    std::ofstream outfile;
    outfile.open("/home/qw376/Indri_data/forward_index_10k/index", std::ios::binary | std::ios::out);
    int new_did = 0;
    for (std::map<std::string,docInfo>::iterator it=url_map.begin(); it!=url_map.end(); ++it) {
      new_did ++;
      std::cout << new_did << " " << it->first << " " << it->second.did << " " << it->second.dname << '\n';
      fprintf(pfile, "%d %d %d %lld %s %s\n", new_did, it->second.did, doc_sizes[new_did-1], offsets[new_did-1], it->second.dname.c_str(), it->first.c_str());
      /*get list content*/
      vector <int> termIds;
      const indri::index::TermList *termList = thisIndex->termList(it->second.did);

      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      /*sort the list and do delta*/
      //todo

      /*write out postings*/
      for(int j=0; j<termIds.size(); ++j){
        // std::cout << termIds[j] << " ";
        // std::cout << thisIndex->term(termIds[j]) << " ";
        outfile.write(reinterpret_cast<const char *>(&termIds[j]), sizeof(int));
      }
    }
    fclose(pfile);
    outfile.close();
}

void FIndexBuilder::build_small_lex(){
  FILE* file = fopen("/home/qw376/Indri_data/inverted_index_10k/lex", "r");
  int tid, listlen;
  long long offset;
  std::map<int, lexInfo> lex_map;
  while(fscanf(file, "%d %d %lld\n", &tid, &listlen, &offset) == 3){
    // std::cout << tid << " " <<  listlen << " " << offset << "\n";
    lexInfo l(listlen, offset);
    lex_map[tid] = l;
  }
  fclose(file);

  FILE* pfile = fopen("/home/qw376/Indri_data/query/test_query", "r");
  FILE* ofile = fopen("/home/qw376/Indri_data/query/test_query_lex", "w");
  int tid1, tid2;
  while(fscanf(pfile, "%d %d\n", &tid1, &tid2) == 2) {
    std::cout << tid1 << " " << tid2 << "\n"; 
    std::cout << tid1 << " " << lex_map[tid1].listlen << " " << lex_map[tid1].offset << "\n";
    std::cout << tid2 << " " << lex_map[tid2].listlen << " " << lex_map[tid2].offset << "\n";
    fprintf(ofile, "%d %d %lld\n", tid1, lex_map[tid1].listlen, lex_map[tid1].offset);
    fprintf(ofile, "%d %d %lld\n", tid2, lex_map[tid2].listlen, lex_map[tid2].offset);
  }
  fclose(pfile);
  fclose(ofile);
}

void FIndexBuilder::inverted_index_test(){
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    int start_did = 1;
    // int end_did = 25205179;
    int end_did = 10000;
    long long offset = 0;
    std::vector<lemur::api::DOCID_T> documentIDs;

    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    parsedDocs=indriEnvironment.documents(documentIDs);
    std::vector<posting> postings; 
    vector <int> termIds;

    for (int i=0; i < documentIDs.size(); i++) {
      /*get list content*/
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);
      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      std::sort(termIds.begin(), termIds.end());
      // std::vector<int>::iterator ip;
      // ip = std::unique(termIds.begin(), termIds.end());
      // termIds.resize(std::distance(termIds.begin(), ip));
      /*go through list and get postings*/
      int tid;
      int freq = 1;
      if(termIds.size() > 1) {
        int pre_tid = termIds[0];
        // std::cout << pre_tid << "\n";
        for(int j = 1; j<termIds.size(); j++){
          if(termIds[j] == pre_tid){
            freq ++;
          }else{
            // std::cout << documentIDs[i] << " " << pre_tid << " " << freq << "\n";
            posting p(documentIDs[i], pre_tid, freq);
            postings.push_back(p);
            freq = 1;
            pre_tid = termIds[j];
          }
        }
        /*flush the buffer*/
        posting p(documentIDs[i], pre_tid, freq);
        postings.push_back(p);
      }else{
        posting p(documentIDs[i], termIds[0], freq);
        postings.push_back(p);
      }
      termIds.clear();
    }
    std::sort(postings.begin(), postings.end(), sort_postings);
    std::cout << postings.size() << std::endl;

    /*go through posting lists, write invert lists*/
    std::ofstream outfile;
    outfile.open("/home/qw376/Indri_data/inverted_index_10k/index", std::ios::binary | std::ios::out);
    int tid;
    int listlen = 1;
    int pre_tid = postings[0].tid;
    outfile.write(reinterpret_cast<const char *>(&postings[0].did), sizeof(int));
    outfile.write(reinterpret_cast<const char *>(&postings[0].freq), sizeof(int));
    std::vector<int> term_ids;
    std::vector<int> listlengths;
    std::vector<long long> offsets;

    std::cout << "go through postings list\n";
    for(int i=1; i<postings.size(); i++){
      if(postings[i].tid == pre_tid) {
        listlen ++;
      }else{
        term_ids.push_back(pre_tid);
        listlengths.push_back(listlen);
        offsets.push_back(offset);
        offset += listlen*8;//freq and did each uses 4 bytes
        listlen = 1;
        pre_tid = postings[i].tid;
      }
      /*write out dids and freqs*/
      outfile.write(reinterpret_cast<const char *>(&postings[i].did), sizeof(int));
      outfile.write(reinterpret_cast<const char *>(&postings[i].freq), sizeof(int));
    }
    term_ids.push_back(pre_tid);
    listlengths.push_back(listlen);
    offsets.push_back(offset);
    offset += listlen*8;

    for(int i=0; i<postings.size(); i++){
      if(postings[i].tid == 718) {
        std::cout << postings[i].tid << " " << postings[i].did << " " << postings[i].freq << "\n";
      }
      if(postings[i].tid > 718) {
        break;
      }
    }
    outfile.close();

    const std::string lex_file = "/home/qw376/Indri_data/inverted_index_10k/lex";
    FILE* pfile = fopen(lex_file.c_str(), "w");
    for(int i = 0; i < term_ids.size(); i++) {
      // std::cout << term_ids[i] << " " << listlengths[i] << " " << offsets[i] << "\n";
      fprintf(pfile, "%d %d %lld\n", term_ids[i], listlengths[i], offsets[i]);
    }
    fclose(pfile);
}

void FIndexBuilder::verify_index(){
  /*load index*/
  std::ifstream in("/home/qw376/Indri_data/inverted_index_10k/index", std::ifstream::ate | std::ifstream::binary);
  int file_size = in.tellg();
  in.close();

  std::cout << "file_size:" << file_size << "\n";

  char * buffer = new char [file_size];
  std::ifstream is ("/home/qw376/Indri_data/inverted_index_10k/index", std::ifstream::binary);
  is.read (buffer, file_size);

  if (is)
      std::cout << "all characters read successfully.\n";
    else
      std::cout << "error: only " << is.gcount() << " could be read\n";
  is.close();

  int length = 0;
  char* ptr1 = buffer;
  int did, freq, loop;
  char c1, c2, c3, c4;
  while(length++ < 1636) {
    did = 0;
    freq = 0;

    c1 = *ptr1++;
    c2 = *ptr1++;
    c3 = *ptr1++;
    c4 = *ptr1++;

    did += (int)c4 & 0x000000ff;
    did = did << 8;
    // std::cout << did << " ";
    did += (int)c3 & 0x000000ff;
    did = did << 8;
    // std::cout << did << " ";
    did += (int)c2 & 0x000000ff;
    did = did << 8;
    // std::cout << did << " ";
    did += (int)c1 & 0x000000ff;
    // std::cout << did << "\n";

    string b1 = std::bitset<8>(c1).to_string();
    string b2 = std::bitset<8>(c2).to_string();
    string b3 = std::bitset<8>(c3).to_string();
    string b4 = std::bitset<8>(c4).to_string();

    // std::cout << (unsigned int)c1 << " " << (unsigned int)c2 << " " << (unsigned int)c3 << " " << (unsigned int)c4 << "\n";
    std::cout << b1 << " " << b2 << " " << b3 << " " << b4 << "\n";  

    c1 = *ptr1++;
    c2 = *ptr1++;
    c3 = *ptr1++;
    c4 = *ptr1++;

    freq += (int)c4 & 0x000000ff;
    freq = freq << 8;
    freq += (int)c3 & 0x000000ff;
    freq = freq << 8;
    freq += (int)c2 & 0x000000ff;
    freq = freq << 8;
    freq += (int)c1 & 0x000000ff;

    std::cout << did << " " << freq << "\n";
  }
}

void FIndexBuilder::show_documents(int did){

    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    std::vector<lemur::api::DOCID_T> documentIDs;
    documentIDs.push_back(did);

    parsedDocs=indriEnvironment.documents(documentIDs);

    for (int i=0; i < documentIDs.size(); i++) {
      std::string documentName = getDocumentName(parsedDocs[i]);
      std::string documentUrl = getDocumentUrl(parsedDocs[i]);
      // print the document ID, document name and its content
      // std::cout <<documentName<<" "<< documentIDs[i] << " " << documentUrl << std::endl;

      docInfo d(documentIDs[i], documentName);
      url_map[documentUrl] = d;

      /*get list content*/
      vector <int> termIds;
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);

      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      /*show content*/
      if(documentIDs[i] == did){
      std::cout <<documentName<<" "<< documentIDs[i] << " " << documentUrl << std::endl;
        for(int j=0; j<termIds.size(); ++j){
          // std::cout << termIds[j] << " ";
          std::cout << thisIndex->term(termIds[j]) << " ";
        }
        std::cout << std::endl;
      }
    }

    // for (std::map<std::string,docInfo>::iterator it=url_map.begin(); it!=url_map.end(); ++it) {
    //   std::cout << it->first << " " << it->second.did << " " << it->second.dname << '\n';
    // }

}

void FIndexBuilder::show_unique_tid_documents(int did){

    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    std::vector<lemur::api::DOCID_T> documentIDs;
    documentIDs.push_back(did);

    parsedDocs=indriEnvironment.documents(documentIDs);

    for (int i=0; i < documentIDs.size(); i++) {
      std::string documentName = getDocumentName(parsedDocs[i]);
      std::string documentUrl = getDocumentUrl(parsedDocs[i]);
      // print the document ID, document name and its content
      // std::cout <<documentName<<" "<< documentIDs[i] << " " << documentUrl << std::endl;

      docInfo d(documentIDs[i], documentName);
      url_map[documentUrl] = d;

      /*get list content*/
      vector <int> termIds;
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);

      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      /*unique the tids*/
      sort(termIds.begin(), termIds.end());
      std::vector<int>::iterator it;
      it = std::unique (termIds.begin(), termIds.end());       
      termIds.resize( std::distance(termIds.begin(),it) );

      /*show content*/
      if(documentIDs[i] == did){
      std::cout <<documentName<<" "<< documentIDs[i] << " " << documentUrl << std::endl;
        for(int j=0; j<termIds.size(); ++j){
          // std::cout << termIds[j] << " ";
          std::cout << thisIndex->term(termIds[j]) << " ";
        }
        std::cout << std::endl;
      }
    }

    // for (std::map<std::string,docInfo>::iterator it=url_map.begin(); it!=url_map.end(); ++it) {
    //   std::cout << it->first << " " << it->second.did << " " << it->second.dname << '\n';
    // }

}

void FIndexBuilder::build(){

    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = "/home/qi/index_indri/gov2_no_sw"; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    int start_did = 1;
    int end_did = 100;
    std::vector<lemur::api::DOCID_T> documentIDs;
    for(int i = start_did; i <= end_did; ++i){
      documentIDs.push_back(i);
    }

    parsedDocs=indriEnvironment.documents(documentIDs);

    for (int i=0; i < documentIDs.size(); i++) {
      std::string documentName = getDocumentName(parsedDocs[i]);
      std::string documentUrl = getDocumentUrl(parsedDocs[i]);
      // print the document ID, document name and its content
      std::cout <<documentName<<" "<< documentIDs[i] << " " << documentUrl << std::endl;


      /*get list content*/
      vector <int> termIds;
      const indri::index::TermList *termList = thisIndex->termList(documentIDs[i]);
     // for(int j=0; j<termList->terms().size(); ++j){
     //   cout << termList->terms()[j] << " ";
     // }
     // cout << endl;
     // for(int j=0; j<termList->terms().size(); ++j){
     //  cout << thisIndex->term(termList->terms()[j]) << " ";
     // }
     // cout << endl;

      for(int j=0; j<termList->terms().size(); ++j){
        if(termList->terms()[j]!=0){ //remove tid 0, it's an NULL symbol
          termIds.push_back(termList->terms()[j]);
        }
      }

      for(int j=0; j<termIds.size(); ++j){
        // std::cout << termIds[j] << " ";
        std::cout << thisIndex->term(termIds[j]) << " ";
      }
      std::cout << std::endl;
    }
}

std::string FIndexBuilder::getDocumentName(ParsedDocument* Pd){
    indri::utility::greedy_vector<indri::parse::MetadataPair>::iterator iter = std::find_if( Pd->metadata.begin(), Pd->metadata.end(),
          indri::parse::MetadataPair::key_equal( "docno" ) );
        if( iter != Pd->metadata.end() ){
          return (char*) iter->value;
        }else{
          cout << "can't find the document.." <<endl;
          exit(0);
        }
}

std::string FIndexBuilder::getDocumentUrl(ParsedDocument* Pd){
    indri::utility::greedy_vector<indri::parse::MetadataPair>::iterator iter = std::find_if( Pd->metadata.begin(), Pd->metadata.end(),
          indri::parse::MetadataPair::key_equal( "url" ) );
        if( iter != Pd->metadata.end() ){
          return (char*) iter->value;
        }else{
          cout << "can't find the document.." <<endl;
          exit(0);
        }
}

void FIndexBuilder::test_vbyte(){
    std::vector<int> nums;
    nums.push_back(3);
    nums.push_back(1601);
    nums.push_back(11);

    std::vector<unsigned char> list;
    int c_size = compressionVbytes(nums, list);
    dumpToFile("/home/qi/reorder_data/forward_index_content/test",  list.begin(), list.end());

    cout << "compressed size: " << c_size << "\n";

    char array[c_size];
    // for(int i = 0; i < c_size; i++) {
    //   array[i] = list[i];
    // }
    FILE* fp = fopen("/home/qi/reorder_data/forward_index_content/test","rb");
    size_t ret_code = fread(array, sizeof *array, c_size, fp); // reads an array of doubles
    if(ret_code == c_size) {
        puts("Array read successfully");
    } else { // error handling
       if (feof(fp))
          printf("Error reading test.bin: unexpected end of file\n");
       else if (ferror(fp)) {
           perror("Error reading test.bin");
       }
    }
    fclose(fp);

    int size = nums.size();
    int result[size];
    decompressionVbytes(array, result, size);

    for(int i = 0; i < size; i++) {
      std::cout << result[i] << " ";
    }
    std::cout << "\n";

}

void FIndexBuilder::parse_lm(string mit_lm, string unigrams, string bigrams){
  // FILE* fp = fopen("/home/qi/reorder_data/lm_info/tera0315.lm", "r");
  // FILE* f = fopen("/home/qi/reorder_data/lm_info/tera0315_bigrams", "w");
  // FILE* f1 = fopen("/home/qi/reorder_data/lm_info/tera0315_unigrams", "w");
  FILE* fp = fopen(mit_lm.c_str(), "r");
  FILE* f1 = fopen(unigrams.c_str(), "w");
  FILE* f = fopen(bigrams.c_str(), "w");
  char c[10000];
  vector<string> contents;
  int line_cnt = 0;
  bool sw = false;
  string sp = "<s>";
  string sp1 = "</s>";

  while (fgets(c, 10000, fp)){
    string s(c);
    line_cnt++;
    /*get rid of header info*/
    if(line_cnt <= 6){
      continue;
    }
    /*remove blank line*/
    if(s.compare("\n")==0){
      continue;
    }

    /*remove \end\*/
    if(s.compare("\\end\\\n")==0){
      continue;
    }

    /*lines before \2-grams:*/
    if(!sw){
      if(s.compare("\\2-grams:\n") != 0){

        if (s.find(sp) != std::string::npos){
          continue;
        }
        if (s.find(sp1) != std::string::npos){
          continue;
        }
        fprintf(f1, "%s", s.c_str());
      }
    }

    /*lines after \2-grams:*/
    if(sw){
      if (s.find(sp) != std::string::npos){
        continue;
      }
      if (s.find(sp1) != std::string::npos){
        continue;
      }
      fprintf(f, "%s", s.c_str());
    }

    /*find \2-grams:*/
    if(s.compare("\\2-grams:\n") == 0){
      sw = true;
    }
  }

  fclose(fp);
  fclose(f);
  fclose(f1);
}


void FIndexBuilder::lm_coverage(){
  set<string> test_set;
  set<string> train_set;
  char t1[10000];
  char t2[10000];
  FILE* ftest = fopen("/home/qi/reorder_data/query_info/tera06_test_queries", "r");
  while(fscanf(ftest, "%s %s\n", t1, t2) == 2){
    string s1(t1);
    string s2(t2);
    // cout << s1 << " " << s2 << "\n";
    test_set.insert(s1);
    test_set.insert(s2);
  }
  fclose(ftest);

  FILE* ftrain = fopen("/home/qi/reorder_data/query_info/tera06_train_queries", "r");
  while(fscanf(ftrain, "%s %s\n", t1, t2) == 2){
    string s1(t1);
    string s2(t2);
    // cout << s1 << " " << s2 << "\n";
    train_set.insert(s1);
    train_set.insert(s2);
  }
  fclose(ftrain);

  cout << test_set.size() << " " << train_set.size() << "\n";

  int total = test_set.size();
  int cover = 0;

  for(auto it = test_set.begin(); it!=test_set.end(); it++) {
    auto it1 = train_set.find(*it);
    if(it1!=train_set.end()){
      cover ++;
    }
  }
  float per = (float)cover/(float)total;

  cout << "total: " << total << " cover: " << cover << " percentage: " << per << "\n";
}

void FIndexBuilder::organize_pairs(string indri_index, string pair_scores, string lm_lex, string lm_index){

    /*prepare index for tid lookup*/
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = indri_index; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    // int tid = thisIndex->term(term);

    // FILE* f = fopen("/home/qi/reorder_data/lm_info/tera0319_pairs_score_threshold_7", "r");
    // FILE* f1 =  fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_content_reverse_0405", "w");
    FILE* f = fopen(pair_scores.c_str(), "r");
    FILE* f1 =  fopen(lm_index.c_str(), "w");
    char t1[10000];
    char t2[10000];
    int score;
    vector<LMPairScore> lm_vec;
    while(fscanf(f, "%s %s %d", t1, t2, &score) == 3){
      string s1(t1);
      string s2(t2);
      LMPairScore p1(s1, s2, score);
      LMPairScore p2(s2, s1, score);
      lm_vec.push_back(p1);
      lm_vec.push_back(p2);
      // cout << t1 << " " << t2 << " " << score << "\n";
    }
    fclose(f); 

    sort(lm_vec.begin(), lm_vec.end(), sort_LMPair_by_term_score);
    int length = 0;
    int offset = 0;
    vector<int> lengths;
    vector<int> offsets;
    vector<string> terms;
    vector<int> tids;
    string init_term;
    int pre_offset = 0;
    init_term = lm_vec[0].term1;

    for(int i = 0; i < lm_vec.size(); i++) {
      // cout << lm_vec[i].term1 << " " << lm_vec[i].term2 << " " << lm_vec[i].score << "\n";
      if(init_term.compare(lm_vec[i].term1)!=0){
        tids.push_back(thisIndex->term(init_term));
        lengths.push_back(length);
        offsets.push_back(pre_offset);
        pre_offset = offset;
        length = 1;
        init_term = lm_vec[i].term1;
      }else{
        length ++;
      }
      offset ++;
      fprintf(f1, "%d %d\n", thisIndex->term(lm_vec[i].term2), lm_vec[i].score);
    }
    tids.push_back(thisIndex->term(init_term));
    lengths.push_back(length);
    offsets.push_back(pre_offset);
    fclose(f1);

    // FILE* f2 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_lex_reverse_0405", "w");
    FILE* f2 = fopen(lm_lex.c_str(), "w");
    for(int i = 0; i < tids.size(); i++){
      fprintf(f2, "%d %d %d\n", tids[i], lengths[i], offsets[i]);
    }
    fclose(f2);
}

void FIndexBuilder::organized_pair_with_probs(){
      FILE* f = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_content", "r");
      FILE* f1 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_lex", "r");

      vector<pair<int, int>> lm_content_vec;
      vector<double> prob_vec;
      int tid;
      int score;
      while(fscanf(f, "%d %d", &tid, &score)==2){
        pair<int, int> p = make_pair(tid, score);
        lm_content_vec.push_back(p);
        prob_vec.push_back(0.0);
        // cout << tid << " " << score << "\n";
      }

      map<int, pair<int, int>> lm_map;
      int length;
      int offset;
      while(fscanf(f1, "%d %d %d", &tid, &length, &offset) == 3){
        lm_map[tid] = make_pair(length, offset);
        // cout << tid << " " << length << " " << "\n";
      }
      fclose(f);
      fclose(f1);

      // cout << lm_map.size() << " " << lm_content_vec.size() << "\n";
      FILE* f2 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_lex_prob", "w");   
      FILE* f3 = fopen("/home/qi/reorder_data/lm_info/tera0321_structural_pairs_threshold_7_content_prob", "w");   
      int sum = 0;
      double prob;
      for(auto it = lm_map.begin(); it!=lm_map.end(); it++){
          length = it->second.first;
          offset = it->second.second;
          for(int k = 0; k < length; k++) {
            sum += lm_content_vec[offset+k].second;
          }
          cout << it->first << " " << length << " " << offset << " " << sum << "\n";
          fprintf(f2, "%d %d %d %d\n", it->first, length, offset, sum);
          for(int k = 0; k < length; k++) {
            prob = (double)lm_content_vec[offset+k].second/(double)sum;
            prob_vec[offset+k] = prob;
          }
          sum = 0;
      }
      fclose(f2);

      for(int i = 0; i < lm_content_vec.size(); i++){
          cout << lm_content_vec[i].first << " " << lm_content_vec[i].second << " " << prob_vec[i] << "\n";
          fprintf(f3, "%d %d %lf\n", lm_content_vec[i].first, lm_content_vec[i].second, prob_vec[i]);
      }
      fclose(f3);
}

void FIndexBuilder::compute_term_score(string indri_index, string unigrams, double multi, string term_with_score){
    /*prepare index for tid lookup*/
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = indri_index; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    FILE* f = fopen(unigrams.c_str(), "r");
    FILE* f1 = fopen(term_with_score.c_str(), "w");
    double l1, l2;
    double base = 10;
    char t[10000];
    while(fscanf(f, "%lf %s %lf\n", &l1, t, &l2) == 3){
        double result = pow(base, l1);
        int quant = result * multi;
        string term(t);
        cout << t << " " << thisIndex->term(term) << " " << quant << " " << result << "\n";
        fprintf(f1, "%d %d\n", thisIndex->term(term), quant);
    }
    fclose(f);
    fclose(f1);
}

void FIndexBuilder::compute_term_score_unweighted(string indri_index, string unigrams, string term_with_score){
    /*prepare index for tid lookup*/
    QueryEnvironment indriEnvironment;
    indri::collection::Repository repository;
    std::vector<ParsedDocument*> parsedDocs;
    indri::index::Index *thisIndex;
    const std::string indexPath = indri_index; 

    indriEnvironment.addIndex(indexPath);
    repository.openRead(indexPath);
    indri::collection::Repository::index_state repIndexState = repository.indexes();
    thisIndex=(*repIndexState)[0];

    FILE* f = fopen(unigrams.c_str(), "r");
    FILE* f1 = fopen(term_with_score.c_str(), "w");
    double l1, l2;
    double base = 10;
    char t[10000];
    while(fscanf(f, "%lf %s %lf\n", &l1, t, &l2) == 3){
        int quant = 10;
        string term(t);
        cout << t << " " << thisIndex->term(term) << " " << quant << "\n";
        fprintf(f1, "%d %d\n", thisIndex->term(term), quant);
    }
    fclose(f);
    fclose(f1);
}

void FIndexBuilder::compute_pair_score(string lm_pairs, double threshold, double multi, string pairs_with_score){
    FILE* f = fopen(lm_pairs.c_str(), "r");
    FILE* f1 = fopen(pairs_with_score.c_str(), "w");
    double l;
    double base = 10;
    char t1[10000];
    char t2[10000];
    while(fscanf(f, "%s %s %lf\n", t1, t2, &l) == 3){
      if(l > threshold) {
        string s1(t1);
        string s2(t2);
        pair<string, string> p = make_pair(s1, s2);
        double result = pow(base, l);
        int quant = result * multi;
        cout << s1 << " " << s2 << " " << result << "\n";
        fprintf(f1, "%s %s %d\n", s1.c_str(), s2.c_str(), quant);
      }
    }
    fclose(f);
    fclose(f1);
}

void FIndexBuilder::show_unigram_prob(){
  FILE* f = fopen("/home/qi/reorder_data/lm_info/tera0315_unigrams", "r");
  float l1, l2;
  char t[10000]; 
  vector<LMUnigram> uni_vec;
  uni_vec.reserve(29511);
  while(fscanf(f, "%f %s %f\n", &l1, t, &l2) == 3){
    string s(t);
    LMUnigram u(s, l1, l2);
    uni_vec.push_back(u);
    // cout << l1 << " " << s << " " << " " << l2 << "\n";
  }
  sort(uni_vec.begin(), uni_vec.end(), sort_LMUnigram_by_prob);

  for(int i = 0; i < uni_vec.size(); i++){
    cout << uni_vec[i].term << " " << uni_vec[i].prob << " " << uni_vec[i].backoff_prob << " " << uni_vec[i].prob + uni_vec[i].backoff_prob << "\n";
  }
  fclose(f);
}

void FIndexBuilder::show_pair_prob(){
    FILE* f1 = fopen("/home/qi/reorder_data/lm_info/tera0319_pairs", "r");
    FILE* f2 = fopen("/home/qi/reorder_data/query_info/tera06_test_0315", "r");
    char t1[10000];
    char t2[10000];
    float l;
    map<pair<string, string>, float> bi_map;
    while(fscanf(f1, "%s %s %f\n", t1, t2, &l) == 3){
      string s1(t1);
      string s2(t2);
      pair<string, string> p = make_pair(s1, s2);
      bi_map[p] = l;
    }

    vector<LMPair> pair_vec;
    while(fscanf(f2, "%s %s\n", t1, t2) == 2){
      string s1(t1);
      string s2(t2);
      pair<string, string> p = make_pair(s1, s2);
      auto it = bi_map.find(p);
      if(it!= bi_map.end()) {
        // cout << s1 << " " << s2 << " " << it->second << "\n";
        LMPair lp(s1, s2, it->second);
        pair_vec.push_back(lp);
      }
    }

    sort(pair_vec.begin(), pair_vec.end(), sort_LMPair_by_prob);

    for(int i = 0; i < pair_vec.size(); i++){
      cout << pair_vec[i].term1 << " " << pair_vec[i].term2 << " " << pair_vec[i].prob << "\n";
    }
    fclose(f1);
    fclose(f2);
}

void FIndexBuilder::compute_pair_probs_from_unigrams_and_bigrams(string unigrams, string bigrams, string pairs){
    // FILE* f1 = fopen("/home/qi/reorder_data/lm_info/tera0315_unigrams", "r");
    // FILE* f2 = fopen("/home/qi/reorder_data/lm_info/tera0315_bigrams", "r");
    // FILE* f3 = fopen("/home/qi/reorder_data/lm_info/tera0319_pairs", "w");
    FILE* f1 = fopen(unigrams.c_str(), "r");
    FILE* f2 = fopen(bigrams.c_str(), "r");
    FILE* f3 = fopen(pairs.c_str(), "w");
    float l, l1, nl;
    char t[10000];
    char t1[10000], t2[10000];
    map<string, float> uni_map;
    while(fscanf(f1, "%f %s %f\n", &l, t, &l1) == 3){
      // cout << l << " " << t << " " << l1 << "\n";
      string s(t);
      uni_map[s] = l;
    }

    // map<pair<string, string>, float> bi_map;
    vector<LMPair> pair_vec;
    pair_vec.reserve(81197);
    while(fscanf(f2, "%f %s %s\n", &l, t1, t2) == 3){
      string s1(t1);
      string s2(t2);
      pair<string, string> p = make_pair(s1, s2);
      auto it = uni_map.find(s1);
      if(it!=uni_map.end()){
        nl = it->second + l;
        // cout <<  it->second << " " << l << " " << nl << " " << s1 << " " << s2 << "\n";
        LMPair lp(s1, s2, nl);
        pair_vec.push_back(lp);
        // fprintf(f3, "%s %s %f\n", s1.c_str(), s2.c_str(), nl);
      }
    }

    sort(pair_vec.begin(), pair_vec.end(), sort_LMPair_by_prob);

    for(int i = 0; i < pair_vec.size(); i++) {
      // cout << pair_vec[i].term1 << " " << pair_vec[i].term2 << " " << pair_vec[i].prob << "\n";
      fprintf(f3, "%s %s %f\n", pair_vec[i].term1.c_str(), pair_vec[i].term2.c_str(), pair_vec[i].prob);
    }

    fclose(f1);
    fclose(f2);
    fclose(f3);
}

void FIndexBuilder::compute_pair_score_with_same_weight(string bigrams, string pairs_with_score){
    FILE* f1 = fopen(bigrams.c_str(), "r");
    FILE* f2 = fopen(pairs_with_score.c_str(), "w");
    int l;
    int uni_score = 10;
    char t1[10000], t2[10000];

    while(fscanf(f1, "%s %s %d\n", t1, t2, &l) == 3){
      fprintf(f2, "%s %s %d\n", t1, t2, uni_score);
    }

    fclose(f1);
    fclose(f2);
}

void FIndexBuilder::fetch_query_terms(std::string queries, std::string terms){
  set<string> term_set;
  FILE* file = fopen(queries.c_str(), "r");
  char t1[1000];
  char t2[1000];

  while(fscanf(file, "%s %s %*[^\n]\n", t1, t2) == 2){
    string s1(t1);
    string s2(t2);
    term_set.insert(t1);
    term_set.insert(t2);
    // cout << s1 << " " << s2 << "\n";
  }

  fclose(file);

  cout << term_set.size() << "\n";

  FILE* f1 = fopen(terms.c_str(), "w");
  for(auto it = term_set.begin(); it!= term_set.end(); it++) {
      // cout << *it << "\n";
    string s = *it;
    fprintf(f1, "%s\n", s.c_str());
  }
  fclose(f1);
}

void FIndexBuilder::qp_non_indri(std::string order, std::string non_indri_lex, std::string non_indri_content, long long data_size, int max_d, std::string order_path, std::string query){
  /*
  cout << "qp using non_indri_inverted_list\n";
  cout << non_indri_lex << " " << non_indri_content << "\n";
  cout << max_d << "\n"; 
  cout << order << "\n";
  */

  //load vbyte index info
  FILE* lex = fopen(non_indri_lex.c_str(), "r");
  if(lex == NULL) {
    cout << "can not open: " << non_indri_lex << "\n";
  }
  int tid, size, length;
  long long offset;
  char term[10000];
  
  map<string, int> tids;
  map<string, int> lengths;
  map<string, int> sizes;
  map<string, long long> offsets; 

  while(fscanf(lex, "%s %d %d %d %lld", term, &tid, &length, &size, &offset) == 5) {
    // cout << tid << " " << length << " " << size << " " << offset << "\n";
    tids[term] = tid;
    sizes[term] = size;
    lengths[term] = length;
    offsets[term] = offset;
  }
  fclose(lex);

  // for(int i = 1; i < MAXD; i++){
  //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
  // }
  cout << "lex loading done\n";

  //load content
  FILE* fp = fopen(non_indri_content.c_str(),"rb");
  char* data = new char[data_size];
  long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
  if(ret_code == data_size) {
      puts("data read successfully");
  } else { // error handling
     if (feof(fp))
        printf("Error reading test.bin: unexpected end of file\n");
     else if (ferror(fp)) {
         perror("Error reading test.bin");
     }
  }
  fclose(fp);


  if( order!="url" && order!="random" && order!="bp" && order!="bplm" && order!="fb"){
    std::cout << "pls specify ordering\n";
    return;
  }

  //load new ordering
  std::map<int, int> order_map;
  if(order == "random"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/random_order_all_0306", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }else if(order == "url"){
    // FILE* lfile = fopen("/home/qi/reorder_data/forward_index_with_dups/url_sorted_lex", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int did;
    int count = 1;
    char c[50000];
    while(fscanf(lfile, "%s %d %*[^\n]\n", c, &did) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[did] = count;
      count++;
    }
    fclose(lfile);
  } else if(order == "bp"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_gov2_0311", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "bplm"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/bp_order_pres_48_100p_ends_0414", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  } else if(order == "fb"){
    // FILE* lfile = fopen("/home/qi/reorder_data/ordering_info/order_fb_0408", "r");
    FILE* lfile = fopen(order_path.c_str(), "r");
    int ndid, odid;
    while(fscanf(lfile, "%d %d\n", &ndid, &odid) == 2){
      // std::cout << ndid << " " << odid << "\n";
      order_map[odid] = ndid;
    }
    fclose(lfile);
  }

  //load query
  FILE* qfile = fopen(query.c_str(), "r");
  char t1[1000];
  char t2[1000];
  int qid;
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;
  long long total_bp = 0;
  long long total_hit = 0;
  int num_query = 0;
  char* list_data = new char[data_size];
  int* uncompressed_list = new int[data_size];
  while(fscanf(qfile, "%s %s %d %*[^\n]\n", t1, t2, &qid) == 3) { //for new format
    //cout <<  ++num_query << " " << t1 << " " << t2 << " " << qid << "\n";
    cout << t1 << " " << t2 << " " << qid << " ";
    num_query++;
    string term1(t1);
    string term2(t2);
    int tid1 = tids[term1];
    int size1 = sizes[term1];
    int length1 = lengths[term1];
    long long offset1 = offsets[term1];
    
    int tid2 = tids[term2];
    int size2 = sizes[term2];
    int length2 = lengths[term2];
    long long offset2 = offsets[term2];
    
    //cout << term1 << " " << tid1 << " " << size1 << " " << length1 << " " << offset1 << "\n";
    //cout << term2 << " " << tid2 << " " << size2 << " " << length2 << " " << offset2 << "\n";
    
   if(offset1 + size1 >= data_size){
      cout << "sth is wrong\n";
      exit(0);
    }

    if(offset2 + size2 >= data_size){
      cout << "sth is wrong\n";
      exit(0);
    }
    
    /*load list for term1*/ 
    for(int i = 0; i < size1; i++) {
      list_data[i] = data[offset1+i];  
    }
    decompressionVbytes(list_data, uncompressed_list, length1);
    for(int i= 1; i < length1; i++) {
      uncompressed_list[i]+= uncompressed_list[i-1];
    }

    vector<int> ndids1;
    for(int i=0; i < length1; i++) {
      if(order_map.find(uncompressed_list[i])!=order_map.end()){
        ndids1.push_back(order_map[uncompressed_list[i]]);
      }else{
        cout << "attention: " << uncompressed_list[i] << " has no match in ordering\n";
        return;
      }
    }


    /*load list for term2*/
    for(int i = 0; i < size2; i++) {
      list_data[i] = data[offset2+i];
    }
    decompressionVbytes(list_data, uncompressed_list, length2);
    for(int i= 1; i < length2; i++) {
      uncompressed_list[i]+= uncompressed_list[i-1];
    }

    vector<int> ndids2;
    for(int i=0; i < length2; i++) {
      if(order_map.find(uncompressed_list[i])!=order_map.end()){
        ndids2.push_back(order_map[uncompressed_list[i]]);
      }else{
        cout << "attention: " << uncompressed_list[i] << " has no match in ordering\n";
        return;
      }
    }

    //cout << ndids1.size() << " " << ndids2.size() << "\n";

    sort(ndids1.begin(), ndids1.end());
    sort(ndids2.begin(), ndids2.end());
        
    ndids1.push_back(max_d);
    ndids2.push_back(max_d);

    int bp = 0;
    int hit = 0;
    qpList l1(ndids1, max_d);
    qpList l2(ndids2, max_d);
    and_bp(l1, l2, bp, hit, max_d);
    std::cout << hit << " " << bp << "\n";
    total_bp += bp;
    total_hit += hit;
  }
  cout << "num of queries: " << num_query << "\n";
  cout << "ave nextGEQ: " << total_bp/num_query << "\n";
  cout << "ave hit: " << total_hit/num_query << "\n";
  fclose(qfile); 
  delete[] data; 
  delete[] list_data;
  delete[] uncompressed_list;
}

void FIndexBuilder::verify_non_indri_term_freqs(string indri_index, string non_indri_lex, string non_indri_content, long long data_size, int tid_to_verify){
  cout << "verify tid: " << tid_to_verify << "\n";
  FILE* lex = fopen(non_indri_lex.c_str(), "r");
  if(lex == NULL) {
    cout << "can not open: " << non_indri_lex << "\n";
  }
  int tid, size, length;
  long long offset;
  char term[10000];
/*
  vector<int> lengths;
  vector<int> sizes;
  vector<long long> offsets;  

  sizes.reserve(max_term);
  lengths.reserve(max_term);
  offsets.reserve(max_term);
  for(long long i = 0; i < max_term; i++){
    sizes.push_back(0);
    lengths.push_back(0);
    offsets.push_back(0);
  }
*/
  
 map<int, int> lengths;
 map<int, int> sizes;
 map<int, long long> offsets; 

  while(fscanf(lex, "%s %d %d %d %lld", term, &tid, &length, &size, &offset) == 5) {
    // cout << tid << " " << length << " " << size << " " << offset << "\n";
    sizes[tid] = size;
    lengths[tid] = length;
    offsets[tid] = offset;
  }
  fclose(lex);

  // for(int i = 1; i < MAXD; i++){
  //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
  // }
  cout << "lex loading done\n";

  /*load content*/
  FILE* fp = fopen(non_indri_content.c_str(),"rb");
  char* data = new char[data_size];
  long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
  if(ret_code == data_size) {
      puts("data read successfully");
  } else { // error handling
     if (feof(fp))
        printf("Error reading test.bin: unexpected end of file\n");
     else if (ferror(fp)) {
         perror("Error reading test.bin");
     }
  }
  fclose(fp);

  /*take out did content and decompress*/
  size = sizes[tid_to_verify];
  length = lengths[tid_to_verify];
  offset = offsets[tid_to_verify];
  cout << tid_to_verify << " " << size << " " << length << " " << offset << endl;
  //exit(0);
  char* d_data = new char[size];
  int* uncompressed_data = new int[length];
  // char d_data[size];
  // int uncompressed_data[length];
  for(int i = 0; i < size; i++){
    d_data[i] = data[offset+i];
  }

  decompressionVbytes(d_data, uncompressed_data, length);

  // for(int i = 0; i < length; i++){
  //   cout << uncompressed_data[i] << " "; 
  // }
  // cout << "\n";
 
  //for(int i = 0; i < length; i++){
    //cout << uncompressed_data[i] << " "; 
  //}
  //cout << "\n";  

  delete[] data;
  delete[] d_data;
  
  /*take out origin inverted list content for tid_to_verify*/
  QueryEnvironment indriEnvironment;
  indri::collection::Repository repository;
  indri::index::Index *thisIndex;
  const std::string indexPath = indri_index;

  indriEnvironment.addIndex(indexPath);
  repository.openRead(indexPath);
  indri::collection::Repository::index_state repIndexState = repository.indexes();
  thisIndex=(*repIndexState)[0];

  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;

  string t1 = thisIndex->term(tid_to_verify);
  iter = thisIndex->docListIterator(t1);
  vector<int> freqs;
  if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      exit(0);
  }
  iter->startIteration();
  for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        //dids1.push_back(entry->document);
	freqs.push_back(entry->positions.size());
        //dids1.push_back(1);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
  }

  bool equal = true;

  for(int i = 0; i < length; i++){
    if(uncompressed_data[i] != freqs[i]){
      equal = false;
      // cout << uncompressed_data[i] << " " << termIds[i] << " not equal\n";
    }
  }

  if(equal){
    cout << "inverted list for " << tid_to_verify << " " << t1 << " verified!\n";
  }else{ cout << "inverted list for " << tid_to_verify << " " << t1 << " not verified... please check!\n";   
  }
  delete iter;
  delete[] uncompressed_data;
}


void FIndexBuilder::verify_non_indri_inverted_list(string indri_index, string non_indri_lex, string non_indri_content, long long data_size, int tid_to_verify){
  cout << "verify tid: " << tid_to_verify << "\n";
  FILE* lex = fopen(non_indri_lex.c_str(), "r");
  if(lex == NULL) {
    cout << "can not open: " << non_indri_lex << "\n";
  }
  int tid, size, length;
  long long offset;
  char term[10000];
/*
  vector<int> lengths;
  vector<int> sizes;
  vector<long long> offsets;  

  sizes.reserve(max_term);
  lengths.reserve(max_term);
  offsets.reserve(max_term);
  for(long long i = 0; i < max_term; i++){
    sizes.push_back(0);
    lengths.push_back(0);
    offsets.push_back(0);
  }
*/
  
 map<int, int> lengths;
 map<int, int> sizes;
 map<int, long long> offsets; 

  while(fscanf(lex, "%s %d %d %d %lld", term, &tid, &length, &size, &offset) == 5) {
    // cout << tid << " " << length << " " << size << " " << offset << "\n";
    sizes[tid] = size;
    lengths[tid] = length;
    offsets[tid] = offset;
  }
  fclose(lex);

  // for(int i = 1; i < MAXD; i++){
  //   cout << i << " " << sizes[i] << " " << offsets[i] << "\n";
  // }
  cout << "lex loading done\n";

  /*load content*/
  FILE* fp = fopen(non_indri_content.c_str(),"rb");
  char* data = new char[data_size];
  long long ret_code = fread(data, sizeof *data, data_size, fp); // reads an array of doubles
  if(ret_code == data_size) {
      puts("data read successfully");
  } else { // error handling
     if (feof(fp))
        printf("Error reading test.bin: unexpected end of file\n");
     else if (ferror(fp)) {
         perror("Error reading test.bin");
     }
  }
  fclose(fp);

  /*take out did content and decompress*/
  size = sizes[tid_to_verify];
  length = lengths[tid_to_verify];
  offset = offsets[tid_to_verify];
  cout << tid_to_verify << " " << size << " " << length << " " << offset << endl;
  //exit(0);
  char* d_data = new char[size];
  int* uncompressed_data = new int[length];
  // char d_data[size];
  // int uncompressed_data[length];
  for(int i = 0; i < size; i++){
    d_data[i] = data[offset+i];
  }

  decompressionVbytes(d_data, uncompressed_data, length);

  // for(int i = 0; i < length; i++){
  //   cout << uncompressed_data[i] << " "; 
  // }
  // cout << "\n";

  for(int i = 1; i < length; i++){
    uncompressed_data[i]+= uncompressed_data[i-1]; 
  }
  
  //for(int i = 0; i < length; i++){
    //cout << uncompressed_data[i] << " "; 
  //}
  //cout << "\n";  

  delete[] data;
  delete[] d_data;
  
  /*take out origin inverted list content for tid_to_verify*/
  QueryEnvironment indriEnvironment;
  indri::collection::Repository repository;
  indri::index::Index *thisIndex;
  const std::string indexPath = indri_index;

  indriEnvironment.addIndex(indexPath);
  repository.openRead(indexPath);
  indri::collection::Repository::index_state repIndexState = repository.indexes();
  thisIndex=(*repIndexState)[0];

  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;

  string t1 = thisIndex->term(tid_to_verify);
  iter = thisIndex->docListIterator(t1);
  vector<int> dids1;
  if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      exit(0);
  }
  iter->startIteration();
  for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        dids1.push_back(entry->document);
        //dids1.push_back(1);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
  }

  bool equal = true;

  for(int i = 0; i < length; i++){
    if(uncompressed_data[i] != dids1[i]){
      equal = false;
      // cout << uncompressed_data[i] << " " << termIds[i] << " not equal\n";
    }
  }

  if(equal){
    cout << "inverted list for " << tid_to_verify << " " << t1 << " verified!\n";
  }else{ cout << "inverted list for " << tid_to_verify << " " << t1 << " not verified... please check!\n";   
  }
  delete iter;
  delete[] uncompressed_data;
}

void FIndexBuilder::write_non_indri_inverted_lists_ascii_url(string indri_index, string terms, string ordering, string content){
  cout << "using indri index: " << indri_index << "\n";
  cout << "build inverted lists for terms from: " << terms << "\n";
  //cout << "start pos: " << start << " end pos " << end << "\n";
  cout << " write content to: " << content << "\n";
  cout << " use ordering: " << ordering << "\n";

  unordered_map<int, int> order_table;
  FILE* lfile = fopen(ordering.c_str(), "r");
  int did;
  int count = 1;
  char c[50000];
  while(fscanf(lfile, "%s %d %*[^\n]\n", c, &did) == 2){
     // std::cout << ndid << " " << odid << "\n";
      order_table[did] = count;
      count++;
  }
  fclose(lfile);

  indri::collection::Repository repository;
  const std::string indexPath = indri_index;
  repository.openRead(indexPath);
  //indri::server::LocalQueryServer local(repository);
  indri::collection::Repository::index_state state = repository.indexes();
  indri::index::Index* index = (*state)[0];

  //do terms first
  string content_file = content;
  FILE* qfile = fopen(terms.c_str(), "r");
  char t1[1000];
  int num_terms = 1;
  long long offset = 0;
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;
  while(fscanf(qfile, "%s\n", t1) == 1) {
    std::vector<int> dids1, ndids1;
    cout << num_terms << " " << t1 << "\n";
    iter = index->docListIterator(t1);
    if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      continue;
    }
    iter->startIteration();
    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        dids1.push_back(entry->document);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
    }
    
    for(int i = 0; i < dids1.size(); i++) {
      // cout << dids1[i] << " " << order_map[dids1[i]] << " "; 
      if(order_table.find(dids1[i])!=order_table.end()){
        ndids1.push_back(order_table[dids1[i]]);
      }else{
        cout << "attention: " << dids1[i] << " has no match in ordering\n";
      }
    }

    sort(ndids1.begin(), ndids1.end());
    
    /*for(int i = 0; i < dids1.size(); i++){
	cout << dids1[i] << " ";
    }
    cout << endl;*/
    string dir = content + to_string(num_terms);
    cout << dir << endl;
    FILE* tmp =  fopen(dir.c_str(), "w");
    for (int i = 0; i < ndids1.size(); i++) {
        fprintf(tmp, "%d ", ndids1[i]);
    }
    fclose(tmp);
    cout << dids1.size() << endl;
    cout << ndids1.size() << endl;
    num_terms++;
  }
  delete iter;
  fclose(qfile);
}


void FIndexBuilder::write_non_indri_inverted_list(string indri_index, string terms, string content, string lex){
  cout << "using indri index: " << indri_index << "\n";
  cout << "build inverted lists for terms from: " << terms << "\n";
  //cout << "start pos: " << start << " end pos " << end << "\n";
  cout << "write lex to " << lex << " write content to: " << content << "\n";

  indri::collection::Repository repository;
  const std::string indexPath = indri_index;
  repository.openRead(indexPath);
  //indri::server::LocalQueryServer local(repository);
  indri::collection::Repository::index_state state = repository.indexes();
  indri::index::Index* index = (*state)[0];

  /*do terms first*/
  string content_file = content;
  string lex_file = lex;
  FILE* pfile = fopen(lex_file.c_str(), "w");
  FILE* qfile = fopen(terms.c_str(), "r");
  char t1[1000];
  int unit_size = 5000000;
  int num_terms = 0;
  long long offset = 0;
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;
  vector<unsigned char> list;
  while(fscanf(qfile, "%s\n", t1) == 1) {
    std::vector<int> dids1;
    cout << num_terms << " " << t1 << "\n";
    iter = index->docListIterator(t1);
    if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      continue;
    }
    iter->startIteration();
    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        dids1.push_back(entry->document);
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
    }
    //sort(dids1.begin(), dids1.end());
    /*
    for(int i = 0; i < dids1.size(); i++){
	cout << dids1[i] << " ";
    }
    cout << endl;*/
    if(list.size() >= unit_size) {
      std::cout << "write out content " << list.size() << "\n";
      dumpToFile(content_file,  list.begin(), list.end());
      list.clear();
    }

    /*take delta of the tids*/
    for(int k = dids1.size()-1; k > 0; k--){
        dids1[k] = dids1[k] - dids1[k-1];
    }

    long long data_size = compressionVbytes(dids1, list);
    int tid = index->term(t1);
    //cout << t1 << " " << tid << " " << dids1.size() << " " << data_size << " " << offset << "\n";
    fprintf(pfile, "%s %d %lu %lld %lld\n", t1, tid, dids1.size(), data_size, offset);
    offset += data_size;
    num_terms++;
    //if(num_terms > 1){
    //  break;
    //}
  }
  dumpToFile(content_file, list.begin(), list.end()); 
  std::cout << "num of terms: " << num_terms << "\n";
  delete iter;
  fclose(qfile);
  fclose(pfile);
}

void FIndexBuilder::write_non_indri_term_freqs(string indri_index, string terms, string content, string lex){
  cout << "using indri index: " << indri_index << "\n";
  cout << "build inverted lists for terms from: " << terms << "\n";
  //cout << "start pos: " << start << " end pos " << end << "\n";
  cout << "write lex to " << lex << " write content to: " << content << "\n";

  indri::collection::Repository repository;
  const std::string indexPath = indri_index;
  repository.openRead(indexPath);
  //indri::server::LocalQueryServer local(repository);
  indri::collection::Repository::index_state state = repository.indexes();
  indri::index::Index* index = (*state)[0];

  /*do terms first*/
  string content_file = content;
  string lex_file = lex;
  FILE* pfile = fopen(lex_file.c_str(), "w");
  FILE* qfile = fopen(terms.c_str(), "r");
  char t1[1000];
  int unit_size = 5000000;
  int num_terms = 0;
  long long offset = 0;
  indri::index::DocListIterator::DocumentData* entry;
  indri::index::DocListIterator* iter;
  vector<unsigned char> list;
  while(fscanf(qfile, "%s\n", t1) == 1) {
    std::vector<int> freqs;
    cout << num_terms << " " << t1 << "\n";
    iter = index->docListIterator(t1);
    if (iter == NULL){
      std::cout << t1 << ": not in the index, attention!\n";
      continue;
    }
    iter->startIteration();
    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
        entry = iter->currentEntry();
        //dids1.push_back(entry->document);
	freqs.push_back(entry->positions.size());
        // std::cout << entry->document << " "
        //           << entry->positions.size() << " "
        //           << index->documentLength( entry->document ) << std::endl;
    }
    //sort(dids1.begin(), dids1.end());
    /*
    for(int i = 0; i < dids1.size(); i++){
	cout << dids1[i] << " ";
    }
    cout << endl;*/
    if(list.size() >= unit_size) {
      std::cout << "write out content " << list.size() << "\n";
      dumpToFile(content_file,  list.begin(), list.end());
      list.clear();
    }

    long long data_size = compressionVbytes(freqs, list);
    int tid = index->term(t1);
    //cout << t1 << " " << tid << " " << dids1.size() << " " << data_size << " " << offset << "\n";
    fprintf(pfile, "%s %d %lu %lld %lld\n", t1, tid, freqs.size(), data_size, offset);
    offset += data_size;
    num_terms++;
    //if(num_terms > 1){
    //  break;
    //}
  }
  dumpToFile(content_file, list.begin(), list.end()); 
  std::cout << "num of terms: " << num_terms << "\n";
  delete iter;
  fclose(qfile);
  fclose(pfile);
}

void FIndexBuilder::stem_queries_generate_term_lex(string indri_index, string input_query, string stemmed_query, string stemmed_term_lex) {
    indri::collection::Repository repository;
    const std::string indexPath = indri_index; 
    repository.openRead(indexPath);
    indri::server::LocalQueryServer local(repository);
    indri::collection::Repository::index_state state = repository.indexes();
    FILE* file = fopen(input_query.c_str(), "r");
    FILE* fl = fopen(stemmed_query.c_str(), "w");
   
    set<string> stemmed_term_set;
     
    char c[10000];
    while(fscanf(file, "%[^\n]\n", c) == 1) {
      string token(c);
      string d = " ";
      size_t pos = 0;
      string term;
      vector<string> terms;
      
      if (token.find(d) != string::npos) {//remove all queries with only one term
        while((pos = token.find(d)) != std::string::npos){
           term = token.substr(0, pos);
           terms.push_back(term);
           token.erase(0, pos + d.length());
        }
        terms.push_back(token);

        for (int i = 0; i < terms.size(); i++) {
           std::string stem = repository.processTerm( terms[i] );
	   stemmed_term_set.insert(stem);
	   fprintf(fl, "%s ", stem.c_str());
	   cout << stem << " ";
        }
	fprintf(fl, "\n");
	cout << "\n";
      }

    }
    
    /*write out the term lex*/
    FILE* f = fopen(stemmed_term_lex.c_str(), "w");
    for(auto it = stemmed_term_set.begin(); it != stemmed_term_set.end(); it++) {
        fprintf(f, "%s\n", (*it).c_str());
    }
    fclose(f);

    fclose(file);
    fclose(fl);
}


int compressionVbytes(std::vector<int>& input, std::vector<unsigned char>& compressedList){
   int compressedSize = 0;
   for (int i = 0; i < input.size(); ++i){
          unsigned char byteArr[sizeof(int)];
          bool started = false;
          int x = 0;

          for (x = 0; x < sizeof(int); x++) {
             byteArr[x] = (input[i]%128);
             input[i] /= 128;
          }

          for (x = sizeof(int) - 1; x > 0; x--) {
            if (byteArr[x] != 0 || started == true) {
              started = true;
              byteArr[x] |= 128;
              compressedList.push_back(byteArr[x]);
              compressedSize++;
            }
          }
          compressedList.push_back(byteArr[0]);
          compressedSize++;
   }
   return compressedSize;
}

int decompressionVbytes(char* input, int* output, int size){
    char* curr_byte = input;
    unsigned n;
    for (int i = 0; i < size; ++i) {
      char b = *curr_byte;
      n = b & 0x7F;
//        cout<<"The first byte: "<<n<<endl;
//        print_binary(n);
//        cout<<endl;

      while((b & 0x80) !=0){
        n = n << 7;
        ++curr_byte;
          b = *curr_byte;
          n |= (b & 0x7F);
//          cout<<"The following byte: "<<n<<endl;
//          print_binary(n);
//          cout<<endl;
      }
      ++curr_byte;
      output[i] = n;
    }

   int num_bytes_consumed = (curr_byte - input);
   return (num_bytes_consumed >> 2) + ((num_bytes_consumed & 3) != 0 ? 1 : 0);
}
